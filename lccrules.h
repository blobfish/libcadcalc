/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef LCC_RLS
#define LCC_RLS

#include "lccstow.h"

namespace lcc
{
  namespace rls
  {
    using namespace ::tao::pegtl;
    using namespace ::lcc::grm;
    
    // Class template for user-defined actions that does
    // nothing by default.
    template<typename Rule>struct action{};
    
    template<> struct action<lhsidentifier>
    {
      template< typename ParseInput >
      static bool apply(const ParseInput &in, Stow &stow)
      {
        stow.currentlyParsingExpression = in.string();
        auto v = stow.findExpression(in.string());
        if (v)
          stow.currentParsingDependents = stow.getDependents(*v);
        
        return true;
      }
    };
    
    template<> struct action<sid>
    {
      template< typename ParseInput >
      static bool apply(const ParseInput &in, Stow &stow)
      {
        auto ov = stow.findExpression(in.string());
        if (!ov)
        {
          std::ostringstream stream;
          stream << "Unknown Scalar Variable: " << in.string();
          throw parse_error(stream.str(), in.position());
        }
        
        if (in.string() == stow.currentlyParsingExpression)
          throw parse_error("Expression Can't Reference Itself", in.position());
        
        if (std::find(stow.currentParsingDependents.begin(), stow.currentParsingDependents.end(), *ov) != stow.currentParsingDependents.end())
          throw parse_error("Cycle Rejected", in.position());
        
        if (stow.graph[*ov].valueType() == ValueType::Scalar)
          return true;

        return false;
      }
    };
    
    template<> struct action<vid>
    {
      template< typename ParseInput >
      static bool apply(const ParseInput &in, Stow &stow)
      {
        auto ov = stow.findExpression(in.string());
        if (!ov)
        {
          std::ostringstream stream;
          stream << "Unknown Vector Variable: " << in.string();
          throw parse_error(stream.str(), in.position());
        }
        
        if (in.string() == stow.currentlyParsingExpression)
          throw parse_error("Expression Can't Reference Itself", in.position());
        
        if (std::find(stow.currentParsingDependents.begin(), stow.currentParsingDependents.end(), *ov) != stow.currentParsingDependents.end())
          throw parse_error("Cycle Rejected", in.position());
        
        if (stow.graph[*ov].valueType() == ValueType::Vector)
          return true;

        return false;
      }
    };
    
    template<> struct action<rid>
    {
      template< typename ParseInput >
      static bool apply(const ParseInput &in, Stow &stow)
      {
        auto ov = stow.findExpression(in.string());
        if (!ov)
        {
          std::ostringstream stream;
          stream << "Unknown Rotation Variable: " << in.string();
          throw parse_error(stream.str(), in.position());
        }
        
        if (in.string() == stow.currentlyParsingExpression)
          throw parse_error("Expression Can't Reference Itself", in.position());
        
        if (std::find(stow.currentParsingDependents.begin(), stow.currentParsingDependents.end(), *ov) != stow.currentParsingDependents.end())
          throw parse_error("Cycle Rejected", in.position());
        
        if (stow.graph[*ov].valueType() == ValueType::Rotation)
          return true;

        return false;
      }
    };
    
    template<> struct action<cid>
    {
      template< typename ParseInput >
      static bool apply(const ParseInput &in, Stow &stow)
      {
        auto ov = stow.findExpression(in.string());
        if (!ov)
        {
          std::ostringstream stream;
          stream << "Unknown CSys Variable: " << in.string();
          throw parse_error(stream.str(), in.position());
        }
        
        if (in.string() == stow.currentlyParsingExpression)
          throw parse_error("Expression Can't Reference Itself", in.position());
        
        if (std::find(stow.currentParsingDependents.begin(), stow.currentParsingDependents.end(), *ov) != stow.currentParsingDependents.end())
          throw parse_error("Cycle Rejected", in.position());
        
        if (stow.graph[*ov].valueType() == ValueType::CSys)
          return true;

        return false;
      }
    };
  }
  namespace slt
  {
    using namespace ::tao::pegtl;
    using namespace ::lcc::grm;
    
    template<typename Rule> struct selector : std::false_type{}; //default to exclude node

    //common
    template<> struct selector<lhsidentifier> : std::true_type{};
    template<> struct selector<axisx> : std::true_type{};
    template<> struct selector<axisy> : std::true_type{};
    template<> struct selector<axisz> : std::true_type{};
    template<> struct selector<axesxy> : std::true_type{};
    template<> struct selector<axesxz> : std::true_type{};
    template<> struct selector<axesyz> : std::true_type{};
    
    //scalar
    template<> struct selector<decimal> : std::true_type{};
    template<> struct selector<sid> : std::true_type{};
    template<> struct selector<sconstantpi> : std::true_type{};
    template<> struct selector<sconstante> : std::true_type{};
    template<> struct selector<sfunction1> : std::true_type{};
    template<> struct selector<ssin> : std::true_type{};
    template<> struct selector<scos> : std::true_type{};
    template<> struct selector<stan> : std::true_type{};
    template<> struct selector<sasin> : std::true_type{};
    template<> struct selector<sacos> : std::true_type{};
    template<> struct selector<satan> : std::true_type{};
    template<> struct selector<sradtodeg> : std::true_type{};
    template<> struct selector<sdegtorad> : std::true_type{};
    template<> struct selector<slog> : std::true_type{};
    template<> struct selector<sexp> : std::true_type{};
    template<> struct selector<ssqrt> : std::true_type{};
    template<> struct selector<sabs> : std::true_type{};
    template<> struct selector<sfunction2> : std::true_type{};
    template<> struct selector<spow> : std::true_type{};
    template<> struct selector<satan2> : std::true_type{};
    template<> struct selector<shypot> : std::true_type{};
    template<> struct selector<smin> : std::true_type{};
    template<> struct selector<smax> : std::true_type{};
    template<> struct selector<sfloor> : std::true_type{};
    template<> struct selector<sceil> : std::true_type{};
    template<> struct selector<sround> : std::true_type{};
    template<> struct selector<scondition> : std::true_type{};
    template<> struct selector<sgreaterequal> : std::true_type{};
    template<> struct selector<slessequal> : std::true_type{};
    template<> struct selector<sgreater> : std::true_type{};
    template<> struct selector<sless> : std::true_type{};
    template<> struct selector<sequal> : std::true_type{};
    template<> struct selector<snotequal> : std::true_type{};
    template<> struct selector<smultiplication> : std::true_type{};
    template<> struct selector<sdivision> : std::true_type{};
    template<> struct selector<saddition> : std::true_type{};
    template<> struct selector<ssubtraction> : std::true_type{};
    template<> struct selector<sterm> : std::true_type{};
    template<> struct selector<sparenth> : std::true_type{};
    template<> struct selector<srhs> : std::true_type{};
    template<> struct selector<tsrhs> : std::true_type{};
    
    //vector
    template<> struct selector<vid> : std::true_type{};
    template<> struct selector<vconstantzero> : std::true_type{};
    template<> struct selector<vscalars> : std::true_type{};
    template<> struct selector<vmultiplication> : std::true_type{};
    template<> struct selector<vdivision> : std::true_type{};
    template<> struct selector<vterm> : std::true_type{};
    template<> struct selector<vaddition> : std::true_type{};
    template<> struct selector<vsubtraction> : std::true_type{};
    template<> struct selector<vrhs> : std::true_type{};
    template<> struct selector<vparenth> : std::true_type{};
    template<> struct selector<tvrhs> : std::true_type{};
    template<> struct selector<vdecay> : std::true_type{};
    template<> struct selector<vlength> : std::true_type{};
    template<> struct selector<vdot> : std::true_type{};
    template<> struct selector<vnormalize> : std::true_type{};
    template<> struct selector<vcross> : std::true_type{};
    
    //rotation
    template<> struct selector<rid> : std::true_type{};
    template<> struct selector<rconstantzero> : std::true_type{};
    template<> struct selector<rscalars> : std::true_type{};
    template<> struct selector<rterm> : std::true_type{};
    template<> struct selector<rmultiplication> : std::true_type{};
    template<> struct selector<rdivision> : std::true_type{};
    template<> struct selector<rparenth> : std::true_type{};
    template<> struct selector<raxisangle> : std::true_type{};
    template<> struct selector<rfromto> : std::true_type{};
    template<> struct selector<rbasepairs> : std::true_type{};
    template<> struct selector<trrhs> : std::true_type{};
    template<> struct selector<rdecayaxis> : std::true_type{};
    template<> struct selector<rdecayangle> : std::true_type{};
    template<> struct selector<rdecayaxisvector> : std::true_type{};
    template<> struct selector<rinverse> : std::true_type{};
    template<> struct selector<rrotatevector> : std::true_type{};
    
    //csys
    template<> struct selector<cid> : std::true_type{};
    template<> struct selector<cconstantzero> : std::true_type{};
    template<> struct selector<cvectors> : std::true_type{};
    template<> struct selector<crotationorigin> : std::true_type{};
    template<> struct selector<cterm> : std::true_type{};
    template<> struct selector<cmultiplication> : std::true_type{};
    template<> struct selector<cdivision> : std::true_type{};
    template<> struct selector<cparenth> : std::true_type{};
    template<> struct selector<tcrhs> : std::true_type{};
    template<> struct selector<cinverse> : std::true_type{};
    template<> struct selector<cmap> : std::true_type{};
    template<> struct selector<cdecayrotation> : std::true_type{};
    template<> struct selector<cdecayorigin> : std::true_type{};
  }
}

#endif
