/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <fstream>
#include <limits>
#include <cmath>

#include <boost/graph/topological_sort.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <boost/math/constants/constants.hpp>

#include <tao/pegtl/contrib/parse_tree_to_dot.hpp>
#include <tao/pegtl/contrib/analyze.hpp>

#include "lccrules.h"
#include "lccstow.h"

namespace
{
  template <class GraphEW>
  class Edge_writer {
  public:
    Edge_writer(const GraphEW &graphEWIn) : graphEW(graphEWIn) {}
    template <class EdgeW>
    void operator()(std::ostream& out, const EdgeW& edgeW) const {
      out << "[label=\"";
      out << lcc::getEdgePropertyString(graphEW[edgeW].tag) << "\\n";
      out << "\"]";
    }
  private:
    const GraphEW &graphEW;
  };

  template <class GraphVW>
  class Vertex_writer {
  public:
    Vertex_writer(const GraphVW &graphVWIn) : graphVW(graphVWIn) {}
    template <class VertexW>
    void operator()(std::ostream& out, const VertexW& vertexW) const {
      out << "[label=\"";
      if (graphVW[vertexW].vertexType == lcc::VertexType::Expression)
      {
        out << graphVW[vertexW].name;
      }
      else
        out << lcc::getVertexTypeString(graphVW[vertexW].vertexType);
      out << "\\n" << ((graphVW[vertexW].dirty) ? ("dirty") : ("clean"));
      if (graphVW[vertexW].isClean())
      {
        std::string value;
        lcc::StringVisitor visitor(value);
        std::visit(visitor, graphVW[vertexW].value);
        out << "\\n" << value;
      }
      out << "\"";
      if (graphVW[vertexW].vertexType == lcc::VertexType::Expression)
      {
        out << "color=aquamarine style=filled";
      }
      out << "]";
    }
  private:
    const GraphVW &graphVW;
  };
  
  std::string prettyDouble(const double &dIn, int precision = std::numeric_limits<double>::digits10 - 1)
  {
    //std::numeric_limits<double>::digits10: gives '25.300000000000001' for '25.3'
    //std::numeric_limits<double>::digits10 - 1: gives '25.3' for '25.3'
    std::ostringstream stream;
    stream << std::setprecision(precision) << std::fixed << dIn;
    std::string out = stream.str();
    out.erase(out.find_last_not_of('0') + 1, std::string::npos);
    if (out.back() == '.')
      out += '0';
    return out;
  }
}

using namespace lcc;
namespace pegtl = tao::pegtl;

Stow::Stow()
{
  //debug helper.
  if(pegtl::analyze<lcc::grm::expression>() != 0)
    std::cout << "there are problems with the grammar" << std::endl;
  
  //constants need to be avoided for dirty visitor and updating.
  auto goConstant = [&](VertexType vt, const Variant &variant)
  {
    Vertex v = boost::add_vertex(VertexProperty(vt), graph);
    graph[v].value = variant;
    graph[v].constant = true;
    graph[v].setClean();
    constantsMap.insert({vt, v});
  };
  
  //build constant vertices.
  goConstant(VertexType::SConstantPI, boost::math::constants::pi<double>());
  goConstant(VertexType::SConstantE, boost::math::constants::e<double>());
  
  goConstant(VertexType::VConstantZero, Vector(0.0, 0.0, 0.0));
  goConstant(VertexType::VConstantX, Vector(1.0, 0.0, 0.0));
  goConstant(VertexType::VConstantY, Vector(0.0, 1.0, 0.0));
  goConstant(VertexType::VConstantZ, Vector(0.0, 0.0, 1.0));
  
  goConstant(VertexType::RConstantZero, Rotation::Identity());

  goConstant(VertexType::CConstantZero, CSys::Identity());
}

Result Stow::parse(const std::string &sIn)
{
  result = Result(); //reset result
  result.input = sIn;
  
  inputString = std::make_unique<pegtl::string_input<>>(sIn, "");
  try
  {
    setFilterOnlyDead();
    currentlyParsingExpression.clear();
    currentParsingDependents.clear();
    root = pegtl::parse_tree::parse<lcc::grm::expression, lcc::slt::selector, lcc::rls::action>(*inputString, *this);
    if (!root)
    {
      result.parseErrorMessage = "null parse tree";
      return result;
    }
    assert(root->children.size() == 2);
    assert(root->children.front()->is_type<grm::lhsidentifier>());
    assert(root->children.front()->has_content());
//     assert(root->children.back()->is_type<grm::rhs>());
    assert(root->children.back()->has_content());
    
    result.parsed = true;
    result.expressionName = root->children.front()->string();
  }
  catch(const pegtl::parse_error& e)
  {
    std::ostringstream stream;
    result.errorPosition = e.positions().front().column;
    stream << "parse failure: " << e.what() << " " << inputString->line_at(e.positions().front());
    result.parseErrorMessage = stream.str();
  }
  catch(...)
  {
    result.parseErrorMessage = "parse failure unknown";
  }
  return result;
}

Result Stow::commit()
{
  //update graph from parse tree. should be no exceptions thrown during this process.
  assert(root);
  
  std::optional<Vertex> expressionVertex = findExpression(root->children.front()->string_view());
  if (expressionVertex)
  {
    result.oldExpression = buildExpressionString(*expressionVertex);
    cleanExpression(*expressionVertex);
    markDependentsDirty(*expressionVertex);
  }
  else
    expressionVertex = buildExpression(root->children.front()->string_view());
  Vertex childVertex = goTRHS(root->children.back());

  assert(expressionVertex);
  connect(*expressionVertex, childVertex, EdgeTag::None);
  
  return result;
}

Result Stow::update()
{
  auto fg = buildIntrusiveGraph(graph);
  
  auto localUpdate = [&]()
  {
    std::vector<Vertex> vertices;
    boost::topological_sort(fg, std::back_inserter(vertices));
    
    for (auto v : vertices)
    {
      if (fg[v].isClean())
        continue;
      assert(updateFunctionMap.count(fg[v].vertexType) == 1);
      updateFunctionMap[fg[v].vertexType](v);
      if (fg[v].vertexType == VertexType::Expression)
        result.updatedIds.emplace_back(fg[v].id);
    }
  };
  
  //errors during parse can be ignored because we haven't changed the graph
  //when errors occur during update the graph has been changed
  //and we want to retain a valid state so 'undo'.
  auto undo = [&]()
  {
    std::optional<Vertex> expressionVertex = findExpression(result.expressionName);
    assert(expressionVertex);
    if (!result.oldExpression.empty())
    {
      cleanExpression(*expressionVertex);
      pegtl::string_input old(result.oldExpression, "");
      auto pt = pegtl::parse_tree::parse<lcc::grm::expression, lcc::slt::selector, lcc::rls::action>(old, *this);
      assert(pt);
      assert(pt->children.size() == 2);
      assert(pt->children.front()->is_type<grm::lhsidentifier>());
      assert(pt->children.front()->has_content());
      assert(pt->children.back()->has_content());
      Vertex childVertex = goTRHS(pt->children.back());
      connect(*expressionVertex, childVertex, EdgeTag::None);
      localUpdate();
    }
    else
    {
      removeExpression(*expressionVertex);
    }
  };
  
  try
  {
    localUpdate();
    
    //verify double values.
    for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
    {
      std::vector<double> out;
      DoubleVisitor vis(out);
      std::visit(vis, graph[*its.first].value);
      for (const auto &d : out)
      {
        int c = std::fpclassify(d);
        if (c != FP_NORMAL && c != FP_ZERO)
          throw std::runtime_error("Invalid Double Value Detected");
      }
    }
    
    result.updated = true;
    result.value = getValue(result.expressionName);
  }
  catch(const boost::not_a_dag &)
  {
    result.updateErrorMessage = "Graph is not a DAG";
    undo();
  }
  catch(const std::runtime_error &e)
  {
    std::ostringstream stream;
    stream << "update failure: " << e.what();
    result.updateErrorMessage = stream.str();
    undo();
  }
  catch(...)
  {
    result.updateErrorMessage = "update failure: unknown";
    undo();
  }
  
  return result;
}

void Stow::writeParseTree(const std::string &pathName)
{
  assert(root); //only call this on a successfully ran parse.
  std::ofstream fo(pathName);
  pegtl::parse_tree::print_dot(fo, *root);
  fo.close();
}

void Stow::writeOutGraph(const std::string &pathName)
{
  std::ofstream file(pathName);
  boost::write_graphviz(file, graph, Vertex_writer<Graph>(graph),
                        Edge_writer<Graph>(graph), boost::default_writer());
}

void Stow::exportExpressions(std::ostream &os) const
{
  auto fg = buildIntrusiveGraph(graph);
  
  std::vector<Vertex> vertices;
  try
  {
    boost::topological_sort(fg, std::back_inserter(vertices));
  }
  catch(const boost::not_a_dag &)
  {
    assert(0); //not a dag should be prevented
    return;
  }
  
  for (auto v : vertices)
  {
    if (fg[v].vertexType != VertexType::Expression)
      continue;
    os << buildExpressionString(v) << std::endl;
  }
}

void Stow::exportExpressions(std::ostream &os, const std::vector<int> &eIdsIn) const
{
  //requested expressions may depend on others. so we need to get dependents also.
  auto fg = buildIntrusiveGraph(graph);
  auto rg = boost::make_reverse_graph(fg);
  
  std::set<int> esOut;
  
  for (auto cId : eIdsIn)
  {
    esOut.insert(cId);
    
    auto ov = findExpression(cId);
    assert(ov);
    std::vector<Vertex> verts;
    AccumulateVisitor av(verts);
    boost::breadth_first_search(rg, *ov, boost::visitor(av));
    for (auto v : verts)
    {
      if (graph[v].vertexType == VertexType::Expression)
        esOut.insert(graph[v].id);
    }
  }
  
  //now we should have all expressions needed for export, just have to make sure the order works for import.
  std::vector<Vertex> vertices;
  try
  {
    boost::topological_sort(fg, std::back_inserter(vertices));
  }
  catch(const boost::not_a_dag &)
  {
    assert(0); //not a dag should be prevented
    return;
  }
  
  for (auto v : vertices)
  {
    if (esOut.count(fg[v].id) != 0)
      os << buildExpressionString(v) << std::endl;
  }
}

std::vector<Result> Stow::importExpressions(std::istream &is)
{
  std::vector<Result> out;
  
  std::string lineBuffer;
  while(std::getline(is, lineBuffer))
  {
    if(lineBuffer.empty())
      continue;
    auto result = parse(lineBuffer);
    if (result.parsed)
    {
      commit();
      result = update();
    }
    out.emplace_back(result);
  }
  
  return out;
}

Serial Stow::serialOut() const
{
  Serial out;
  
  auto fg = buildIntrusiveGraph(graph);
  std::vector<Vertex> vertices;
  try
  {
    boost::topological_sort(fg, std::back_inserter(vertices));
  }
  catch(const boost::not_a_dag &)
  {
    assert(0); //not a dag should be prevented
    return out;
  }
  for (auto v : vertices)
  {
    if (fg[v].vertexType != VertexType::Expression)
      continue;
    out.expressions.push_back({fg[v].id, buildExpressionString(v)});
  }
  
  for (const auto &g : groups)
    out.groups.push_back(SGroup{g.getId(), g.name, g.getExpressionIds()});
  
  return out;
}

std::vector<Result> Stow::serialIn(const Serial &sIn)
{
  std::vector<Result> out;
  
  for (const auto &se : sIn.expressions)
  {
    auto result = parse(se.expression);
    if (!result.parsed)
    {
      out.emplace_back(result);
      continue;
    }
    commit();
    result = update();
    if (!result.updated)
    {
      out.emplace_back(result);
      continue;
    }
    auto vertex = findExpression(result.expressionName);
    if (vertex)
      graph[*vertex].id = se.id;
  }
  
  for (const auto &sg : sIn.groups)
  {
    groups.emplace_back(sg.name, sg.id);
    for (const auto &eId : sg.expressionIds)
      groups.back().addExpression(eId);
  }
  
  deriveNextExpressionId();
  deriveNextGroupId();
  
  return out;
}

void Stow::deriveNextExpressionId()
{
  nextExpressionId = 0;
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType != VertexType::Expression)
      continue;
    nextExpressionId = std::max(nextExpressionId, fg[*its.first].id);
  }
  nextExpressionId++;
}

void Stow::deriveNextGroupId()
{
  nextGroupId = 0;
  for (const auto &g : groups)
    nextGroupId = std::max(nextGroupId, g.getId());
  nextGroupId++;
}

std::vector<std::string> Stow::getExpressionNames() const
{
  std::vector<std::string> out;
  
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression)
      out.emplace_back(fg[*its.first].name);
  }
  
  return out;
}

std::vector<int> Stow::getExpressionIds() const
{
  std::vector<int> out;
  
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression)
      out.emplace_back(fg[*its.first].id);
  }
  
  return out;
}

bool Stow::hasExpression(const std::string &nameIn) const
{
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression && fg[*its.first].name == nameIn)
      return true;
  }
  return false;
}

bool Stow::hasExpression(int idIn) const
{
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression && fg[*its.first].id == idIn)
      return true;
  }
  return false;
}

std::optional<int> Stow::getExpressionId(const std::string &nameIn) const
{
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression && fg[*its.first].name == nameIn)
      return fg[*its.first].id;
  }
  return std::nullopt;
}

std::optional<std::string> Stow::getExpressionName(int idIn) const
{
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression && fg[*its.first].id == idIn)
      return fg[*its.first].name;
  }
  return std::nullopt;
}

bool Stow::hasGroup(const std::string &nameIn) const
{
  for (const auto &g : groups)
  {
    if (g.name == nameIn)
      return true;
  }
  return false;
}

bool Stow::hasGroup(int idIn) const
{
  for (const auto &g : groups)
  {
    if (g.getId() == idIn)
      return true;
  }
  return false;
}

int Stow::addGroup(const std::string &nameIn)
{
  //assumes caller has verified name is unique.
  assert (!hasGroup(nameIn));
  
  int id = nextGroupId++;
  groups.emplace_back(nameIn, id);
  return id;
}

void Stow::removeGroup(int idIn)
{
  auto it = groups.begin();
  for (; it != groups.end(); ++it)
  {
    if (it->getId() == idIn)
      break;
  }
  if (it != groups.end())
    groups.erase(it);
}

void Stow::removeGroup(const std::string &nameIn)
{
  auto it = groups.begin();
  for (; it != groups.end(); ++it)
  {
    if (it->name == nameIn)
      break;
  }
  if (it != groups.end())
    groups.erase(it);
}

std::vector<int> Stow::getGroupIds() const
{
  std::vector<int> out;
  for (const auto &g : groups)
    out.emplace_back(g.getId());
  return out;
}

std::vector<std::string> Stow::getGroupNames() const
{
  std::vector<std::string> out;
  for (const auto &g : groups)
    out.emplace_back(g.name);
  return out;
}

std::optional<std::string> Stow::getGroupName(int idIn) const
{
  for (const auto &g : groups)
  {
    if (g.getId() == idIn)
      return g.name;
  }
  return std::nullopt;
}

std::optional<int> Stow::getGroupId(const std::string &nameIn) const
{
  for (const auto &g : groups)
  {
    if (g.name == nameIn)
      return g.getId();
  }
  return std::nullopt;
}

void Stow::renameGroup(int idIn, const std::string &newNameIn)
{
  for (auto &g : groups)
  {
    if (g.getId() == idIn)
    {
      g.name = newNameIn;
      break;
    }
  }
}

void Stow::addExpressionToGroup(int group, int expression)
{
  for (auto &g : groups)
  {
    if (g.getId() == group)
    {
      g.addExpression(expression);
      break;
    }
  }
}

void Stow::removeExpressionFromGroup(int group, int expression)
{
  for (auto &g : groups)
  {
    if (g.getId() == group)
    {
      g.removeExpression(expression);
      break;
    }
  }
}

bool Stow::groupHasExpression(int group, int expression) const
{
  for (const auto &g : groups)
  {
    if (g.getId() != group)
      continue;
    return g.hasExpression(expression);
  }
  return false;
}

const std::vector<int>& Stow::getExpressionsOfGroup(int group) const
{
  for (const auto &g : groups)
  {
    if (g.getId() != group)
      continue;
    return g.getExpressionIds();
  }
  
  static std::vector<int> dummy;
  return dummy;
}

std::optional<Vertex> Stow::findExpression(const std::string &nIn) const
{
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression && fg[*its.first].name == nIn)
      return *its.first;
  }
  
  return std::nullopt;
}

std::optional<Vertex> Stow::findExpression(int idIn) const
{
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression && fg[*its.first].id == idIn)
      return *its.first;
  }
  
  return std::nullopt;
}

std::optional<Vertex> Stow::findExpression(std::string_view nIn) const
{
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::vertices(fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].vertexType == VertexType::Expression && fg[*its.first].name == nIn)
      return *its.first;
  }
  
  return std::nullopt;
}

Vertex Stow::buildExpression(std::string_view stringIn)
{
  Vertex out = boost::add_vertex(VertexProperty(VertexType::Expression), graph);
  graph[out].name = stringIn;
  graph[out].id = nextExpressionId++;
  return out;
}

void Stow::cleanExpression(Vertex ev)
{
  //only dead should be default state.
  setFilterConstants();
  setFilterExpressions();
  graph[ev].filterMe = false;
  auto fg = buildIntrusiveGraph(graph);
  
  std::vector<Vertex> vertices;
  AccumulateVisitor visitor(vertices);
  boost::breadth_first_search(fg, ev, boost::visitor(visitor));
  
  assert(!vertices.empty());
  auto it = vertices.begin() + 1;
  for (; it != vertices.end(); ++it)
  {
    boost::clear_vertex(*it, graph);
    graph[*it].alive = false;
  }
  
  //if expression is connected to a constant, the constant will filtered out of bfs
  //and we will be left with the edge connecting the expression to the constant. this will fix that case.
  boost::clear_out_edges(ev, graph);
  
  graph[ev].alive = true; //expression vertex itself shouldn't be set to false. bfs visitor does this.
  setFilterOnlyDead();
}

void Stow::removeExpression(const std::string &sIn)
{
  std::optional<Vertex> expressionVertex = findExpression(sIn);
  if (expressionVertex)
    removeExpression(*expressionVertex);
}

void Stow::removeExpression(int idIn)
{
  std::optional<Vertex> expressionVertex = findExpression(idIn);
  if (expressionVertex)
    removeExpression(*expressionVertex);
}

void Stow::removeExpression(Vertex ev)
{
  cleanExpression(ev);
  
  if (boost::in_degree(ev, graph) > 0)
  {
    assert(graph[ev].isClean());
    std::optional<Vertex> replacement;
    if (graph[ev].valueType() == ValueType::Scalar)
      replacement = buildSConstant(std::get<double>(graph[ev].value));
    else if (graph[ev].valueType() == ValueType::Vector)
      replacement = buildVConstant(std::get<Vector>(graph[ev].value));
    else if (graph[ev].valueType() == ValueType::Rotation)
      replacement = buildRConstant(std::get<Rotation>(graph[ev].value));
    else if (graph[ev].valueType() == ValueType::CSys)
      replacement = buildCConstant(std::get<CSys>(graph[ev].value));
    assert(replacement);
    
    //shouldn't need a filtered graph. Edges are removed.
    for (auto its = boost::in_edges(ev, graph); its.first != its.second; ++its.first)
      connect(boost::source(*its.first, graph), *replacement, graph[*its.first].tag);
  }
  
  //remove expression from any groups
  for (auto &g : groups)
    g.removeExpression(graph[ev].id);
  
  boost::clear_vertex(ev, graph);
  graph[ev].alive = false;
  setFilterOnlyDead();
}

void Stow::renameExpression(const std::string &oldName, const std::string &newName)
{
  std::optional<Vertex> expressionVertex = findExpression(oldName);
  if (expressionVertex)
    graph[*expressionVertex].name = newName;
}

void Stow::renameExpression(int idIn, const std::string &newName)
{
  std::optional<Vertex> expressionVertex = findExpression(idIn);
  if (expressionVertex)
    graph[*expressionVertex].name = newName;
}

void Stow::markDependentsDirty(Vertex v)
{
  auto fg = buildIntrusiveGraph(graph);
  auto rg = boost::make_reverse_graph(fg);
  
  std::vector<Vertex> verts;
  AccumulateVisitor av(verts);
  boost::breadth_first_search(rg, v, boost::visitor(av));
  
  for (auto v : verts)
    graph[v].dirty = true;
}

std::vector<Vertex> Stow::getDependents(Vertex v)
{
  auto fg = buildIntrusiveGraph(graph);
  auto rg = boost::make_reverse_graph(fg);
  
  std::vector<Vertex> verts;
  AccumulateVisitor av(verts);
  boost::breadth_first_search(rg, v, boost::visitor(av));
  
  return verts;
}

void Stow::setFilterOnlyDead()
{
  for (auto its = boost::vertices(graph); its.first != its.second; ++its.first)
    graph[*its.first].filterMe = !graph[*its.first].alive;
  for (auto its = boost::edges(graph); its.first != its.second; ++its.first)
    graph[*its.first].filterMe = false;
}

void Stow::setFilterConstants()
{
  for (auto its = boost::vertices(graph); its.first != its.second; ++its.first)
  {
    if (constantsMap.count(graph[*its.first].vertexType) != 0)
      graph[*its.first].filterMe = true;
  }
}

void Stow::setFilterExpressions()
{
  for (auto its = boost::vertices(graph); its.first != its.second; ++its.first)
  {
    if (graph[*its.first].vertexType == VertexType::Expression)
      graph[*its.first].filterMe = true;
  }
}

Edge Stow::connect(Vertex sourceIn, Vertex targetIn, EdgeTag ep)
{
  bool results;
  Edge edge;
  boost::tie(edge, results) = boost::add_edge(sourceIn, targetIn, graph);
  assert(results);
  graph[edge].tag = ep;
  return edge;
}

std::optional<Vertex> Stow::findChildOfTag(Vertex parent, EdgeTag ep) const
{
  auto fg = buildIntrusiveGraph(graph);
  for (auto its = boost::out_edges(parent, fg); its.first != its.second; ++its.first)
  {
    if (fg[*its.first].tag == ep)
      return boost::target(*its.first, fg);
  }
  
  return std::nullopt;
}

std::string Stow::buildExpressionString(const std::string &sIn) const
{
  std::optional<Vertex> expressionVertex = findExpression(sIn);
  if (expressionVertex)
    return buildExpressionString(*expressionVertex);
  
  return std::string();
}

std::string Stow::buildExpressionString(int idIn) const
{
  std::optional<Vertex> expressionVertex = findExpression(idIn);
  if (expressionVertex)
    return buildExpressionString(*expressionVertex);
  
  return std::string();
}

std::string Stow::buildExpressionString(Vertex v) const
{
  std::string out = graph[v].name + " = ";
  auto oChild = findChildOfTag(v, EdgeTag::None);
  assert(oChild);
  assert(stringFunctionMap.count(graph[*oChild].vertexType) == 1);
  out += stringFunctionMap.at(graph[*oChild].vertexType)(*oChild);
  
  return out;
}

std::vector<double> Stow::getValue(const std::string &sIn)
{
  std::optional<Vertex> expressionVertex = findExpression(sIn);
  if (expressionVertex)
    return getValue(*expressionVertex);
  return std::vector<double>();
}

std::vector<double> Stow::getValue(int idIn)
{
  std::optional<Vertex> expressionVertex = findExpression(idIn);
  if (expressionVertex)
    return getValue(*expressionVertex);
  return std::vector<double>();
}

std::vector<double> Stow::getValue(Vertex v)
{
  assert(graph[v].alive);
  assert(graph[v].isClean());
  
  std::vector<double> out;
  DoubleVisitor vis(out);
  std::visit(vis, graph[v].value);
  return out;
}

Vertex Stow::goTRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(buildFunctionMap.count(nIn->type) == 1);
  return buildFunctionMap[nIn->type](nIn);
}

Vertex Stow::buildSConstant(const double &constantIn)
{
  Vertex out = boost::add_vertex(VertexProperty(VertexType::SConstant), graph);
  graph[out].value = constantIn;
  graph[out].setClean(); //constants are always clean;
  return out;
}

Vertex Stow::goTSRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  return goSRHS(nIn);
}

Vertex Stow::goSParenth(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  auto out = boost::add_vertex(VertexProperty(VertexType::SParenth), graph);
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.front()->type) == 1);
  Vertex child = buildFunctionMap[nIn->children.front()->type](nIn->children.front());
  connect(out, child, EdgeTag::None);
  return out;
}

Vertex Stow::goSFunction1(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.front()->type) == 1);
  assert(buildFunctionMap.count(nIn->children.back()->type) == 1);
  Vertex f = buildFunctionMap[nIn->children.front()->type](nIn->children.front());
  Vertex arg = buildFunctionMap[nIn->children.back()->type](nIn->children.back());
  connect(f, arg, EdgeTag::None);
  return f;
}

Vertex Stow::goSSin(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SSin), graph);
}

Vertex Stow::goSCos(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SCos), graph);
}

Vertex Stow::goSTan(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::STan), graph);
}

Vertex Stow::goSAsin(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SAsin), graph);
}

Vertex Stow::goSAcos(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SAcos), graph);
}

Vertex Stow::goSAtan(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SAtan), graph);
}

Vertex Stow::goSRadToDeg(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SRadToDeg), graph);
}

Vertex Stow::goSDegToRad(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SDegToRad), graph);
}

Vertex Stow::goSLog(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SLog), graph);
}

Vertex Stow::goSExp(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SExp), graph);
}

Vertex Stow::goSSqrt(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SSqrt), graph);
}

Vertex Stow::goSAbs(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SAbs), graph);
}

Vertex Stow::goSFunction2(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 3);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(2)->type) == 1);
  
  Vertex f = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex arg1 = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  Vertex arg2 = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(2));
  connect(f, arg1, EdgeTag::Parameter1);
  connect(f, arg2, EdgeTag::Parameter2);
  return f;
}

Vertex Stow::goSPow(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SPow), graph);
}

Vertex Stow::goSAtan2(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SAtan2), graph);
}

Vertex Stow::goSHypot(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SHypot), graph);
}

Vertex Stow::goSMin(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SMin), graph);
}

Vertex Stow::goSMax(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SMax), graph);
}

Vertex Stow::goSFloor(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SFloor), graph);
}

Vertex Stow::goSCeil(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SCeil), graph);
}

Vertex Stow::goSRound(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SRound), graph);
}

Vertex Stow::goSCondition(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 5);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  //no vertex for condition.
  assert(buildFunctionMap.count(nIn->children.at(2)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(3)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(4)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::SCondition), graph);
  
  Vertex lhs = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  
  EdgeTag condition = EdgeTag::None;
  auto ct = nIn->children.at(1)->type;
  if (ct == ::tao::pegtl::demangle<grm::sgreaterequal>()) condition = EdgeTag::GreaterEqual;
  else if (ct == ::tao::pegtl::demangle<grm::slessequal>()) condition = EdgeTag::LessEqual;
  else if (ct == ::tao::pegtl::demangle<grm::sgreater>()) condition = EdgeTag::Greater;
  else if (ct == ::tao::pegtl::demangle<grm::sless>()) condition = EdgeTag::Less;
  else if (ct == ::tao::pegtl::demangle<grm::sequal>()) condition = EdgeTag::Equal;
  else if (ct == ::tao::pegtl::demangle<grm::snotequal>()) condition = EdgeTag::NotEqual;
  assert(condition != EdgeTag::None);
  
  Vertex rhs = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(2));
  Vertex vThen = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(3));
  Vertex vElse = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(4));
  connect(out, lhs, condition);
  connect(out, rhs, EdgeTag::Rhs);
  connect(out, vThen, EdgeTag::Then);
  connect(out, vElse, EdgeTag::Else);
  
  return out;
}

Vertex Stow::goSRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  std::optional<Vertex> currentLHS;
  for (const auto &c : nIn->children)
  {
    assert(buildFunctionMap.count(c->type) == 1);
    Vertex current = buildFunctionMap[c->type](c);
    if (c->type == ::tao::pegtl::demangle<grm::saddition>() || c->type == ::tao::pegtl::demangle<grm::ssubtraction>())
    {
      assert(currentLHS);
      connect(current, *currentLHS, EdgeTag::Lhs);
      currentLHS = current;
    }
    else
    {
      if (currentLHS)
      {
        assert(graph[*currentLHS].vertexType == VertexType::SAddition || graph[*currentLHS].vertexType == VertexType::SSubtraction);
        connect(*currentLHS, current, EdgeTag::Rhs);
      }
      else
        currentLHS = current;
    }
  }
  assert(currentLHS);
  return *currentLHS;
}

Vertex Stow::goSAddition(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SAddition), graph);
}

Vertex Stow::goSSubtraction(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SSubtraction), graph);
}

Vertex Stow::goSTerm(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  std::optional<Vertex> currentLHS;
  for (const auto &c : nIn->children)
  {
    assert(buildFunctionMap.count(c->type) == 1);
    Vertex current = buildFunctionMap[c->type](c);
    if (c->type == ::tao::pegtl::demangle<grm::smultiplication>() || c->type == ::tao::pegtl::demangle<grm::sdivision>())
    {
      assert(currentLHS);
      connect(current, *currentLHS, EdgeTag::Lhs);
      currentLHS = current;
    }
    else
    {
      if (currentLHS)
      {
        assert(graph[*currentLHS].vertexType == VertexType::SMultiplication || graph[*currentLHS].vertexType == VertexType::SDivision);
        connect(*currentLHS, current, EdgeTag::Rhs);
      }
      else
        currentLHS = current;
    }
  }
  assert(currentLHS);
  return *currentLHS;
}

Vertex Stow::goSMultiplication(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SMultiplication), graph);
}

Vertex Stow::goSDivision(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::SDivision), graph);
}

Vertex Stow::goSID(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  auto e = findExpression(nIn->string());
  assert(e);
  assert(graph[*e].valueType() == ValueType::Scalar);
  assert(graph[*e].isClean());
  return *e;
}

Vertex Stow::goSConstantPI(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  assert(constantsMap.count(VertexType::SConstantPI) == 1);
  return constantsMap.at(VertexType::SConstantPI);
}

Vertex Stow::goSConstantE(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  assert(constantsMap.count(VertexType::SConstantE) == 1);
  return constantsMap.at(VertexType::SConstantE);
}

Vertex Stow::goDecimal(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  return buildSConstant(std::stod(nIn->string()));
}

Vertex Stow::buildVConstant(const Vector &vIn)
{
  Vertex out = boost::add_vertex(VertexProperty(VertexType::VScalars), graph);
  Vertex x = buildSConstant(vIn.x());
  Vertex y = buildSConstant(vIn.y());
  Vertex z = buildSConstant(vIn.z());
  
  connect(out, x, EdgeTag::X);
  connect(out, y, EdgeTag::Y);
  connect(out, z, EdgeTag::Z);
  return out;
}

Vertex Stow::goTVRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  return goVRHS(nIn);
}

Vertex Stow::goVRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  std::optional<Vertex> currentLHS;
  for (const auto &c : nIn->children)
  {
    assert(buildFunctionMap.count(c->type) == 1);
    Vertex current = buildFunctionMap[c->type](c);
    if (c->type == ::tao::pegtl::demangle<grm::vaddition>() || c->type == ::tao::pegtl::demangle<grm::vsubtraction>())
    {
      assert(currentLHS);
      connect(current, *currentLHS, EdgeTag::Lhs);
      currentLHS = current;
    }
    else
    {
      if (currentLHS)
      {
        assert(graph[*currentLHS].vertexType == VertexType::VAddition || graph[*currentLHS].vertexType == VertexType::VSubtraction);
        connect(*currentLHS, current, EdgeTag::Rhs);
      }
      else
        currentLHS = current;
    }
  }
  assert(currentLHS);
  return *currentLHS;
}

Vertex Stow::goVScalars(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 3);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(2)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::VScalars), graph);
  Vertex x = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex y = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  Vertex z = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(2));
  
  connect(out, x, EdgeTag::X);
  connect(out, y, EdgeTag::Y);
  connect(out, z, EdgeTag::Z);
  return out;
}

Vertex Stow::goVID(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  auto e = findExpression(nIn->string());
  assert(e);
  assert(graph[*e].valueType() == ValueType::Vector);
  assert(graph[*e].isClean());
  return *e;
}

Vertex Stow::goVConstantX(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  assert(constantsMap.count(VertexType::VConstantX) == 1);
  return constantsMap.at(VertexType::VConstantX);
}

Vertex Stow::goVConstantY(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  assert(constantsMap.count(VertexType::VConstantY) == 1);
  return constantsMap.at(VertexType::VConstantY);
}

Vertex Stow::goVConstantZ(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  assert(constantsMap.count(VertexType::VConstantZ) == 1);
  return constantsMap.at(VertexType::VConstantZ);
}

Vertex Stow::goVConstantZero(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  assert(constantsMap.count(VertexType::VConstantZero) == 1);
  return constantsMap.at(VertexType::VConstantZero);
}

Vertex Stow::goVDecay(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  
  //we don't need the constant axis node itself, but we need a graph edge to attach axis designation.
  Vertex out = boost::add_vertex(VertexProperty(VertexType::VDecay), graph);
  Vertex axis = buildFunctionMap[nIn->children.front()->type](nIn->children.front());
  Vertex vrhs = buildFunctionMap[nIn->children.back()->type](nIn->children.back());
  
  if (nIn->children.front()->type == ::tao::pegtl::demangle<grm::axisx>())
    connect(out, axis, EdgeTag::X);
  if (nIn->children.front()->type == ::tao::pegtl::demangle<grm::axisy>())
    connect(out, axis, EdgeTag::Y);
  if (nIn->children.front()->type == ::tao::pegtl::demangle<grm::axisz>())
    connect(out, axis, EdgeTag::Z);
  
  connect(out, vrhs, EdgeTag::Rhs);
  
  return out;
}

Vertex Stow::goVLength(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::VLength), graph);
  Vertex vector = buildFunctionMap[nIn->children.front()->type](nIn->children.front());
  
  connect(out, vector, EdgeTag::Vector);
  
  return out;
}

Vertex Stow::goVDot(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::VDot), graph);
  Vertex vector1 = buildFunctionMap[nIn->children.front()->type](nIn->children.front());
  Vertex vector2 = buildFunctionMap[nIn->children.back()->type](nIn->children.back());
  
  connect(out, vector1, EdgeTag::Parameter1);
  connect(out, vector2, EdgeTag::Parameter2);
  
  return out;
}

Vertex Stow::goVNormalize(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::VNormalize), graph);
  Vertex vector1 = buildFunctionMap[nIn->children.front()->type](nIn->children.front());
  
  connect(out, vector1, EdgeTag::Vector);
  
  return out;
}

Vertex Stow::goVCross(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::VCross), graph);
  Vertex vector1 = buildFunctionMap[nIn->children.front()->type](nIn->children.front());
  Vertex vector2 = buildFunctionMap[nIn->children.back()->type](nIn->children.back());
  
  connect(out, vector1, EdgeTag::Parameter1);
  connect(out, vector2, EdgeTag::Parameter2);
  
  return out;
}

Vertex Stow::goVTerm(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  std::optional<Vertex> currentLHS;
  for (const auto &c : nIn->children)
  {
    assert(buildFunctionMap.count(c->type) == 1);
    Vertex current = buildFunctionMap[c->type](c);
    if (c->type == ::tao::pegtl::demangle<grm::vmultiplication>() || c->type == ::tao::pegtl::demangle<grm::vdivision>())
    {
      assert(currentLHS);
      connect(current, *currentLHS, EdgeTag::Lhs);
      currentLHS = current;
    }
    else
    {
      if (currentLHS)
      {
        assert(graph[*currentLHS].vertexType == VertexType::VMultiplication || graph[*currentLHS].vertexType == VertexType::VDivision);
        connect(*currentLHS, current, EdgeTag::Rhs);
      }
      else
        currentLHS = current;
    }
  }
  assert(currentLHS);
  return *currentLHS;
}

Vertex Stow::goVMultiplication(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::VMultiplication), graph);
}

Vertex Stow::goVDivision(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::VDivision), graph);
}

Vertex Stow::goVAddition(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::VAddition), graph);
}

Vertex Stow::goVSubtraction(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::VSubtraction), graph);
}

Vertex Stow::goVParenth(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  auto out = boost::add_vertex(VertexProperty(VertexType::VParenth), graph);
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.front()->type) == 1);
  Vertex child = buildFunctionMap[nIn->children.front()->type](nIn->children.front());
  connect(out, child, EdgeTag::None);
  return out;
}

Vertex Stow::buildRConstant(const Rotation &rIn)
{
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RScalars), graph);
  
  Vertex x = buildSConstant(rIn.x());
  Vertex y = buildSConstant(rIn.y());
  Vertex z = buildSConstant(rIn.z());
  Vertex w = buildSConstant(rIn.w());
  
  connect(out, x, EdgeTag::X);
  connect(out, y, EdgeTag::Y);
  connect(out, z, EdgeTag::Z);
  connect(out, w, EdgeTag::W);
  
  return out;
}

Vertex Stow::goTRRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  return goRRHS(nIn);
}

Vertex Stow::goRRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  return goRTerm(nIn);
}

Vertex Stow::goRConstantZero(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  assert(constantsMap.count(VertexType::RConstantZero) == 1);
  return constantsMap.at(VertexType::RConstantZero);
}

Vertex Stow::goRScalars(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 4);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(2)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(3)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RScalars), graph);
  Vertex x = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex y = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  Vertex z = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(2));
  Vertex w = buildFunctionMap[nIn->children.at(3)->type](nIn->children.at(3));
  
  connect(out, x, EdgeTag::X);
  connect(out, y, EdgeTag::Y);
  connect(out, z, EdgeTag::Z);
  connect(out, w, EdgeTag::W);
  return out;
}

Vertex Stow::goRID(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  auto e = findExpression(nIn->string());
  assert(e);
  assert(graph[*e].valueType() == ValueType::Rotation);
  assert(graph[*e].isClean());
  return *e;
}

Vertex Stow::goRAxisAngle(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RAxisAngle), graph);
  Vertex vector = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex angle = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  
  connect(out, vector, EdgeTag::Axis);
  connect(out, angle, EdgeTag::Angle);
  return out;
}

Vertex Stow::goRFromTo(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RFromTo), graph);
  Vertex vector1 = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex vector2 = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  
  connect(out, vector1, EdgeTag::From);
  connect(out, vector2, EdgeTag::To);
  return out;
}

Vertex Stow::goRBasePairs(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 3);
//   assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(2)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RBasePairs), graph);
  Vertex vector1 = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  Vertex vector2 = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(2));
  
  std::optional<EdgeTag>op1;
  std::optional<EdgeTag>op2;
  if (nIn->children.at(0)->type == ::tao::pegtl::demangle<grm::axesxy>())
  {
    op1 = EdgeTag::X;
    op2 = EdgeTag::Y;
  }
  else if (nIn->children.at(0)->type == ::tao::pegtl::demangle<grm::axesxz>())
  {
    op1 = EdgeTag::X;
    op2 = EdgeTag::Z;
  }
  else if (nIn->children.at(0)->type == ::tao::pegtl::demangle<grm::axesyz>())
  {
    op1 = EdgeTag::Y;
    op2 = EdgeTag::Z;
  }
  assert(op1 && op2);
  
  connect(out, vector1, *op1);
  connect(out, vector2, *op2);
  return out;
}

Vertex Stow::goRDecayAxis(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RDecayAxis), graph);
  Vertex rotation = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  
  connect(out, rotation, EdgeTag::None);
  return out;
}

Vertex Stow::goRDecayAngle(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RDecayAngle), graph);
  Vertex rotation = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  
  connect(out, rotation, EdgeTag::None);
  return out;
}

Vertex Stow::goRDecayAxisVector(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  //don't need the vconstant nodes, but going to build anyway. makes update easier.
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RDecayAxisVector), graph);
  Vertex vconstant = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  
  std::optional<EdgeTag> oep;
  
  if (nIn->children.front()->type == ::tao::pegtl::demangle<grm::axisx>())
    oep = EdgeTag::X;
  if (nIn->children.front()->type == ::tao::pegtl::demangle<grm::axisy>())
    oep = EdgeTag::Y;
  if (nIn->children.front()->type == ::tao::pegtl::demangle<grm::axisz>())
    oep = EdgeTag::Z;
  assert(oep);
  connect(out, vconstant, *oep);
  
  Vertex rhs = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  connect(out, rhs, EdgeTag::Rotation);
  
  return out;
}

Vertex Stow::goRInverse(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RInverse), graph);
  Vertex rotation = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  
  connect(out, rotation, EdgeTag::Rotation);
  
  return out;
}

Vertex Stow::goRRotateVector(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::RRotateVector), graph);
  Vertex rotation = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex vector = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  
  connect(out, rotation, EdgeTag::Rotation);
  connect(out, vector, EdgeTag::Vector);
  
  return out;
}

Vertex Stow::goRTerm(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  std::optional<Vertex> currentLHS;
  for (const auto &c : nIn->children)
  {
    assert(buildFunctionMap.count(c->type) == 1);
    Vertex current = buildFunctionMap[c->type](c);
    if (c->type == ::tao::pegtl::demangle<grm::rmultiplication>() || c->type == ::tao::pegtl::demangle<grm::rdivision>())
    {
      assert(currentLHS);
      connect(current, *currentLHS, EdgeTag::Lhs);
      currentLHS = current;
    }
    else
    {
      if (currentLHS)
      {
        assert(graph[*currentLHS].vertexType == VertexType::RMultiplication || graph[*currentLHS].vertexType == VertexType::RDivision);
        connect(*currentLHS, current, EdgeTag::Rhs);
      }
      else
        currentLHS = current;
    }
  }
  assert(currentLHS);
  return *currentLHS;
}

Vertex Stow::goRMultiplication(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::RMultiplication), graph);
}

Vertex Stow::goRDivision(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::RDivision), graph);
}

Vertex Stow::goRParenth(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  auto out = boost::add_vertex(VertexProperty(VertexType::RParenth), graph);
  Vertex child = goRRHS(nIn);
  connect(out, child, EdgeTag::None);
  return out;
}

Vertex Stow::buildCConstant(const CSys &sysIn)
{
  Vertex out = boost::add_vertex(VertexProperty(VertexType::CRotationOrigin), graph);
  Vertex rotation = buildRConstant(Rotation(sysIn.linear()));
  Vertex origin = buildVConstant(sysIn.translation());
  
  connect(out, rotation, EdgeTag::Rotation);
  connect(out, origin, EdgeTag::Origin);
  return out;
}

Vertex Stow::goTCRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  return goCRHS(nIn);
}

Vertex Stow::goCRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  return goCTerm(nIn);
}

Vertex Stow::goCVectors(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 4);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(2)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(3)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::CVectors), graph);
  Vertex x = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex y = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  Vertex z = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(2));
  Vertex origin = buildFunctionMap[nIn->children.at(3)->type](nIn->children.at(3));
  
  connect(out, x, EdgeTag::X);
  connect(out, y, EdgeTag::Y);
  connect(out, z, EdgeTag::Z);
  connect(out, origin, EdgeTag::Origin);
  return out;
}

Vertex Stow::goCID(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  auto e = findExpression(nIn->string());
  assert(e);
  assert(graph[*e].valueType() == ValueType::CSys);
  assert(graph[*e].isClean());
  return *e;
}

Vertex Stow::goCRotationOrigin(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 2);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::CRotationOrigin), graph);
  Vertex rotation = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex origin = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  
  connect(out, rotation, EdgeTag::Rotation);
  connect(out, origin, EdgeTag::Origin);
  return out;
}

Vertex Stow::goCConstantZero(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  assert(constantsMap.count(VertexType::CConstantZero) == 1);
  return constantsMap.at(VertexType::CConstantZero);
}

Vertex Stow::goCInverse(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::CInverse), graph);
  Vertex sys = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  
  connect(out, sys, EdgeTag::None);
  
  return out;
}

Vertex Stow::goCMap(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 3);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(1)->type) == 1);
  assert(buildFunctionMap.count(nIn->children.at(2)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::CMap), graph);
  Vertex from = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  Vertex to = buildFunctionMap[nIn->children.at(1)->type](nIn->children.at(1));
  Vertex vec = buildFunctionMap[nIn->children.at(2)->type](nIn->children.at(2));
  
  connect(out, from, EdgeTag::From);
  connect(out, to, EdgeTag::To);
  connect(out, vec, EdgeTag::Vector);
  
  return out;
}

Vertex Stow::goCTerm(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  std::optional<Vertex> currentLHS;
  for (const auto &c : nIn->children)
  {
    assert(buildFunctionMap.count(c->type) == 1);
    Vertex current = buildFunctionMap[c->type](c);
    if (c->type == ::tao::pegtl::demangle<grm::cmultiplication>() || c->type == ::tao::pegtl::demangle<grm::cdivision>())
    {
      assert(currentLHS);
      connect(current, *currentLHS, EdgeTag::Lhs);
      currentLHS = current;
    }
    else
    {
      if (currentLHS)
      {
        assert(graph[*currentLHS].vertexType == VertexType::CMultiplication || graph[*currentLHS].vertexType == VertexType::CDivision);
        connect(*currentLHS, current, EdgeTag::Rhs);
      }
      else
        currentLHS = current;
    }
  }
  assert(currentLHS);
  return *currentLHS;
}

Vertex Stow::goCMultiplication(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::CMultiplication), graph);
}

Vertex Stow::goCDivision(const std::unique_ptr<::tao::pegtl::parse_tree::node>&)
{
  return boost::add_vertex(VertexProperty(VertexType::CDivision), graph);
}

Vertex Stow::goCParenth(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  auto out = boost::add_vertex(VertexProperty(VertexType::CParenth), graph);
  Vertex child = goCRHS(nIn);
  connect(out, child, EdgeTag::None);
  return out;
}

Vertex Stow::goCDecayRotation(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::CDecayRotation), graph);
  Vertex csys = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  
  connect(out, csys, EdgeTag::None);
  
  return out;
}

Vertex Stow::goCDecayOrigin(const std::unique_ptr<::tao::pegtl::parse_tree::node> &nIn)
{
  assert(nIn->children.size() == 1);
  assert(buildFunctionMap.count(nIn->children.at(0)->type) == 1);
  
  Vertex out = boost::add_vertex(VertexProperty(VertexType::CDecayOrigin), graph);
  Vertex csys = buildFunctionMap[nIn->children.at(0)->type](nIn->children.at(0));
  
  connect(out, csys, EdgeTag::None);
  
  return out;
}

void Stow::updateExpression(Vertex v)
{
  auto ov = findChildOfTag(v, EdgeTag::None);
  assert(ov);
  assert(graph[*ov].isClean());
  if (graph[v].valueType() == ValueType::None)
    graph[v].value = graph[*ov].value; //new expression and just assign to set type for expression
  else
  {
    if (graph[v].valueType() != graph[*ov].valueType())
      throw std::runtime_error("Can't change type of expression");
    graph[v].value = graph[*ov].value; //new expression and just assign to set type for expression
  }
  graph[v].setClean();
}

void Stow::updateSMultiplication(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Scalar);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  
  graph[v].value = std::get<double>(graph[*olhs].value) * std::get<double>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateSDivision(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Scalar);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  double denom = std::get<double>(graph[*orhs].value);
  if (denom == 0)
    throw std::runtime_error("Division By Zero");
  
  graph[v].value = std::get<double>(graph[*olhs].value) / denom;
  graph[v].setClean();
}

void Stow::updateSAddition(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Scalar);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  
  graph[v].value = std::get<double>(graph[*olhs].value) + std::get<double>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateSSubtraction(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Scalar);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  
  graph[v].value = std::get<double>(graph[*olhs].value) - std::get<double>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateSParenth(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = graph[*oc].value;
  graph[v].setClean();
}

void Stow::updateSSin(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::sin(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSCos(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::cos(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSTan(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::tan(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSAsin(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::asin(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSAcos(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::acos(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSAtan(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::atan(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSRadToDeg(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::get<double>(graph[*oc].value) * 180.0 / boost::math::constants::pi<double>();
  graph[v].setClean();
}

void Stow::updateSDegToRad(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::get<double>(graph[*oc].value) * boost::math::constants::pi<double>() / 180.0;
  graph[v].setClean();
}

void Stow::updateSLog(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::log10(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSExp(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::exp(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSSqrt(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  double cv = std::get<double>(graph[*oc].value);
  if (cv < 0.0)
    throw std::runtime_error("Square root of a negative number.");
  
  graph[v].value = std::sqrt(cv);
  graph[v].setClean();
}

void Stow::updateSAbs(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  
  graph[v].value = std::fabs(std::get<double>(graph[*oc].value));
  graph[v].setClean();
}

void Stow::updateSPow(Vertex v)
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  
  graph[v].value = std::pow(std::get<double>(graph[*op1].value), std::get<double>(graph[*op2].value));
  graph[v].setClean();
}

void Stow::updateSAtan2(Vertex v)
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  
  double v1 = std::get<double>(graph[*op1].value);
  double v2 = std::get<double>(graph[*op2].value);
  
  if (std::fabs(v1) < std::numeric_limits<float>::epsilon() && std::fabs(v2) < std::numeric_limits<float>::epsilon())
    throw std::runtime_error("zero values for atan2");
  
  graph[v].value = std::atan2(v1, v2);
  graph[v].setClean();
}

void Stow::updateSHypot(Vertex v)
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  
  graph[v].value = std::hypot(std::get<double>(graph[*op1].value), std::get<double>(graph[*op2].value));
  graph[v].setClean();
}

void Stow::updateSMin(Vertex v)
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  
  graph[v].value = std::min(std::get<double>(graph[*op1].value), std::get<double>(graph[*op2].value));
  graph[v].setClean();
}

void Stow::updateSMax(Vertex v)
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  
  graph[v].value = std::max(std::get<double>(graph[*op1].value), std::get<double>(graph[*op2].value));
  graph[v].setClean();
}

void Stow::updateSFloor(Vertex v)
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  double v1 = std::get<double>(graph[*op1].value);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  double v2 = std::fabs(std::get<double>(graph[*op2].value)); // no negatives
  
  if (v2 == 0)
  {
    //don't divide by zero.
    graph[v].value = v1;
    graph[v].setClean();
    return;
  }
  
  int factor = static_cast<int>(v1 / v2);
  if (v1 < 0)
  {
    if (std::fabs(std::fmod(v1, v2)) > 0)
      factor--;
  }
  
  graph[v].value =  v2 * static_cast<double>(factor);
  graph[v].setClean();
}

void Stow::updateSCeil(Vertex v)
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  double v1 = std::get<double>(graph[*op1].value);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  double v2 = std::fabs(std::get<double>(graph[*op2].value)); // no negatives
  
  if (v2 == 0)
  {
    //don't divide by zero.
    graph[v].value = v1;
    graph[v].setClean();
    return;
  }
  
  int factor = static_cast<int>(v1 / v2);
  if (v1 > 0)
  {
    if (std::fmod(v1, v2) > 0)
      factor++;
  }
  
  graph[v].value =  v2 * static_cast<double>(factor);
  graph[v].setClean();
}

void Stow::updateSRound(Vertex v)
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  double v1 = std::get<double>(graph[*op1].value);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  double v2 = std::fabs(std::get<double>(graph[*op2].value)); // no negatives
  
  if (v2 == 0)
  {
    //don't divide by zero.
    graph[v].value = v1;
    graph[v].setClean();
    return;
  }
  
  int factor = static_cast<int>(v1 / v2);
  if (v1 < 0)
  {
    if (std::fabs(std::fmod(v1, v2)) > (v2 / 2))
        factor--;
  }
  else
  {
    if (std::fmod(v1, v2) >= (v2 / 2))
        factor++;
  }
  
  graph[v].value =  v2 * static_cast<double>(factor);
  graph[v].setClean();
}

void Stow::updateSCondition(Vertex v)
{
  auto getDouble = [&](EdgeTag ep) -> double
  {
    auto op1 = findChildOfTag(v, ep);
    assert(op1);
    assert(graph[*op1].isClean());
    assert(graph[*op1].valueType() == ValueType::Scalar);
    return std::get<double>(graph[*op1].value);
  };
  
  double rhs = getDouble(EdgeTag::Rhs);
  double thenValue = getDouble(EdgeTag::Then);
  double elseValue = getDouble(EdgeTag::Else);
  
  auto validate = [&](Vertex vv) -> double
  {
    assert(graph[vv].isClean());
    assert(graph[vv].valueType() == ValueType::Scalar);
    return std::get<double>(graph[vv].value);
  };
  
  std::optional<double> freshValue;
  for (auto its = boost::out_edges(v, graph); its.first != its.second; ++its.first)
  {
    auto cv = boost::target(*its.first, graph);
    double lhs = validate(cv);
    if (graph[*its.first].tag == EdgeTag::GreaterEqual)
    {
      if (lhs >= rhs)
        freshValue = thenValue;
      else
        freshValue = elseValue;
      break;
    }
    if (graph[*its.first].tag == EdgeTag::LessEqual)
    {
      if (lhs <= rhs)
        freshValue = thenValue;
      else
        freshValue = elseValue;
      break;
    }
    if (graph[*its.first].tag == EdgeTag::Greater)
    {
      if (lhs > rhs)
        freshValue = thenValue;
      else
        freshValue = elseValue;
      break;
    }
    if (graph[*its.first].tag == EdgeTag::Less)
    {
      if (lhs < rhs)
        freshValue = thenValue;
      else
        freshValue = elseValue;
      break;
    }
    if (graph[*its.first].tag == EdgeTag::Equal)
    {
      if (lhs == rhs)
        freshValue = thenValue;
      else
        freshValue = elseValue;
      break;
    }
    if (graph[*its.first].tag == EdgeTag::NotEqual)
    {
      if (lhs != rhs)
        freshValue = thenValue;
      else
        freshValue = elseValue;
      break;
    }
  }
  assert(freshValue);
  
  graph[v].value =  *freshValue;
  graph[v].setClean();
}

void Stow::updateVScalars(Vertex v)
{
  auto ox = findChildOfTag(v, EdgeTag::X);
  auto oy = findChildOfTag(v, EdgeTag::Y);
  auto oz = findChildOfTag(v, EdgeTag::Z);
  assert(ox);
  assert(graph[*ox].isClean());
  assert(graph[*ox].valueType() == ValueType::Scalar);
  assert(oy);
  assert(graph[*oy].isClean());
  assert(graph[*oy].valueType() == ValueType::Scalar);
  assert(oz);
  assert(graph[*oz].isClean());
  assert(graph[*oz].valueType() == ValueType::Scalar);
  
  graph[v].value = Vector(std::get<double>(graph[*ox].value), std::get<double>(graph[*oy].value), std::get<double>(graph[*oz].value));
  graph[v].setClean();
}

void Stow::updateVDecay(Vertex v)
{
  auto vrhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(vrhs);
  assert(graph[*vrhs].isClean());
  assert(graph[*vrhs].valueType() == ValueType::Vector);
  
  Vector vec = std::get<Vector>(graph[*vrhs].value);
  std::optional<double> value;
  
  auto axis = findChildOfTag(v, EdgeTag::X);
  if (axis)
    value = vec.x();
  else
  {
    axis = findChildOfTag(v, EdgeTag::Y);
    if (axis)
      value = vec.y();
    else
    {
      axis = findChildOfTag(v, EdgeTag::Z);
      if (axis)
        value = vec.z();
    }
  }
  assert(value);
  graph[v].value = *value;
  graph[v].setClean();
}

void Stow::updateVLength(Vertex v)
{
  auto vrhs = findChildOfTag(v, EdgeTag::Vector);
  assert(vrhs);
  assert(graph[*vrhs].isClean());
  assert(graph[*vrhs].valueType() == ValueType::Vector);
  
  Vector vec = std::get<Vector>(graph[*vrhs].value);

  graph[v].value = vec.norm();
  graph[v].setClean();
}

void Stow::updateVDot(Vertex v)
{
  auto parameter1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(parameter1);
  assert(graph[*parameter1].isClean());
  assert(graph[*parameter1].valueType() == ValueType::Vector);
  Vector vec1 = std::get<Vector>(graph[*parameter1].value);
  
  auto parameter2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(parameter2);
  assert(graph[*parameter2].isClean());
  assert(graph[*parameter2].valueType() == ValueType::Vector);
  Vector vec2 = std::get<Vector>(graph[*parameter2].value);

  graph[v].value = vec1.dot(vec2);
  graph[v].setClean();
}

void Stow::updateVNormalize(Vertex v)
{
  auto vrhs = findChildOfTag(v, EdgeTag::Vector);
  assert(vrhs);
  assert(graph[*vrhs].isClean());
  assert(graph[*vrhs].valueType() == ValueType::Vector);
  
  Vector vec = std::get<Vector>(graph[*vrhs].value);
  if (vec.norm() < std::numeric_limits<float>::epsilon())
    throw std::runtime_error("Normalizing zero vector");

  graph[v].value = vec.normalized();
  graph[v].setClean();
}

void Stow::updateVCross(Vertex v)
{
  auto parameter1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(parameter1);
  assert(graph[*parameter1].isClean());
  assert(graph[*parameter1].valueType() == ValueType::Vector);
  Vector vec1 = std::get<Vector>(graph[*parameter1].value);
  
  auto parameter2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(parameter2);
  assert(graph[*parameter2].isClean());
  assert(graph[*parameter2].valueType() == ValueType::Vector);
  Vector vec2 = std::get<Vector>(graph[*parameter2].value);

  graph[v].value = vec1.cross(vec2);
  graph[v].setClean();
}

void Stow::updateVMultiplication(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Vector);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  
  graph[v].value = std::get<Vector>(graph[*olhs].value) * std::get<double>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateVDivision(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Vector);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  
  graph[v].value = std::get<Vector>(graph[*olhs].value) / std::get<double>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateVAddition(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Vector);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Vector);
  
  graph[v].value = std::get<Vector>(graph[*olhs].value) + std::get<Vector>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateVSubtraction(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Vector);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Vector);
  
  graph[v].value = std::get<Vector>(graph[*olhs].value) - std::get<Vector>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateVParenth(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Vector);
  
  graph[v].value = graph[*oc].value;
  graph[v].setClean();
}

void Stow::updateRScalars(Vertex v)
{
  auto getDouble = [&](Vertex v, EdgeTag ep) -> double
  {
    auto ov = findChildOfTag(v, ep);
    assert(ov);
    assert(graph[*ov].isClean());
    assert(graph[*ov].valueType() == ValueType::Scalar);
    return std::get<double>(graph[*ov].value);
  };
  
  double x = getDouble(v, EdgeTag::X);
  double y = getDouble(v, EdgeTag::Y);
  double z = getDouble(v, EdgeTag::Z);
  double w = getDouble(v, EdgeTag::W);
  
  graph[v].value = Rotation(w, x, y, z).normalized();
  graph[v].setClean();
}

void Stow::updateRAxisAngle(Vertex v)
{
  auto ov = findChildOfTag(v, EdgeTag::Axis);
  auto oa = findChildOfTag(v, EdgeTag::Angle);
  assert(ov);
  assert(graph[*ov].isClean());
  assert(graph[*ov].valueType() == ValueType::Vector);
  assert(oa);
  assert(graph[*oa].isClean());
  assert(graph[*oa].valueType() == ValueType::Scalar);
  
  //eigen3 says vector must be unit so assuming resultant quaternion is normalized;
  auto vec = std::get<Vector>(graph[*ov].value);
  vec.normalize();
  
  Eigen::AngleAxisd aa(std::get<double>(graph[*oa].value), vec);
  
  graph[v].value = Rotation(aa);
  graph[v].setClean();
}

void Stow::updateRFromTo(Vertex v)
{
  auto ofv = findChildOfTag(v, EdgeTag::From);
  auto otv = findChildOfTag(v, EdgeTag::To);
  assert(ofv);
  assert(graph[*ofv].isClean());
  assert(graph[*ofv].valueType() == ValueType::Vector);
  assert(otv);
  assert(graph[*otv].isClean());
  assert(graph[*otv].valueType() == ValueType::Vector);
  
  //note eigen will normalize the vectors before creating quaternion
  Rotation rot = Rotation::FromTwoVectors(std::get<Vector>(graph[*ofv].value), std::get<Vector>(graph[*otv].value));
  
  graph[v].value = rot;
  graph[v].setClean();
}

void Stow::updateRBasePairs(Vertex v)
{
  auto ovx = findChildOfTag(v, EdgeTag::X);
  auto ovy = findChildOfTag(v, EdgeTag::Y);
  auto ovz = findChildOfTag(v, EdgeTag::Z);
  
  std::optional<Rotation> orot;
  auto checkParallel = [&](const Vector &v1, const Vector &v2) -> bool
  {
    //how to get error to user?
    double theDot = v1.dot(v2);
    if (std::fabs(std::fabs(theDot) - 1.0) < std::numeric_limits<float>::epsilon())
    {
      orot->setIdentity();
      graph[v].value = *orot;
      graph[v].setClean();
      return true;
    }
    return false;
  };
  
  if (ovx && ovy)
  {
    assert(graph[*ovx].isClean());
    assert(graph[*ovx].valueType() == ValueType::Vector);
    assert(graph[*ovy].isClean());
    assert(graph[*ovy].valueType() == ValueType::Vector);
    
    Vector x = std::get<Vector>(graph[*ovx].value).normalized();
    Vector y = std::get<Vector>(graph[*ovy].value).normalized();
    if (checkParallel(x, y))
      return;
    Vector z = x.cross(y).normalized();
    y = z.cross(x).normalized();
    
    CSys::LinearMatrixType rm;
    rm.col(0) = x;
    rm.col(1) = y;
    rm.col(2) = z;
    
    orot = Rotation(rm);
  }
  else if (ovx && ovz)
  {
    assert(graph[*ovx].isClean());
    assert(graph[*ovx].valueType() == ValueType::Vector);
    assert(graph[*ovz].isClean());
    assert(graph[*ovz].valueType() == ValueType::Vector);
    
    Vector x = std::get<Vector>(graph[*ovx].value).normalized();
    Vector z = std::get<Vector>(graph[*ovz].value).normalized();
    if (checkParallel(x, z))
      return;
    Vector y = z.cross(x).normalized();
    z = x.cross(y).normalized();
    
    CSys::LinearMatrixType rm;
    rm.col(0) = x;
    rm.col(1) = y;
    rm.col(2) = z;
    
    orot = Rotation(rm);
  }
  else if (ovy && ovz)
  {
    assert(graph[*ovy].isClean());
    assert(graph[*ovy].valueType() == ValueType::Vector);
    assert(graph[*ovz].isClean());
    assert(graph[*ovz].valueType() == ValueType::Vector);
    
    Vector y = std::get<Vector>(graph[*ovy].value).normalized();
    Vector z = std::get<Vector>(graph[*ovz].value).normalized();
    if (checkParallel(y, z))
      return;
    Vector x = y.cross(z).normalized();
    z = x.cross(y).normalized();
    
    CSys::LinearMatrixType rm;
    rm.col(0) = x;
    rm.col(1) = y;
    rm.col(2) = z;
    
    orot = Rotation(rm);
  }
  assert(orot);
  graph[v].value = *orot;
  graph[v].setClean();
}

void Stow::updateRDecayAxis(Vertex v)
{
  auto orot = findChildOfTag(v, EdgeTag::None);
  assert(orot);
  assert(graph[*orot].valueType() == ValueType::Rotation);
  assert(graph[*orot].isClean());
  
  Eigen::AngleAxisd aa(std::get<Rotation>(graph[*orot].value));
  graph[v].value = aa.axis();
  graph[v].setClean();
}

void Stow::updateRDecayAngle(Vertex v)
{
  auto orot = findChildOfTag(v, EdgeTag::None);
  assert(orot);
  assert(graph[*orot].valueType() == ValueType::Rotation);
  assert(graph[*orot].isClean());
  
  Eigen::AngleAxisd aa(std::get<Rotation>(graph[*orot].value));
  graph[v].value = aa.angle();
  graph[v].setClean();
}

void Stow::updateRDecayAxisVector(Vertex v)
{
  auto orot = findChildOfTag(v, EdgeTag::Rotation);
  assert(orot);
  assert(graph[*orot].valueType() == ValueType::Rotation);
  assert(graph[*orot].isClean());
  
  auto m = std::get<Rotation>(graph[*orot].value).toRotationMatrix();
  
  std::optional<Vector> ov;
  auto ovc = findChildOfTag(v, EdgeTag::X);
  if (ovc)
    ov = m.col(0);
  else
  {
    ovc = findChildOfTag(v, EdgeTag::Y);
    if (ovc)
      ov = m.col(1);
    else
    {
      ovc = findChildOfTag(v, EdgeTag::Z);
      if (ovc)
        ov = m.col(2);
    }
  }
  assert(ov);
  
  graph[v].value = *ov;
  graph[v].setClean();
}

void Stow::updateRInverse(Vertex v)
{
  auto orot = findChildOfTag(v, EdgeTag::Rotation);
  assert(orot);
  assert(graph[*orot].valueType() == ValueType::Rotation);
  assert(graph[*orot].isClean());
  
  graph[v].value = std::get<Rotation>(graph[*orot].value).inverse();
  graph[v].setClean();
}

void Stow::updateRRotateVector(Vertex v)
{
  auto orot = findChildOfTag(v, EdgeTag::Rotation);
  assert(orot);
  assert(graph[*orot].valueType() == ValueType::Rotation);
  assert(graph[*orot].isClean());
  Rotation rot = std::get<Rotation>(graph[*orot].value);
  
  auto ovec = findChildOfTag(v, EdgeTag::Vector);
  assert(ovec);
  assert(graph[*ovec].valueType() == ValueType::Vector);
  assert(graph[*ovec].isClean());
  Vector vec = std::get<Vector>(graph[*ovec].value);
  
  graph[v].value = rot * vec;
  graph[v].setClean();
}

void Stow::updateRMultiplication(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Rotation);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Rotation);
  
  graph[v].value = std::get<Rotation>(graph[*olhs].value) * std::get<Rotation>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateRDivision(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Rotation);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Rotation);
  
  graph[v].value = std::get<Rotation>(graph[*olhs].value) * std::get<Rotation>(graph[*orhs].value).inverse();
  graph[v].setClean();
}

void Stow::updateRParenth(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Rotation);
  
  graph[v].value = graph[*oc].value;
  graph[v].setClean();
}

void Stow::updateCVectors(Vertex v)
{
  auto getVertex = [&](EdgeTag ep) -> Vertex
  {
    auto ov = findChildOfTag(v, ep);
    assert(ov);
    assert(graph[*ov].isClean());
    assert(graph[*ov].valueType() == ValueType::Vector);
    return *ov;
  };
  
  auto xv = getVertex(EdgeTag::X);
  auto yv = getVertex(EdgeTag::Y);
  auto zv = getVertex(EdgeTag::Z);
  auto ov = getVertex(EdgeTag::Origin);
  
  //non orthoganol vectors?
  //non unit?
  CSys::LinearMatrixType rm;
  rm.col(0) = std::get<Vector>(graph[xv].value);
  rm.col(1) = std::get<Vector>(graph[yv].value);
  rm.col(2) = std::get<Vector>(graph[zv].value);
  
  CSys t;
  t.linear() = rm;
  t.translation() = std::get<Vector>(graph[ov].value);
  
  graph[v].value = t;
  graph[v].setClean();
}

void Stow::updateCRotationOrigin(Vertex v)
{
  auto rotation = findChildOfTag(v, EdgeTag::Rotation);
  assert(rotation);
  assert(graph[*rotation].isClean());
  assert(graph[*rotation].valueType() == ValueType::Rotation);
  
  auto origin = findChildOfTag(v, EdgeTag::Origin);
  assert(origin);
  assert(graph[*origin].isClean());
  assert(graph[*origin].valueType() == ValueType::Vector);
  
  CSys t;
  t.linear() = std::get<Rotation>(graph[*rotation].value).toRotationMatrix();
  t.translation() = std::get<Vector>(graph[*origin].value);
  
  graph[v].value = t;
  graph[v].setClean();
}

void Stow::updateCConstantZero(Vertex v)
{
  //doesn't need to update, but:
  //to KISS we mark all dependent children dirty during changes.
  //so we need this to set it clean
  graph[v].setClean();
}

void Stow::updateCInverse(Vertex v)
{
  auto system = findChildOfTag(v, EdgeTag::None);
  assert(system);
  assert(graph[*system].isClean());
  assert(graph[*system].valueType() == ValueType::CSys);
  
  graph[v].value = std::get<CSys>(graph[*system].value).inverse();
  graph[v].setClean();
}

void Stow::updateCMap(Vertex v)
{
  auto from = findChildOfTag(v, EdgeTag::From);
  assert(from);
  assert(graph[*from].isClean());
  assert(graph[*from].valueType() == ValueType::CSys);
  
  auto to = findChildOfTag(v, EdgeTag::To);
  assert(to);
  assert(graph[*to].isClean());
  assert(graph[*to].valueType() == ValueType::CSys);
  
  auto vector = findChildOfTag(v, EdgeTag::Vector);
  assert(vector);
  assert(graph[*vector].isClean());
  assert(graph[*vector].valueType() == ValueType::Vector);
  
  CSys frm = std::get<CSys>(graph[*from].value);
  CSys t = std::get<CSys>(graph[*to].value);
  Vector vec = std::get<Vector>(graph[*vector].value);
  Vector out = frm.inverse() * t * vec;
  
  graph[v].value = out;
  graph[v].setClean();
}

void Stow::updateCMultiplication(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::CSys);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::CSys);
  
  graph[v].value = std::get<CSys>(graph[*olhs].value) * std::get<CSys>(graph[*orhs].value);
  graph[v].setClean();
}

void Stow::updateCDivision(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::CSys);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::CSys);
  
  graph[v].value = std::get<CSys>(graph[*olhs].value) * std::get<CSys>(graph[*orhs].value).inverse();
  graph[v].setClean();
}

void Stow::updateCParenth(Vertex v)
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::CSys);
  
  graph[v].value = graph[*oc].value;
  graph[v].setClean();
}

void Stow::updateCDecayRotation(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::None);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::CSys);
  
  graph[v].value = Rotation(std::get<CSys>(graph[*olhs].value).linear());
  graph[v].setClean();
}

void Stow::updateCDecayOrigin(Vertex v)
{
  auto olhs = findChildOfTag(v, EdgeTag::None);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::CSys);
  
  graph[v].value = Vector(std::get<CSys>(graph[*olhs].value).translation());
  graph[v].setClean();
}

std::string Stow::toStringExpression(Vertex v) const
{
  //note: don't visit beyond expression.
  return graph[v].name;
}

std::string Stow::toStringSConstant(Vertex v) const
{
  return prettyDouble(std::get<double>(graph[v].value));
}

std::string Stow::toStringSConstantPI(Vertex) const
{
  return "PI";
}

std::string Stow::toStringSConstantE(Vertex) const
{
  return "E";
}

std::string Stow::toStringSMultiplication(Vertex v) const
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*olhs].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string lhsString = stringFunctionMap.at(graph[*olhs].vertexType)(*olhs);
  std::string rhsString = stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  
  return lhsString + " * " + rhsString;
}

std::string Stow::toStringSDivision(Vertex v) const
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*olhs].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string lhsString = stringFunctionMap.at(graph[*olhs].vertexType)(*olhs);
  std::string rhsString = stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  
  return lhsString + " / " + rhsString;
}

std::string Stow::toStringSAddition(Vertex v) const
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*olhs].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string lhsString = stringFunctionMap.at(graph[*olhs].vertexType)(*olhs);
  std::string rhsString = stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  
  return lhsString + " + " + rhsString;
}

std::string Stow::toStringSSubtraction(Vertex v) const
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*olhs].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string lhsString = stringFunctionMap.at(graph[*olhs].vertexType)(*olhs);
  std::string rhsString = stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  
  return lhsString + " - " + rhsString;
}

std::string Stow::toStringSParenth(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSSin(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "sin(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSCos(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "cos(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSTan(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "tan(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSAsin(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "asin(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSAcos(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "acos(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSAtan(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "atan(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSRadToDeg(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "radtodeg(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSDegToRad(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "degtorad(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSLog(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "log(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSExp(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "exp(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSSqrt(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "sqrt(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSAbs(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "abs(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSPow(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "pow(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSAtan2(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "atan2(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSHypot(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "hypot(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSMin(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "min(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSMax(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "max(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSFloor(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "floor(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSCeil(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "ceil(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSRound(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "round(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringSCondition(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::Scalar);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  std::string out = "if(";
  
  auto validate = [&](Vertex vv)
  {
    assert(graph[vv].isClean());
    assert(graph[vv].valueType() == ValueType::Scalar);
  };
  
  auto its = boost::out_edges(v, graph);
  for (; its.first != its.second; ++its.first)
  {
    auto cv = boost::target(*its.first, graph);
    validate(cv);
    if (graph[*its.first].tag == EdgeTag::GreaterEqual)
    {
      out += stringFunctionMap.at(graph[cv].vertexType)(cv);
      out += " >= ";
      break;
    }
    if (graph[*its.first].tag == EdgeTag::LessEqual)
    {
      out += stringFunctionMap.at(graph[cv].vertexType)(cv);
      out += " <= ";
      break;
    }
    if (graph[*its.first].tag == EdgeTag::Greater)
    {
      out += stringFunctionMap.at(graph[cv].vertexType)(cv);
      out += " > ";
      break;
    }
    if (graph[*its.first].tag == EdgeTag::Less)
    {
      out += stringFunctionMap.at(graph[cv].vertexType)(cv);
      out += " < ";
      break;
    }
    if (graph[*its.first].tag == EdgeTag::Equal)
    {
      out += stringFunctionMap.at(graph[cv].vertexType)(cv);
      out += " == ";
      break;
    }
    if (graph[*its.first].tag == EdgeTag::NotEqual)
    {
      out += stringFunctionMap.at(graph[cv].vertexType)(cv);
      out += " != ";
      break;
    }
  }
  assert(its.first != its.second); //can't find condition
    
  auto rhsv = getChildVertex(EdgeTag::Rhs);
  out += stringFunctionMap.at(graph[rhsv].vertexType)(rhsv);
  out += ")";
  
  out += " then ";
  auto thenv = getChildVertex(EdgeTag::Then);
  out += stringFunctionMap.at(graph[thenv].vertexType)(thenv);
  
  out += " else ";
  auto elsev = getChildVertex(EdgeTag::Else);
  out += stringFunctionMap.at(graph[elsev].vertexType)(elsev);
  
  return out;
}

std::string Stow::toStringVScalars(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::Scalar);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto x = getChildVertex(EdgeTag::X);
  auto y = getChildVertex(EdgeTag::Y);
  auto z = getChildVertex(EdgeTag::Z);
  
  std::string childString = "[";
  childString += stringFunctionMap.at(graph[x].vertexType)(x);
  childString += ", ";
  childString += stringFunctionMap.at(graph[y].vertexType)(y);
  childString += ", ";
  childString += stringFunctionMap.at(graph[z].vertexType)(z);
  childString += "]";
  
  return childString;
}

std::string Stow::toStringVConstantX(Vertex) const
{
  return "X";
}

std::string Stow::toStringVConstantY(Vertex) const
{
  return "Y";
}

std::string Stow::toStringVConstantZ(Vertex) const
{
  return "Z";
}

std::string Stow::toStringVConstantZero(Vertex) const
{
  return "VZERO";
}

std::string Stow::toStringVDecay(Vertex v) const
{
  auto oaxis = findChildOfTag(v, EdgeTag::X);
  if (!oaxis)
    oaxis = findChildOfTag(v, EdgeTag::Y);
  if (!oaxis)
    oaxis = findChildOfTag(v, EdgeTag::Z);
  assert(oaxis);
  assert(stringFunctionMap.count(graph[*oaxis].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string childString = "vdecay(";
  childString += stringFunctionMap.at(graph[*oaxis].vertexType)(*oaxis);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringVLength(Vertex v) const
{
  auto ovec = findChildOfTag(v, EdgeTag::Vector);
  assert(ovec);
  assert(graph[*ovec].isClean());
  assert(graph[*ovec].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*ovec].vertexType) == 1);
  
  std::string childString = "vlength(";
  childString += stringFunctionMap.at(graph[*ovec].vertexType)(*ovec);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringVDot(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "vdot(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringVNormalize(Vertex v) const
{
  auto ovec = findChildOfTag(v, EdgeTag::Vector);
  assert(ovec);
  assert(graph[*ovec].isClean());
  assert(graph[*ovec].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*ovec].vertexType) == 1);
  
  std::string childString = "vnormalize(";
  childString += stringFunctionMap.at(graph[*ovec].vertexType)(*ovec);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringVCross(Vertex v) const
{
  auto op1 = findChildOfTag(v, EdgeTag::Parameter1);
  assert(op1);
  assert(graph[*op1].isClean());
  assert(graph[*op1].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*op1].vertexType) == 1);
  
  auto op2 = findChildOfTag(v, EdgeTag::Parameter2);
  assert(op2);
  assert(graph[*op2].isClean());
  assert(graph[*op2].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*op2].vertexType) == 1);
  
  std::string childString = "vcross(";
  childString += stringFunctionMap.at(graph[*op1].vertexType)(*op1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*op2].vertexType)(*op2);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringVMultiplication(Vertex v) const
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*olhs].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string childString = stringFunctionMap.at(graph[*olhs].vertexType)(*olhs);
  childString += " * ";
  childString += stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  
  return childString;
}

std::string Stow::toStringVDivision(Vertex v) const
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*olhs].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Scalar);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string childString = stringFunctionMap.at(graph[*olhs].vertexType)(*olhs);
  childString += " / ";
  childString += stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  
  return childString;
}

std::string Stow::toStringVAddition(Vertex v) const
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*olhs].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string childString = stringFunctionMap.at(graph[*olhs].vertexType)(*olhs);
  childString += " + ";
  childString += stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  
  return childString;
}

std::string Stow::toStringVSubtraction(Vertex v) const
{
  auto olhs = findChildOfTag(v, EdgeTag::Lhs);
  assert(olhs);
  assert(graph[*olhs].isClean());
  assert(graph[*olhs].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*olhs].vertexType) == 1);
  
  auto orhs = findChildOfTag(v, EdgeTag::Rhs);
  assert(orhs);
  assert(graph[*orhs].isClean());
  assert(graph[*orhs].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*orhs].vertexType) == 1);
  
  std::string childString = stringFunctionMap.at(graph[*olhs].vertexType)(*olhs);
  childString += " - ";
  childString += stringFunctionMap.at(graph[*orhs].vertexType)(*orhs);
  
  return childString;
}

std::string Stow::toStringVParenth(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Vector);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringRConstantZero(Vertex) const
{
  return "RZERO";
}

std::string Stow::toStringRScalars(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::Scalar);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto x = getChildVertex(EdgeTag::X);
  auto y = getChildVertex(EdgeTag::Y);
  auto z = getChildVertex(EdgeTag::Z);
  auto w = getChildVertex(EdgeTag::W);
  
  std::string childString = "[";
  childString += stringFunctionMap.at(graph[x].vertexType)(x);
  childString += ", ";
  childString += stringFunctionMap.at(graph[y].vertexType)(y);
  childString += ", ";
  childString += stringFunctionMap.at(graph[z].vertexType)(z);
  childString += ", ";
  childString += stringFunctionMap.at(graph[w].vertexType)(w);
  childString += "]";
  
  return childString;
}

std::string Stow::toStringRAxisAngle(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto axis = getChildVertex(EdgeTag::Axis);
  assert(graph[axis].valueType() == ValueType::Vector);
  
  auto angle = getChildVertex(EdgeTag::Angle);
  assert(graph[angle].valueType() == ValueType::Scalar);
  
  std::string childString = "[";
  childString += stringFunctionMap.at(graph[axis].vertexType)(axis);
  childString += ", ";
  childString += stringFunctionMap.at(graph[angle].vertexType)(angle);
  childString += "]";
  
  return childString;
}

std::string Stow::toStringRFromTo(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::Vector);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto from = getChildVertex(EdgeTag::From);
  auto to = getChildVertex(EdgeTag::To);
  
  std::string childString = "[";
  childString += stringFunctionMap.at(graph[from].vertexType)(from);
  childString += ", ";
  childString += stringFunctionMap.at(graph[to].vertexType)(to);
  childString += "]";
  
  return childString;
}

std::string Stow::toStringRDecayAxis(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Rotation);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "rdecayaxis(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringRDecayAngle(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Rotation);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "rdecayangle(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringRDecayAxisVector(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  std::optional<std::string> axis;
  if (findChildOfTag(v, EdgeTag::X))
    axis = "X";
  else if (findChildOfTag(v, EdgeTag::Y))
    axis = "Y";
  else if (findChildOfTag(v, EdgeTag::Z))
    axis = "Z";
  assert(axis);
  
  auto rotation = getChildVertex(EdgeTag::Rotation);
  assert(graph[rotation].valueType() == ValueType::Rotation);
  
  std::string childString = "rdecayaxis(";
  childString += *axis;
  childString += ", ";
  childString += stringFunctionMap.at(graph[rotation].vertexType)(rotation);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringRBasePairs(Vertex v) const
{
  std::optional<Vertex> ov1, ov2;
  std::string stringPair;
  
  //x is always first
  auto ox = findChildOfTag(v, EdgeTag::X);
  if (ox)
  {
    assert(graph[*ox].isClean());
    assert(stringFunctionMap.count(graph[*ox].vertexType) == 1);
    stringPair += 'X';
    ov1 = ox;
  }
  
  //y can be first or second
  auto oy = findChildOfTag(v, EdgeTag::Y);
  if (oy)
  {
    assert(graph[*oy].isClean());
    assert(stringFunctionMap.count(graph[*oy].vertexType) == 1);
    stringPair += 'Y';
    if (!ov1)
      ov1 = oy;
    else
      ov2 = oy;
  }
  
  // z is always last.
  auto oz = findChildOfTag(v, EdgeTag::Z);
  if (oz)
  {
    assert(graph[*oz].isClean());
    assert(stringFunctionMap.count(graph[*oz].vertexType) == 1);
    stringPair += 'Z';
    ov2 = oz;
  }
  
  assert(ov1);
  assert(ov2);
  assert(stringPair.size() == 2);
  
  std::string childString = "[";
  childString += stringPair;
  childString += ", ";
  childString += stringFunctionMap.at(graph[*ov1].vertexType)(*ov1);
  childString += ", ";
  childString += stringFunctionMap.at(graph[*ov2].vertexType)(*ov2);
  childString += "]";
  
  return childString;
}

std::string Stow::toStringRInverse(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::Rotation);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Rotation);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "rinverse(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringRMultiplication(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::Rotation);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto lhs = getChildVertex(EdgeTag::Lhs);
  auto rhs = getChildVertex(EdgeTag::Rhs);
  
  std::string childString = stringFunctionMap.at(graph[lhs].vertexType)(lhs);
  childString += " * ";
  childString += stringFunctionMap.at(graph[rhs].vertexType)(rhs);
  
  return childString;
}

std::string Stow::toStringRDivision(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::Rotation);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto lhs = getChildVertex(EdgeTag::Lhs);
  auto rhs = getChildVertex(EdgeTag::Rhs);
  
  std::string childString = stringFunctionMap.at(graph[lhs].vertexType)(lhs);
  childString += " / ";
  childString += stringFunctionMap.at(graph[rhs].vertexType)(rhs);
  
  return childString;
}

std::string Stow::toStringRParenth(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::Rotation);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringRRotateVector(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto rotation = getChildVertex(EdgeTag::Rotation);
  assert(graph[rotation].valueType() == ValueType::Rotation);
  
  auto vector = getChildVertex(EdgeTag::Vector);
  assert(graph[vector].valueType() == ValueType::Vector);
  
  std::string childString = "rrotatevector(";
  childString += stringFunctionMap.at(graph[rotation].vertexType)(rotation);
  childString += ", ";
  childString += stringFunctionMap.at(graph[vector].vertexType)(vector);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringCVectors(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::Vector);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto xv = getChildVertex(EdgeTag::X);
  auto yv = getChildVertex(EdgeTag::Y);
  auto zv = getChildVertex(EdgeTag::Z);
  auto ov = getChildVertex(EdgeTag::Origin);
  
  std::string childString = "[";
  childString += stringFunctionMap.at(graph[xv].vertexType)(xv);
  childString += ", ";
  childString += stringFunctionMap.at(graph[yv].vertexType)(yv);
  childString += ", ";
  childString += stringFunctionMap.at(graph[zv].vertexType)(zv);
  childString += ", ";
  childString += stringFunctionMap.at(graph[ov].vertexType)(ov);
  childString += "]";
  
  return childString;
}

std::string Stow::toStringCRotationOrigin(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto rotation = getChildVertex(EdgeTag::Rotation);
  assert(graph[rotation].valueType() == ValueType::Rotation);
  auto origin = getChildVertex(EdgeTag::Origin);
  assert(graph[origin].valueType() == ValueType::Vector);
  
  std::string childString = "[";
  childString += stringFunctionMap.at(graph[rotation].vertexType)(rotation);
  childString += ", ";
  childString += stringFunctionMap.at(graph[origin].vertexType)(origin);
  childString += "]";
  
  return childString;
}

std::string Stow::toStringCConstantZero(Vertex) const
{
  return "CZERO";
}

std::string Stow::toStringCInverse(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::CSys);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "cinverse(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringCMap(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto from = getChildVertex(EdgeTag::From);
  assert(graph[from].valueType() == ValueType::CSys);
  auto to = getChildVertex(EdgeTag::To);
  assert(graph[to].valueType() == ValueType::CSys);
  auto vector = getChildVertex(EdgeTag::Vector);
  assert(graph[vector].valueType() == ValueType::Vector);
  
  std::string childString = "cmap(";
  childString += stringFunctionMap.at(graph[from].vertexType)(from);
  childString += ", ";
  childString += stringFunctionMap.at(graph[to].vertexType)(to);
  childString += ", ";
  childString += stringFunctionMap.at(graph[vector].vertexType)(vector);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringCMultiplication(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::CSys);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto lhs = getChildVertex(EdgeTag::Lhs);
  auto rhs = getChildVertex(EdgeTag::Rhs);
  
  std::string childString = stringFunctionMap.at(graph[lhs].vertexType)(lhs);
  childString += " * ";
  childString += stringFunctionMap.at(graph[rhs].vertexType)(rhs);
  
  return childString;
}

std::string Stow::toStringCDivision(Vertex v) const
{
  auto getChildVertex = [&](EdgeTag et) -> Vertex
  {
    auto oc = findChildOfTag(v, et);
    assert(oc);
    assert(graph[*oc].isClean());
    assert(graph[*oc].valueType() == ValueType::CSys);
    assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
    return *oc;
  };
  
  auto lhs = getChildVertex(EdgeTag::Lhs);
  auto rhs = getChildVertex(EdgeTag::Rhs);
  
  std::string childString = stringFunctionMap.at(graph[lhs].vertexType)(lhs);
  childString += " / ";
  childString += stringFunctionMap.at(graph[rhs].vertexType)(rhs);
  
  return childString;
}

std::string Stow::toStringCParenth(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::CSys);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringCDecayRotation(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::CSys);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "cdecayrotation(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}

std::string Stow::toStringCDecayOrigin(Vertex v) const
{
  auto oc = findChildOfTag(v, EdgeTag::None);
  assert(oc);
  assert(graph[*oc].isClean());
  assert(graph[*oc].valueType() == ValueType::CSys);
  assert(stringFunctionMap.count(graph[*oc].vertexType) == 1);
  
  std::string childString = "cdecayorigin(";
  childString += stringFunctionMap.at(graph[*oc].vertexType)(*oc);
  childString += ")";
  
  return childString;
}
