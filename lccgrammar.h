/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef LCC_GRAMMAR
#define LCC_GRAMMAR

#include <tao/pegtl/rules.hpp>
#include <tao/pegtl/ascii.hpp>

namespace lcc
{
  namespace grm
  {
    using namespace ::tao::pegtl;

    //Parsing rule to match doubles. Taken and simplified from examples.
    struct plus_minus : opt<one<'+', '-'>>{};
    struct dot : one<'.'> {};
    struct number : if_then_else<dot, plus<digit>, seq<plus<digit>, opt<dot, star<digit>>>>{};
    struct decimal : seq<plus_minus, number>{};

    //helpers
    struct spc : star<ascii::space>{}; //optional space or spaces
    struct cmm : seq<spc, one<','>, spc>{}; //comma surrounded by optional spaces.
    struct bpn : seq<spc, one<'['>, spc>{}; //open bracket surrounded by optional spaces.
    struct bcl : seq<spc, one<']'>, spc>{}; //closed bracket surrounded by optional spaces.
    struct ppn : seq<spc, one<'('>, spc>{}; //parentheses open
    struct pcl : seq<spc, one<')'>, spc>{}; //parentheses closed
    struct multiplicationtoken : seq<spc, one<'*'>, spc>{};
    struct divisiontoken : seq<spc, one<'/'>, spc>{};
    struct additiontoken : seq<spc, one<'+'>, spc>{};
    struct subtractiontoken : seq<spc, one<'-'>, spc>{};
    struct axisx : one<'X'>{};
    struct axisy : one<'Y'>{};
    struct axisz : one<'Z'>{};
    struct oneaxis : seq<spc, sor<axisx, axisy, axisz>, spc>{};
    struct axesxy : string<'X', 'Y'>{};
    struct axesxz : string<'X', 'Z'>{};
    struct axesyz : string<'Y', 'Z'>{};
    struct twoaxes : seq<spc, sor<axesxy, axesxz, axesyz>, spc>{};
    struct axesreserved : sor<twoaxes, oneaxis>{}; //double must be first
    struct reserved; //forward

    //scalars
    //sfunction2 has to be listed before sfunction1 because of 'atan' and 'atan2'
    struct sparenth; //forward
    struct sfunction1; //forward
    struct sfunction2; //forward
    struct scondition; //forward
    struct vdecay; //forward
    struct rdecayangle; //forward
    struct vlength; //forward
    struct vdot; //forward
    struct sid : minus<identifier, reserved>{}; //to attach action
    struct sconstantpi : string<'P', 'I'>{};
    struct sconstante : one<'E'>{};
    struct sconstants : seq<spc, sor<sconstantpi, sconstante>, spc>{};
    struct sfactor : sor<seq<spc, decimal, spc>, sconstants, seq<spc, sid, spc>, sparenth, sfunction2, sfunction1, scondition, vdecay, vlength, vdot, rdecayangle>{};
    struct smultiplication : multiplicationtoken{};
    struct sdivision : divisiontoken{};
    struct sterm : list_must<sfactor, sor<smultiplication, sdivision>>{};
    struct saddition : additiontoken{};
    struct ssubtraction : subtractiontoken{};
    struct srhs : list_must<sterm, sor<saddition, ssubtraction>>{};
    struct sparenth : seq<ppn, srhs, pcl>{};
    struct ssin : string<'s', 'i', 'n'>{};
    struct scos : string<'c', 'o', 's'>{};
    struct stan : string<'t', 'a', 'n'>{};
    struct sasin : string<'a', 's', 'i', 'n'>{};
    struct sacos : string<'a', 'c', 'o', 's'>{};
    struct satan : string<'a', 't', 'a', 'n'>{};
    struct sradtodeg : string<'r', 'a', 'd', 't', 'o', 'd', 'e', 'g'>{};
    struct sdegtorad : string<'d', 'e', 'g', 't', 'o', 'r', 'a', 'd'>{};
    struct slog : string<'l', 'o', 'g'>{};
    struct sexp : string<'e', 'x', 'p'>{};
    struct ssqrt : string<'s', 'q', 'r', 't'>{};
    struct sabs : string<'a', 'b', 's'>{};
    struct sfunction1id : seq<spc, sor<ssin, scos, stan, sasin, sacos, satan, sradtodeg, sdegtorad, slog, sexp, ssqrt, sabs>, spc>{};
    struct sfunction1 : seq<sfunction1id, ppn, srhs, pcl>{};
    struct spow : string<'p', 'o', 'w'>{};
    struct satan2 : string<'a', 't', 'a', 'n', '2'>{};
    struct shypot : string<'h', 'y', 'p', 'o', 't'>{};
    struct smin : string<'m', 'i', 'n'>{};
    struct smax : string<'m', 'a', 'x'>{};
    struct sfloor : string<'f', 'l', 'o', 'o', 'r'>{};
    struct sceil : string<'c', 'e', 'i', 'l'>{};
    struct sround : string<'r', 'o', 'u', 'n', 'd'>{};
    struct sfunction2id : seq<spc, sor<spow, satan2, shypot, smin, smax, sfloor, sceil, sround>, spc>{};
    struct sfunction2 : seq<sfunction2id, ppn, srhs, cmm, srhs, pcl>{};
    struct siftoken : seq<spc, string<'i', 'f'>, spc>{};
    struct sthentoken : seq<spc, string<'t', 'h', 'e', 'n'>, spc>{};
    struct selsetoken : seq<spc, string<'e', 'l', 's', 'e'>, spc>{};
    struct sgreaterequal : seq<spc, string<'>', '='>, spc>{};
    struct slessequal : seq<spc, string<'<', '='>, spc>{};
    struct sgreater : seq<spc, one<'>'>, spc>{};
    struct sless : seq<spc, one<'<'>, spc>{};
    struct sequal : seq<spc, string<'=', '='>, spc>{};
    struct snotequal : seq<spc, string<'!', '='>, spc>{};
    struct sconditionaloperator : sor<sgreaterequal, slessequal, sgreater, sless, sequal, snotequal>{};
    struct scondition : seq<siftoken, ppn, srhs, sconditionaloperator, srhs, pcl, sthentoken, srhs, selsetoken, srhs>{};
    struct sreserved : sor<sconstants, sfunction2id, sfunction1id, siftoken, sthentoken, selsetoken>{};

    //vectors
    struct vnormalize;
    struct vcross;
    struct vparenth;
    struct rdecayaxis;
    struct rdecayaxisvector;
    struct rrotatevector;
    struct cmap;
    struct cdecayorigin;
    struct vid : minus<identifier, reserved>{}; //to attach action
    struct vconstantzero : seq<spc, string<'V', 'Z', 'E', 'R', 'O'>, spc>{};
    struct vconstants : sor<vconstantzero>{};
    struct vscalars : seq<bpn, srhs, cmm, srhs, cmm, srhs, bcl>{};
    struct vfactor : sor<oneaxis, vconstants, vscalars, seq<spc, vid, spc>, vparenth, vnormalize, vcross, rdecayaxis, rdecayaxisvector, rrotatevector, cmap, cdecayorigin>{};
    struct vmultiplication : multiplicationtoken{};
    struct vdivision : divisiontoken{};
    struct vterm : seq<vfactor, opt<if_must<sor<vmultiplication, vdivision>, srhs>>>{};
    struct vaddition : additiontoken{};
    struct vsubtraction : subtractiontoken{};
    struct vrhs : list_must<vterm, sor<vaddition, vsubtraction>>{};
    struct vparenth : seq<ppn, vrhs, pcl>{};
    struct vdecaytoken : seq<spc, string<'v', 'd', 'e', 'c', 'a', 'y'>, spc>{};
    struct vdecay : seq<vdecaytoken, ppn, oneaxis, cmm, vrhs, pcl>{};
    struct vlengthtoken : seq<spc, string<'v', 'l', 'e', 'n', 'g', 't', 'h'>, spc>{};
    struct vlength : seq<vlengthtoken, ppn, vrhs, pcl>{};
    struct vdottoken : seq<spc, string<'v', 'd', 'o', 't'>, spc>{};
    struct vdot : seq<vdottoken, ppn, vrhs, cmm, vrhs, pcl>{};
    struct vnormalizetoken : seq<spc, string<'v', 'n', 'o', 'r', 'm', 'a', 'l', 'i', 'z', 'e'>, spc>{};
    struct vnormalize : seq<vnormalizetoken, ppn, vrhs, pcl>{};
    struct vcrosstoken : seq<spc, string<'v', 'c', 'r', 'o', 's', 's'>, spc>{};
    struct vcross : seq<vcrosstoken, ppn, vrhs, cmm, vrhs, pcl>{};
    struct vreserved : sor<vconstants, vdecaytoken, vlengthtoken, vdottoken, vnormalizetoken, vcrosstoken>{};

    //rotations
    struct rinverse; //forward
    struct rparenth; //forward
    struct cdecayrotation; //forward
    struct rid : minus<identifier, reserved>{}; //to attach action
    struct rconstantzero : seq<spc, string<'R', 'Z', 'E', 'R', 'O'>, spc>{};
    struct rscalars : seq<bpn, srhs, cmm, srhs, cmm, srhs, cmm, srhs, bcl>{};
    struct raxisangle : seq<bpn, vrhs, cmm, srhs, bcl>{};
    struct rfromto : seq<bpn, vrhs, cmm, vrhs, bcl>{};
    struct rbasepairs : seq<bpn, twoaxes, cmm, vrhs, cmm, vrhs, bcl>{};
    struct rfactor : sor<rscalars, raxisangle, rfromto, rbasepairs, seq<spc, rid, spc>, rparenth, rconstantzero, rinverse, cdecayrotation>{};
    struct rmultiplication : multiplicationtoken{};
    struct rdivision : divisiontoken{};
    struct rterm : list_must<rfactor, sor<rmultiplication, rdivision>>{};
    struct rrhs : rterm{}; //place holder
    struct rparenth : seq<ppn, rrhs, pcl>{};
    struct rdecayaxistoken : seq<spc, string<'r', 'd', 'e', 'c', 'a', 'y', 'a', 'x', 'i', 's'>, spc>{};
    struct rdecayaxis : seq<rdecayaxistoken, ppn, rrhs, pcl>{}; //axis of a angle axis rotation
    struct rdecayangletoken : seq<spc, string<'r', 'd', 'e', 'c', 'a', 'y', 'a', 'n', 'g', 'l', 'e'>, spc>{};
    struct rdecayangle : seq<rdecayangletoken, ppn, rrhs, pcl>{};
    struct rdecayaxisvector : seq<spc, rdecayaxistoken, ppn, oneaxis, cmm, rrhs, pcl>{}; //axis of a rotation matrix
    struct rinversetoken : seq<spc, string<'r', 'i', 'n', 'v', 'e', 'r', 's', 'e'>, spc>{};
    struct rinverse : seq<rinversetoken, ppn, rrhs, pcl>{};
    struct rrotatevectortoken : seq<spc, string<'r', 'r', 'o', 't', 'a', 't', 'e', 'v', 'e', 'c', 't', 'o', 'r'>, spc>{};
    struct rrotatevector : seq<rrotatevectortoken, ppn, rrhs, cmm, vrhs, pcl>{};
    struct rreserved : sor<rdecayaxistoken, rdecayangletoken, rconstantzero, rinversetoken, rrotatevectortoken>{};

    //csys
    struct cinverse;
    struct cparenth; //forward
    struct cid : minus<identifier, reserved>{}; //to attach action
    struct cconstantzero : seq<spc, string<'C', 'Z', 'E', 'R', 'O'>, spc>{};
    struct cvectors : seq<bpn, vrhs, cmm, vrhs, cmm, vrhs, cmm, vrhs, bcl>{}; //rotation matrix and origin
    struct crotationorigin : seq<bpn, rrhs, cmm, vrhs, bcl>{};
    struct cfactor : sor<cvectors, crotationorigin, seq<spc, cid, spc>, cparenth, cconstantzero, cinverse>{};
    struct cmultiplication : multiplicationtoken{};
    struct cdivision : divisiontoken{};
    struct cterm : list_must<cfactor, sor<cmultiplication, cdivision>>{};
    struct crhs : cterm{};
    struct cparenth : seq<ppn, crhs, pcl>{};
    struct cinversetoken : seq<spc, string<'c', 'i', 'n', 'v', 'e', 'r', 's', 'e'>, spc>{};
    struct cinverse : seq<cinversetoken, ppn, crhs, pcl>{};
    struct cmaptoken : seq<spc, string<'c', 'm', 'a', 'p'>, spc>{};
    struct cmap : seq<cmaptoken, ppn, crhs, cmm, crhs, cmm, vrhs, pcl>{};
    struct cdecayrotationtoken : seq<spc, string<'c', 'd', 'e', 'c', 'a', 'y', 'r', 'o', 't', 'a', 't', 'i', 'o', 'n'>, spc>{};
    struct cdecayrotation : seq<cdecayrotationtoken, ppn, crhs, pcl>{};
    struct cdecayorigintoken : seq<spc, string<'c', 'd', 'e', 'c', 'a', 'y', 'o', 'r', 'i', 'g', 'i', 'n'>, spc>{};
    struct cdecayorigin : seq<cdecayorigintoken, ppn, crhs, pcl>{};
    struct creserved : sor<cconstantzero, cinversetoken, cmaptoken, cdecayrotationtoken, cdecayorigintoken>{};

    //expression
    struct lhsidentifier : identifier{}; //differentiate for action
    struct reserved : sor<axesreserved, sreserved, vreserved, rreserved, creserved>{};
    struct lhs : minus<seq<spc, lhsidentifier, spc>, reserved>{};
    struct tsrhs : srhs{}; //top srhs to differentiate for action
    struct tvrhs : vrhs{};
    struct trrhs : rrhs{};
    struct tcrhs : crhs{};
    struct equals : one<'='>{};
    struct trhs : sor<tsrhs, tvrhs, trrhs, tcrhs>{};
    struct rhs : seq<spc, trhs, spc>{};
    struct expression : must<lhs, equals, rhs, eof>{};
  }
}

#endif
