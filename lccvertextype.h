/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LCC_VERTEXTYPE
#define LCC_VERTEXTYPE

namespace lcc
{
  /*! @brief Types for graph vertices
  * 
  * prefix of S = scalar
  * prefix of V = vector
  * prefix of R = rotation
  * prefix of CSys = coordinate system. 4x4 matrix
  */
  enum class VertexType
  {
    None
    , Expression
    , SConstant
    , SConstantPI
    , SConstantE
    , SAddition
    , SSubtraction
    , SMultiplication
    , SDivision
    , SParenth
    , SSin
    , SCos
    , STan
    , SAsin
    , SAcos
    , SAtan
    , SRadToDeg
    , SDegToRad
    , SLog
    , SExp
    , SSqrt
    , SAbs
    , SPow
    , SAtan2
    , SHypot
    , SMin
    , SMax
    , SFloor
    , SCeil
    , SRound
    , SCondition
    , VConstantZero
    , VConstantX
    , VConstantY
    , VConstantZ
    , VScalars
    , VMultiplication
    , VDivision
    , VAddition
    , VSubtraction
    , VParenth
    , VLength
    , VDot
    , VNormalize
    , VCross
    , VDecay
    , RConstantZero
    , RScalars
    , RAxisAngle
    , RDecayAxis
    , RDecayAngle
    , RDecayAxisVector
    , RFromTo
    , RBasePairs
    , RMultiplication
    , RDivision
    , RParenth
    , RInverse
    , RRotateVector
    , CConstantZero
    , CVectors
    , CRotationOrigin
    , CMultiplication
    , CDivision
    , CParenth
    , CInverse
    , CTranspose
    , CMap
    , CDecayRotation
    , CDecayOrigin
  };
  
  //! @brief Return a string associated to an edge property.
  static inline std::string getVertexTypeString(VertexType typeIn)
  {
    const static std::map<VertexType, std::string> strings =
    {
        {VertexType::None, "None"}
        , {VertexType::Expression, "Expression"}
        , {VertexType::SConstant, "SConstant"}
        , {VertexType::SConstantPI, "SConstantPI"}
        , {VertexType::SConstantE, "SConstantE"}
        , {VertexType::SAddition, "SAddition"}
        , {VertexType::SSubtraction, "SSubtraction"}
        , {VertexType::SMultiplication, "SMultiplication"}
        , {VertexType::SDivision, "SDivision"}
        , {VertexType::SParenth, "SParentheses"}
        , {VertexType::SSin, "SSin"}
        , {VertexType::SCos, "SCos"}
        , {VertexType::STan, "STan"}
        , {VertexType::SAsin, "SAsin"}
        , {VertexType::SAcos, "SAcos"}
        , {VertexType::SAtan, "SAtan"}
        , {VertexType::SRadToDeg, "SRadToDeg"}
        , {VertexType::SDegToRad, "SDegToRad"}
        , {VertexType::SLog, "SLog"}
        , {VertexType::SExp, "SExp"}
        , {VertexType::SSqrt, "SSqrt"}
        , {VertexType::SAbs, "SAbs"}
        , {VertexType::SPow, "SPow"}
        , {VertexType::SAtan2, "SAtan2"}
        , {VertexType::SHypot, "SHypot"}
        , {VertexType::SMin, "SMin"}
        , {VertexType::SMax, "SMax"}
        , {VertexType::SFloor, "SFloor"}
        , {VertexType::SCeil, "SCeil"}
        , {VertexType::SRound, "SRound"}
        , {VertexType::SCondition, "SCondition"}
        , {VertexType::VConstantZero, "VConstantZero"}
        , {VertexType::VConstantX, "VConstantX"}
        , {VertexType::VConstantY, "VConstantY"}
        , {VertexType::VConstantZ, "VConstantZ"}
        , {VertexType::VScalars, "VScalars"}
        , {VertexType::VMultiplication, "VMultiplication"}
        , {VertexType::VDivision, "VDivision"}
        , {VertexType::VAddition, "VAddition"}
        , {VertexType::VSubtraction, "VSubtraction"}
        , {VertexType::VParenth, "VParenth"}
        , {VertexType::VLength, "VLength"}
        , {VertexType::VDot, "VDot"}
        , {VertexType::VNormalize, "VNormalize"}
        , {VertexType::VCross, "VCross"}
        , {VertexType::VDecay, "VDecay"}
        , {VertexType::RConstantZero, "RConstantZero"}
        , {VertexType::RScalars, "RScalars"}
        , {VertexType::RAxisAngle, "RAxisAngle"}
        , {VertexType::RDecayAxis, "RDecayAxis"}
        , {VertexType::RDecayAngle, "RDecayAngle"}
        , {VertexType::RDecayAxisVector, "RDecayAxisVector"}
        , {VertexType::RFromTo, "RFromTo"}
        , {VertexType::RBasePairs, "RBasePairs"}
        , {VertexType::RInverse, "RInverse"}
        , {VertexType::RRotateVector, "RRotateVector"}
        , {VertexType::RMultiplication, "RMultiplication"}
        , {VertexType::RDivision, "RDivision"}
        , {VertexType::RParenth, "RParenth"}
        , {VertexType::CConstantZero, "CConstantZero"}
        , {VertexType::CVectors, "CVectors"}
        , {VertexType::CRotationOrigin, "CRotationOrigin"}
        , {VertexType::CMultiplication, "CMultiplication"}
        , {VertexType::CDivision, "CDivision"}
        , {VertexType::CParenth, "CParenth"}
        , {VertexType::CInverse, "CInverse"}
        , {VertexType::CTranspose, "CTranspose"}
        , {VertexType::CMap, "CMap"}
        , {VertexType::CDecayRotation, "CDecayRotation"}
        , {VertexType::CDecayOrigin, "CDecayOrigin"}
    };
    std::map<VertexType, std::string>::const_iterator it = strings.find(typeIn);
    assert(it != strings.end());
    return it != strings.end() ? it->second : std::string();
  }
}

#endif
