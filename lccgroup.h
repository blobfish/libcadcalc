/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef LCC_GROUP
#define LCC_GROUP

#include <string>
#include <vector>
#include <algorithm>

namespace lcc
{
  struct Group
  {
    std::string name; //!< name of group
    
    Group() = delete;
    Group(const std::string &nameIn, int idIn) : name(nameIn), groupId(idIn){}
    Group(int idIn) : Group(std::to_string(idIn), idIn){}
    void setId(int idIn){groupId = idIn;} //serial only!
    int getId() const {return groupId;}
    
    bool hasExpression(int eId) const
    {
      return std::find(expressionIds.begin(), expressionIds.end(), eId) != expressionIds.end();
    }
    
    void addExpression(int eId)
    {
      if (std::find(expressionIds.begin(), expressionIds.end(), eId) == expressionIds.end())
        expressionIds.emplace_back(eId);
    }
    
    void removeExpression(int eId)
    {
      auto it = std::find(expressionIds.begin(), expressionIds.end(), eId);
      if (it != expressionIds.end())
        expressionIds.erase(it);
    }
    
    const std::vector<int>& getExpressionIds() const {return expressionIds;}
  private:
    int groupId;
    std::vector<int> expressionIds; //!< ids of contained expression
  };
}

#endif
