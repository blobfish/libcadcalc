/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef LCC_RESULT
#define LCC_RESULT

namespace lcc
{
  /*! @struct Result
   * @brief The results of a parse and update operation @see Manager::parseString
   * 
   */
  struct Result
  {
    std::string input; //!< string that was parsed.
    std::string expressionName; //!< name of the expression parsed from input.
    std::string oldExpression; //!< old expression with same name. Can be empty if didn't exist before
    bool parsed = false; //!< true if parsing succeeded otherwise false.
    bool updated = false; //!< true if updating succeeded otherwise false.
    std::string parseErrorMessage = "No Error"; //!< Parsing error message or 'No Error' if parsed.
    std::string updateErrorMessage = "No Error"; //!< Updating error message or 'No Error' if updated.
    int errorPosition = 0; //!< Position in input where parsing failed if applicable.
    std::vector<int> updatedIds; //!< expression ids of potentially changed expression values. Potentially, because we only track dirty state not actual value.
    std::vector<double> value; //!< value of the this successful parse and update otherwise empty.
    
    bool isAllGood() const {return parsed && updated;}
    std::string getError() const
    {
      if (!parsed)
        return parseErrorMessage;
      if (!updated)
        return updateErrorMessage;
      return "No Error";
    }
  };
}

#endif
