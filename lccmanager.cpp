/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "lccstow.h"
#include "lccmanager.h"

namespace pegtl = tao::pegtl;
using namespace lcc;

Manager::Manager()
: stow(std::make_unique<Stow>())
{}

Manager::~Manager() = default;

void Manager::reset()
{
  stow = std::make_unique<Stow>();
}

void Manager::writeOutGraph(const std::string &fpIn)
{
  stow->writeOutGraph(fpIn);
}

/*! @brief parse string and creates or updates the expression
 * 
 * @param sIn is the string to parse,
 * @return Result of operation.
 * 
 * If the parsing fails, the graph is left unchanged.
 * If the updating fails, the graph is restored to previous state.
 */
Result Manager::parseString(const std::string &sIn)
{
  Result out = stow->parse(sIn);
  if (!out.parsed)
    return out;
  if (writeParseTree)
    stow->writeParseTree(out.expressionName + "_parse" + ".dot");

  out = stow->commit();
  
  out = stow->update();
  if (!out.updated)
    return out;
  if (writeExpressionGraph)
    stow->writeOutGraph(out.expressionName + "_expression.dot");
  
  return out;
}

std::vector<std::string> Manager::getExpressionNames() const
{
  return stow->getExpressionNames();
}

std::vector<int> Manager::getExpressionIds() const
{
  return stow->getExpressionIds();
}

bool Manager::hasGroup(const std::string &nameIn) const
{
  return stow->hasGroup(nameIn);
}

bool Manager::hasGroup(int idIn) const
{
  return stow->hasGroup(idIn);
}

int Manager::addGroup(const std::string &nameIn)
{
  return stow->addGroup(nameIn);
}

void Manager::removeGroup(int idIn)
{
  stow->removeGroup(idIn);
}

void Manager::removeGroup(const std::string &nameIn)
{
  stow->removeGroup(nameIn);
}

std::vector<int> Manager::getGroupIds() const
{
  return stow->getGroupIds();
}

std::vector<std::string> Manager::getGroupNames() const
{
  return stow->getGroupNames();
}

std::optional<std::string> Manager::getGroupName(int idIn) const
{
  return stow->getGroupName(idIn);
}

std::optional<int> Manager::getGroupId(const std::string &nameIn) const
{
  return stow->getGroupId(nameIn);
}

void Manager::renameGroup(int idIn, const std::string &nameIn)
{
  stow->renameGroup(idIn, nameIn);
}

void Manager::addExpressionToGroup(int group, int expression)
{
  stow->addExpressionToGroup(group, expression);
}

void Manager::removeExpressionFromGroup(int group, int expression)
{
  stow->removeExpressionFromGroup(group, expression);
}

bool Manager::groupHasExpression(int group, int expression) const
{
  return stow->groupHasExpression(group, expression);
}

const std::vector<int>& Manager::getExpressionsOfGroup(int group) const
{
  return stow->getExpressionsOfGroup(group);
}

bool Manager::hasExpression(const std::string &nameIn) const
{
  return stow->hasExpression(nameIn);
}

bool Manager::hasExpression(int idIn) const
{
  return stow->hasExpression(idIn);
}

std::optional<int> Manager::getExpressionId(const std::string &sIn) const
{
  return stow->getExpressionId(sIn);
}

std::optional<std::string> Manager::getExpressionName(int idIn) const
{
  return stow->getExpressionName(idIn);
}

std::vector<double> Manager::getExpressionValue(const std::string &nameIn) const
{
  return stow->getValue(nameIn);
}

std::vector<double> Manager::getExpressionValue(int idIn) const
{
  return stow->getValue(idIn);
}

void Manager::removeExpression(const std::string &nameIn)
{
  stow->removeExpression(nameIn);
  
  if (writeExpressionGraph)
    stow->writeOutGraph("remove_" + nameIn + "_expression.dot");
}

void Manager::removeExpression(int idIn)
{
  stow->removeExpression(idIn);
  
  if (writeExpressionGraph)
    stow->writeOutGraph("remove_" + *stow->getExpressionName(idIn) + "_expression.dot");
}

void Manager::renameExpression(const std::string &oldName, const std::string &newName)
{
  stow->renameExpression(oldName, newName);
}

void Manager::renameExpression(int idIn, const std::string &newName)
{
  stow->renameExpression(idIn, newName);
}

void Manager::exportExpressions(std::ostream &os) const
{
  stow->exportExpressions(os);
}

void Manager::exportExpressions(std::ostream &os, const std::vector<int> &eIdsIn) const
{
  stow->exportExpressions(os, eIdsIn);
}

std::vector<Result> Manager::importExpressions(std::istream &is)
{
  return stow->importExpressions(is);
}

Serial Manager::serialOut() const
{
  return stow->serialOut();
}

std::vector<Result> Manager::serialIn(const Serial &sIn)
{
  return stow->serialIn(sIn);
}

std::string Manager::buildExpressionString(const std::string &nameIn) const
{
  return stow->buildExpressionString(nameIn);
}

std::string Manager::buildExpressionString(int idIn) const
{
  return stow->buildExpressionString(idIn);
}
