/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef LCC_MANAGER
#define LCC_MANAGER

#include <memory>
#include <optional>

#include "lccresult.h"
#include "lccserial.h"

//! namespace for all things libcadcalc
namespace lcc
{
  struct Stow;
  /*! @struct Manager
   * @brief Central control of expressions
   * @details An expression takes the form of: 'expression_name' = 'expression'.<br>
   * First expression: length = 20.0 <br>
   * expressions can reference other expressions: width = length / 2.0 <br>
   * height = width * 2 / 3 <br>
   * The first three examples demonstrate real or scalar values.<br>
   * We can also create vectors like: myFirstVector = [1.0, 2.0, 3.0]<br>
   * Any supported scalar math can exist in vector definition: mySecondVector = [1.0 * 3/8, 2.0*PI, sqrt(3.0)]<br>
   * Of course we can use scalar expression names also: myThirdVector = [length * 4, width / 2.0, height]<br>
   * 
   */
  struct Manager
  {
  public:
    Manager();
    ~Manager();
    
    //! clears everything.
    void reset();
    //! write out graphviz file(.dot). Should pass in complete path and filename.
    void writeOutGraph(const std::string&);
    
    //! @name Expression operations
    ///@{
    Result parseString(const std::string&);
    //! build the entire expression from name. Maybe empty if doesn't exist
    std::string buildExpressionString(const std::string&) const;
    //! build the entire expression from id. Maybe empty if doesn't exist
    std::string buildExpressionString(int) const;
    //! test whether an expression with given name exists
    bool hasExpression(const std::string&) const;
    //! test whether an expression with given id exists
    bool hasExpression(int) const;
    //! get the optional id of the expression with given name.
    std::optional<int> getExpressionId(const std::string&) const;
    //! get the optional name of the expression with given id.
    std::optional<std::string> getExpressionName(int) const;
    //! remove expression by name.
    void removeExpression(const std::string&);
    //! remove expression by id.
    void removeExpression(int);
    //! rename expression by name. first is old name. second is new name
    void renameExpression(const std::string&, const std::string&);
    //! rename expression by id.
    void renameExpression(int, const std::string&);
    //! get the names of all expressions
    std::vector<std::string> getExpressionNames() const;
    //! get the ids of all expressions
    std::vector<int> getExpressionIds() const;
    //! export topologically sorted list of expressions to stream.
    void exportExpressions(std::ostream&) const;
    //! export topologically sorted subset of expressions to stream.
    void exportExpressions(std::ostream&, const std::vector<int>&) const;
    //! import expressions from stream.
    std::vector<Result> importExpressions(std::istream&);
    ///@}
    
    /*! @name Expression values
     * get values associated to expression name or id. <br>
     * Scalar is 1 value <br>
     * Vector is 3 values. Respectively x, y, z <br>
     * Rotation(quaternion) is 4 values. Respectively x, y, z, w <br>
     * CSys is 7 values. Respectively Rotation, Vector.
     */
    ///@{
    std::vector<double> getExpressionValue(const std::string&) const; //!< Get value from name. Can be empty if doesn't exist.
    std::vector<double> getExpressionValue(int) const; //!< Get value from id. Can be empty if doesn't exist.
    ///@}
    
    //! @name Group operations
    ///@{
    bool hasGroup(const std::string&) const; //!< return true if we have a group with name. else false.
    bool hasGroup(int) const; //!< return true if we have a group with id. else false.
    int addGroup(const std::string&); //!< add a new group with given name and return it's id.
    void removeGroup(int); //!< remove group with given id.
    void removeGroup(const std::string&); //!< remove group with given name.
    std::vector<int> getGroupIds() const; //!< get all group ids
    std::vector<std::string> getGroupNames() const; //!< get all group names
    std::optional<std::string> getGroupName(int) const; //!< get the optional name of the group with given id.
    std::optional<int> getGroupId(const std::string&) const; //!< get the optional id of the group with given name.
    void renameGroup(int, const std::string&); //!< rename the group with given id to the given new name.
    ///@}
    
    //! @name Group Expression interaction
    ///@{
    void addExpressionToGroup(int group, int expression); //!< add the expression to the group.
    void removeExpressionFromGroup(int group, int expression); //!< remove the expression from the group.
    bool groupHasExpression(int group, int expression) const; //!< return true if group has expression, otherwise false.
    const std::vector<int>& getExpressionsOfGroup(int group) const; //!< return all the expression ids belonging to given group.
    ///@}

    /// \cond
    bool writeParseTree = false; // useful debug
    bool writeExpressionGraph = false; // useful debug
    /// \endcond
    
    //! @name Serialize groups and expressions
    ///@{
    //! Return a serial object of the current manager state.
    Serial serialOut() const;
    //! Restore manager from a serial object. Call reset before if needed. Report only error results.
    std::vector<Result> serialIn(const Serial&);
    ///@}
    
  private:
    std::unique_ptr<Stow> stow;
  };
}

#endif
