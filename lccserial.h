/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef LCC_SERIAL
#define LCC_SERIAL

#include <string>
#include <vector>

namespace lcc
{
  struct SExpression
  {
    int id;
    std::string expression;
  };
  
  struct SGroup
  {
    int id;
    std::string name;
    std::vector<int> expressionIds;
  };
  
  struct Serial
  {
    std::vector<SExpression> expressions;
    std::vector<SGroup> groups;
  };
}

#endif
