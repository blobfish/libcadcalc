/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef LCC_GRAPH
#define LCC_GRAPH

#include <variant>
#include <map>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/breadth_first_search.hpp>

#include <Eigen/Core>
// #include <eigen3/Eigen/Geometry>

#include <Eigen/SVD>
#include <Eigen/LU>
#include <Eigen/src/Geometry/OrthoMethods.h>
#include <Eigen/src/Geometry/RotationBase.h>
#include <Eigen/src/Geometry/Quaternion.h>
#include <Eigen/src/Geometry/AngleAxis.h>
#include <Eigen/src/Geometry/Transform.h>

#include "lccvaluetype.h"
#include "lccvertextype.h"

/*! We are using a std::vector for the storage base of our graph, so we have to consider invalidation of the vertices.
   * The vertex invalidation is unacceptable as it invalidates the edges that reference them and 'breaks' the graph.
   * The edges are not a problem as we can remove edges and the graph will remain valid as nothing inside the graph
   * references the edges. So we added an 'alive' member to our vertex property to filter out dead vertices. See
   * IntrusiveFilter and IntrusiveGraph. Now it would be nice to have our IntrusiveGraph a member of stow, so we could
   * set it up once at the start of an operation. We can't do that as filtered_graph keeps a const ref to the graph
   * restricting the constructor generated assignment operator. We can get around this by setting the 'filterMe' members
   * to the appropriate values at the start of an operation and build the filtered graph where needed.
   * This should be acceptable as the construction of filters and filtered_graph should be fast as they are light.
   * We have created several functions to set the filter state of the graph as needed by various operations. The default
   * state of the graph can be set by calling setFilterOnlyDead. This will just filter out the vertices that have been
   * deleted. This default state should be maintained, meaning any operations internal to stow that need to use the other
   * filters methods, should ensure the default state is restored. A 'filterMe' has been added to the graph edge property
   * but isn't being used at this time.
   */

namespace lcc
{
  struct None{}; //signal for new expression vertex, so we can assign and subsequently check types.
  using Scalar = double;
  using Vector = Eigen::Vector3d;
  using Rotation = Eigen::Quaterniond;
  using CSys = Eigen::Affine3d;
  
  using Variant = std::variant<None, double, Vector, Rotation, CSys>;
  struct StringVisitor
  {
    StringVisitor(std::string &sIn) : out(sIn){}
    std::string &out;
    void operator()(const None&){out = "None";}
    void operator()(const double& dIn){out = buildString(dIn);}
    void operator()(const Vector &vIn){out = buildString(vIn);}
    void operator()(const Rotation &rIn){out = buildString(rIn);}
    void operator()(const CSys &cIn){out = buildString(cIn);}
    
    static std::string buildString(const double &dIn)
    {
      return std::to_string(dIn);
    }
    
    static std::string buildString(const Vector &vIn)
    {
      std::ostringstream stream;
      stream << "[" << buildString(vIn.x())
      << ", " << buildString(vIn.y())
      << ", " << buildString(vIn.z())
      << "]";
      return stream.str();
    }
    
    static std::string buildString(const Rotation &rIn)
    {
      std::ostringstream stream;
      stream << "[" << buildString(rIn.x())
      << ", " << buildString(rIn.y())
      << ", " << buildString(rIn.z())
      << ", " << buildString(rIn.w())
      << "]";
      return stream.str();
    }
    
    static std::string buildString(const CSys &cIn)
    {
      std::ostringstream stream;
      stream << "[" << buildString(Rotation(cIn.rotation()))
      << ", " << buildString(cIn.translation())
      << "]";
      return stream.str();
    }
  };
  struct DoubleVisitor
  {
    std::vector<double> &v;
    DoubleVisitor(std::vector<double> &vIn) : v(vIn) {}
    void operator()(const None&){assert(0);}
    void operator()(const double &dIn){v.push_back(dIn);}
    void operator()(const Vector &vIn){v.push_back(vIn.x()); v.push_back(vIn.y()); v.push_back(vIn.z());}
    void operator()(const Rotation &rIn){v.push_back(rIn.x()); v.push_back(rIn.y()); v.push_back(rIn.z()); v.push_back(rIn.w());}
    void operator()(const CSys &cIn)
    {
      auto rIn = Rotation(cIn.linear());
      v.push_back(rIn.x());
      v.push_back(rIn.y());
      v.push_back(rIn.z());
      v.push_back(rIn.w());
      auto tIn = cIn.translation();
      v.push_back(tIn.x());
      v.push_back(tIn.y());
      v.push_back(tIn.z());
    }
  };
  
  struct VertexProperty
  {
    VertexProperty() = default;
    VertexProperty(VertexType vt) : vertexType(vt){}
    VertexType vertexType;
    Variant value;
    std::string name; //used only for expression node
    int id = 0; //used only for expression node
    bool alive = true;
    bool dirty = true;
    bool constant = false; //constant nodes won't get dirty and don't need update
    bool filterMe = false;
    
    ValueType valueType() const {return static_cast<ValueType>(value.index());}
    void setDirty(){dirty = true;}
    void setClean(){dirty = false;}
    bool isDirty() const {return dirty;}
    bool isClean() const {return !dirty;}
  };
  
  /*! @brief Edge properties are attached to graph edges.
  * 
  * These properties indicate child node relevance to parent operation. @see Graph
  */
  enum class EdgeTag
  {
    None,
    Lhs,
    Rhs,
    Parameter1,
    Parameter2,
    Then,
    Else,
    X,
    Y,
    Z,
    W,
    Vector,
    Rotation,
    Angle,
    From,
    To,
    Axis,
    Origin,
    GreaterEqual,
    LessEqual,
    Greater,
    Less,
    Equal,
    NotEqual
  };
  
  //! @brief Return a string associated to an edge property.
  static inline std::string getEdgePropertyString(const EdgeTag &property)
  {
    const static std::map<EdgeTag, std::string> strings =
    {
        std::make_pair(EdgeTag::None, "None")
        , std::make_pair(EdgeTag::Lhs, "LHS")
        , std::make_pair(EdgeTag::Rhs, "RHS")
        , std::make_pair(EdgeTag::Parameter1, "Parameter1")
        , std::make_pair(EdgeTag::Parameter2, "Parameter2")
        , std::make_pair(EdgeTag::Then, "Then")
        , std::make_pair(EdgeTag::Else, "Else")
        , std::make_pair(EdgeTag::X, "X")
        , std::make_pair(EdgeTag::Y, "Y")
        , std::make_pair(EdgeTag::Z, "Z")
        , std::make_pair(EdgeTag::W, "W")
        , std::make_pair(EdgeTag::Vector, "Vector")
        , std::make_pair(EdgeTag::Rotation, "Rotation")
        , std::make_pair(EdgeTag::Angle, "Angle")
        , std::make_pair(EdgeTag::From, "From")
        , std::make_pair(EdgeTag::To, "To")
        , std::make_pair(EdgeTag::Axis, "Axis")
        , std::make_pair(EdgeTag::Origin, "Origin")
        , std::make_pair(EdgeTag::GreaterEqual, "GreaterEqual")
        , std::make_pair(EdgeTag::LessEqual, "LessEqual")
        , std::make_pair(EdgeTag::Greater, "Greater")
        , std::make_pair(EdgeTag::Less, "Less")
        , std::make_pair(EdgeTag::Equal, "Equal")
        , std::make_pair(EdgeTag::NotEqual, "NotEqual")
    };
    std::map<EdgeTag, std::string>::const_iterator it = strings.find(property);
    assert(it != strings.end());
    return it != strings.end() ? it->second : std::string();
  }
  
  struct EdgeProperty
  {
    EdgeTag tag;
    bool filterMe = false;
  };
  
  using Graph = boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, VertexProperty, EdgeProperty>;
  using Vertex = boost::graph_traits<Graph>::vertex_descriptor;
  using Edge = boost::graph_traits<Graph>::edge_descriptor;
  
  struct IntrusiveFilter
  {
    IntrusiveFilter() : g(nullptr) { }
    IntrusiveFilter(const Graph &gIn) : g(&gIn) { }
    bool operator()(const Vertex &vIn) const
    {
      if(!g)
        return false;
      
      return !(*g)[vIn].filterMe;
    }
    bool operator()(const Edge &eIn) const
    {
      if(!g)
        return false;
      
      return !(*g)[eIn].filterMe;
    }
    const Graph *g = nullptr;
  };
  
  using IntrusiveGraph = boost::filtered_graph<Graph, IntrusiveFilter, IntrusiveFilter>;
  inline IntrusiveGraph buildIntrusiveGraph(Graph &gIn)
  {
    IntrusiveFilter f(gIn);
    IntrusiveGraph out(gIn, f, f);
    return out;
  }
  inline IntrusiveGraph buildIntrusiveGraph(const Graph &gIn)
  {
    IntrusiveFilter f(gIn);
    IntrusiveGraph out(gIn, f, f);
    return out;
  }
  
  /*! @brief Accumulates dependent vertices 
  * A breadth first visitor accumulating all visited vertices
  */
  class AccumulateVisitor : public boost::default_bfs_visitor
  {
  public:
    AccumulateVisitor() = delete;
    AccumulateVisitor(std::vector<Vertex> &vsIn) : vertices(vsIn){}
    
    template<typename V, typename G>
    void discover_vertex(V vertex, G&)
    {
      vertices.push_back(vertex);
    }
  private:
    std::vector<Vertex> &vertices;
  };
}

#endif
