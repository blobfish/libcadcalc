/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LCC_STOW_H
#define LCC_STOW_H

#include <functional>
#include <map>

#include <tao/pegtl/demangle.hpp>
#include <tao/pegtl/string_input.hpp>
#include <tao/pegtl/contrib/parse_tree.hpp>

#include "lccgrammar.h"
#include "lccgraph.h"
#include "lccresult.h"
#include "lccgroup.h"
#include "lccserial.h"

/* notes:
 *  tracing appears to be an area under development.
 *    Github example doesn't match code here in debian, which is the latest release.
 */

namespace tao{namespace pegtl{namespace parse_tree{struct node;}}}

namespace lcc
{
  struct Stow
  {
    Graph graph;
    int nextExpressionId = 1;
    int nextGroupId = 1;
    std::vector<Group> groups;
    
    std::string currentlyParsingExpression; //to make sure expression doesn't reference itself
    std::vector<Vertex> currentParsingDependents; //to detect and prevent cycles in parsing stage
    
    std::unique_ptr<tao::pegtl::string_input<>> inputString;
    std::unique_ptr<tao::pegtl::parse_tree::node> root; //root node of last parse.
    Result result; //current state of 3 step process.
    
    Stow();
    //3 step process: parse, commit and update.
    Result parse(const std::string&); //only parses. no changes to graph. use while user is typing
    Result commit(); //alter graph to last parse result.
    Result update(); //calculate all values
    
    void writeParseTree(const std::string&);
    void writeOutGraph(const std::string&);
    void exportExpressions(std::ostream&) const;
    void exportExpressions(std::ostream&, const std::vector<int>&) const;
    std::vector<Result> importExpressions(std::istream&);
    Serial serialOut() const;
    std::vector<Result> serialIn(const Serial&);
    void deriveNextExpressionId();
    void deriveNextGroupId();
    
    std::vector<std::string> getExpressionNames() const;
    std::vector<int> getExpressionIds() const;
    bool hasExpression(const std::string&) const;
    bool hasExpression(int) const;
    std::optional<int> getExpressionId(const std::string&) const;
    std::optional<std::string> getExpressionName(int) const;
    
    bool hasGroup(const std::string&) const;
    bool hasGroup(int) const;
    int addGroup(const std::string&);
    void removeGroup(int);
    void removeGroup(const std::string&);
    std::vector<int> getGroupIds() const;
    std::vector<std::string> getGroupNames() const;
    std::optional<std::string> getGroupName(int) const;
    std::optional<int> getGroupId(const std::string&) const;
    void renameGroup(int, const std::string&);
    
    void addExpressionToGroup(int group, int expression);
    void removeExpressionFromGroup(int group, int expression);
    bool groupHasExpression(int group, int expression) const;
    const std::vector<int>& getExpressionsOfGroup(int group) const;
    
    std::optional<Vertex> findExpression(const std::string&) const;
    std::optional<Vertex> findExpression(int) const;
    std::optional<Vertex> findExpression(std::string_view) const;
    Vertex buildExpression(std::string_view);
    void cleanExpression(Vertex);
    void removeExpression(const std::string&);
    void removeExpression(int);
    void removeExpression(Vertex);
    void renameExpression(const std::string&, const std::string&);
    void renameExpression(int, const std::string&);
    void markDependentsDirty(Vertex);
    std::vector<Vertex> getDependents(Vertex);
    
    void setFilterOnlyDead(); //!< like a clear and filters dead. default filter state.
    void setFilterConstants();
    void setFilterExpressions();
    
    std::map<VertexType, Vertex> constantsMap; 
    
    Edge connect(Vertex, Vertex, EdgeTag);
    std::optional<Vertex> findChildOfTag(Vertex, EdgeTag) const;
    
    std::string buildExpressionString(const std::string&) const;
    std::string buildExpressionString(int) const;
    std::string buildExpressionString(Vertex) const;
    std::vector<double> getValue(const std::string&);
    std::vector<double> getValue(int);
    std::vector<double> getValue(Vertex);
    
    Vertex goTRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&); //called from manager.
    Vertex buildSConstant(const double&);
    Vertex goTSRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSParenth(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSFunction1(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSSin(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSCos(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSTan(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSAsin(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSAcos(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSAtan(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSRadToDeg(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSDegToRad(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSLog(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSExp(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSSqrt(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSAbs(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSFunction2(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSPow(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSAtan2(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSHypot(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSMin(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSMax(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSFloor(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSCeil(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSRound(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSCondition(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSAddition(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSSubtraction(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSTerm(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSMultiplication(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSDivision(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSID(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSConstantPI(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goSConstantE(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goDecimal(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex buildVConstant(const Vector&);
    Vertex goTVRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVScalars(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVID(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVConstantX(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVConstantY(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVConstantZ(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVConstantZero(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVDecay(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVLength(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVDot(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVNormalize(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVCross(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVTerm(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVMultiplication(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVDivision(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVAddition(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVSubtraction(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goVParenth(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex buildRConstant(const Rotation&);
    Vertex goTRRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRConstantZero(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRScalars(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRID(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRAxisAngle(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRFromTo(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRBasePairs(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRDecayAxis(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRDecayAngle(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRDecayAxisVector(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRInverse(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRRotateVector(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRTerm(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRMultiplication(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRDivision(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goRParenth(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex buildCConstant(const CSys&);
    Vertex goTCRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCRHS(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCVectors(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCID(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCRotationOrigin(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCConstantZero(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCInverse(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCMap(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCTerm(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCMultiplication(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCDivision(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCParenth(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCDecayRotation(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    Vertex goCDecayOrigin(const std::unique_ptr<::tao::pegtl::parse_tree::node>&);
    
    using BuildFunction = std::function<Vertex (const std::unique_ptr<::tao::pegtl::parse_tree::node>&)>;
    std::map<std::string_view, BuildFunction> buildFunctionMap =
    {
      {::tao::pegtl::demangle<grm::tsrhs>(), std::bind(&Stow::goTSRHS, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sparenth>(), std::bind(&Stow::goSParenth, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::srhs>(), std::bind(&Stow::goSRHS, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sfunction1>(), std::bind(&Stow::goSFunction1, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::ssin>(), std::bind(&Stow::goSSin, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::scos>(), std::bind(&Stow::goSCos, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::stan>(), std::bind(&Stow::goSTan, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sasin>(), std::bind(&Stow::goSAsin, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sacos>(), std::bind(&Stow::goSAcos, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::satan>(), std::bind(&Stow::goSAtan, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sradtodeg>(), std::bind(&Stow::goSRadToDeg, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sdegtorad>(), std::bind(&Stow::goSDegToRad, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::slog>(), std::bind(&Stow::goSLog, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sexp>(), std::bind(&Stow::goSExp, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::ssqrt>(), std::bind(&Stow::goSSqrt, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sabs>(), std::bind(&Stow::goSAbs, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sfunction2>(), std::bind(&Stow::goSFunction2, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::spow>(), std::bind(&Stow::goSPow, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::satan2>(), std::bind(&Stow::goSAtan2, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::shypot>(), std::bind(&Stow::goSHypot, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::smin>(), std::bind(&Stow::goSMin, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::smax>(), std::bind(&Stow::goSMax, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sfloor>(), std::bind(&Stow::goSFloor, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sceil>(), std::bind(&Stow::goSCeil, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sround>(), std::bind(&Stow::goSRound, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::scondition>(), std::bind(&Stow::goSCondition, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::saddition>(), std::bind(&Stow::goSAddition, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::ssubtraction>(), std::bind(&Stow::goSSubtraction, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sterm>(), std::bind(&Stow::goSTerm, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::smultiplication>(), std::bind(&Stow::goSMultiplication, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sdivision>(), std::bind(&Stow::goSDivision, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sid>(), std::bind(&Stow::goSID, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sconstantpi>(), std::bind(&Stow::goSConstantPI, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::sconstante>(), std::bind(&Stow::goSConstantE, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::decimal>(), std::bind(&Stow::goDecimal, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::tvrhs>(), std::bind(&Stow::goTVRHS, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vrhs>(), std::bind(&Stow::goVRHS, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vscalars>(), std::bind(&Stow::goVScalars, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vid>(), std::bind(&Stow::goVID, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::axisx>(), std::bind(&Stow::goVConstantX, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::axisy>(), std::bind(&Stow::goVConstantY, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::axisz>(), std::bind(&Stow::goVConstantZ, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vconstantzero>(), std::bind(&Stow::goVConstantZero, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vdecay>(), std::bind(&Stow::goVDecay, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vlength>(), std::bind(&Stow::goVLength, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vdot>(), std::bind(&Stow::goVDot, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vnormalize>(), std::bind(&Stow::goVNormalize, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vcross>(), std::bind(&Stow::goVCross, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vterm>(), std::bind(&Stow::goVTerm, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vmultiplication>(), std::bind(&Stow::goVMultiplication, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vdivision>(), std::bind(&Stow::goVDivision, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vaddition>(), std::bind(&Stow::goVAddition, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vsubtraction>(), std::bind(&Stow::goVSubtraction, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::vparenth>(), std::bind(&Stow::goVParenth, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::trrhs>(), std::bind(&Stow::goTRRHS, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rrhs>(), std::bind(&Stow::goTRRHS, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rconstantzero>(), std::bind(&Stow::goRConstantZero, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rscalars>(), std::bind(&Stow::goRScalars, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rid>(), std::bind(&Stow::goRID, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::raxisangle>(), std::bind(&Stow::goRAxisAngle, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rfromto>(), std::bind(&Stow::goRFromTo, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rbasepairs>(), std::bind(&Stow::goRBasePairs, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rdecayaxis>(), std::bind(&Stow::goRDecayAxis, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rdecayangle>(), std::bind(&Stow::goRDecayAngle, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rdecayaxisvector>(), std::bind(&Stow::goRDecayAxisVector, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rinverse>(), std::bind(&Stow::goRInverse, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rrotatevector>(), std::bind(&Stow::goRRotateVector, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rterm>(), std::bind(&Stow::goRTerm, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rmultiplication>(), std::bind(&Stow::goRMultiplication, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rdivision>(), std::bind(&Stow::goRDivision, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::rparenth>(), std::bind(&Stow::goRParenth, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::tcrhs>(), std::bind(&Stow::goTCRHS, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::crhs>(), std::bind(&Stow::goCRHS, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cvectors>(), std::bind(&Stow::goCVectors, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cid>(), std::bind(&Stow::goCID, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::crotationorigin>(), std::bind(&Stow::goCRotationOrigin, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cconstantzero>(), std::bind(&Stow::goCConstantZero, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cinverse>(), std::bind(&Stow::goCInverse, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cmap>(), std::bind(&Stow::goCMap, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cterm>(), std::bind(&Stow::goCTerm, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cmultiplication>(), std::bind(&Stow::goCMultiplication, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cdivision>(), std::bind(&Stow::goCDivision, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cparenth>(), std::bind(&Stow::goCParenth, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cdecayrotation>(), std::bind(&Stow::goCDecayRotation, this, std::placeholders::_1)}
      , {::tao::pegtl::demangle<grm::cdecayorigin>(), std::bind(&Stow::goCDecayOrigin, this, std::placeholders::_1)}
    };
    
    void updateExpression(Vertex);
    void updateSMultiplication(Vertex);
    void updateSDivision(Vertex);
    void updateSAddition(Vertex);
    void updateSSubtraction(Vertex);
    void updateSParenth(Vertex);
    void updateSSin(Vertex);
    void updateSCos(Vertex);
    void updateSTan(Vertex);
    void updateSAsin(Vertex);
    void updateSAcos(Vertex);
    void updateSAtan(Vertex);
    void updateSRadToDeg(Vertex);
    void updateSDegToRad(Vertex);
    void updateSLog(Vertex);
    void updateSExp(Vertex);
    void updateSSqrt(Vertex);
    void updateSAbs(Vertex);
    void updateSPow(Vertex);
    void updateSAtan2(Vertex);
    void updateSHypot(Vertex);
    void updateSMin(Vertex);
    void updateSMax(Vertex);
    void updateSFloor(Vertex);
    void updateSCeil(Vertex);
    void updateSRound(Vertex);
    void updateSCondition(Vertex);
    void updateSNotEqual(Vertex);
    void updateVScalars(Vertex);
    void updateVDecay(Vertex);
    void updateVLength(Vertex);
    void updateVDot(Vertex);
    void updateVNormalize(Vertex);
    void updateVCross(Vertex);
    void updateVMultiplication(Vertex);
    void updateVDivision(Vertex);
    void updateVAddition(Vertex);
    void updateVSubtraction(Vertex);
    void updateVParenth(Vertex);
    void updateRScalars(Vertex);
    void updateRAxisAngle(Vertex);
    void updateRBasePairs(Vertex);
    void updateRFromTo(Vertex);
    void updateRDecayAxis(Vertex);
    void updateRDecayAngle(Vertex);
    void updateRDecayAxisVector(Vertex);
    void updateRInverse(Vertex);
    void updateRRotateVector(Vertex);
    void updateRMultiplication(Vertex);
    void updateRDivision(Vertex);
    void updateRParenth(Vertex);
    void updateCVectors(Vertex);
    void updateCRotationOrigin(Vertex);
    void updateCConstantZero(Vertex);
    void updateCInverse(Vertex);
    void updateCMap(Vertex);
    void updateCMultiplication(Vertex);
    void updateCDivision(Vertex);
    void updateCParenth(Vertex);
    void updateCDecayRotation(Vertex);
    void updateCDecayOrigin(Vertex);
    using UpdateFunction = std::function<void (Vertex)>;
    std::map<VertexType, UpdateFunction> updateFunctionMap =
    {
      {VertexType::Expression, std::bind(&Stow::updateExpression, this, std::placeholders::_1)}
      , {VertexType::SMultiplication, std::bind(&Stow::updateSMultiplication, this, std::placeholders::_1)}
      , {VertexType::SDivision, std::bind(&Stow::updateSDivision, this, std::placeholders::_1)}
      , {VertexType::SAddition, std::bind(&Stow::updateSAddition, this, std::placeholders::_1)}
      , {VertexType::SSubtraction, std::bind(&Stow::updateSSubtraction, this, std::placeholders::_1)}
      , {VertexType::SParenth, std::bind(&Stow::updateSParenth, this, std::placeholders::_1)}
      , {VertexType::SSin, std::bind(&Stow::updateSSin, this, std::placeholders::_1)}
      , {VertexType::SCos, std::bind(&Stow::updateSCos, this, std::placeholders::_1)}
      , {VertexType::STan, std::bind(&Stow::updateSTan, this, std::placeholders::_1)}
      , {VertexType::SAsin, std::bind(&Stow::updateSAsin, this, std::placeholders::_1)}
      , {VertexType::SAcos, std::bind(&Stow::updateSAcos, this, std::placeholders::_1)}
      , {VertexType::SAtan, std::bind(&Stow::updateSAtan, this, std::placeholders::_1)}
      , {VertexType::SRadToDeg, std::bind(&Stow::updateSRadToDeg, this, std::placeholders::_1)}
      , {VertexType::SDegToRad, std::bind(&Stow::updateSDegToRad, this, std::placeholders::_1)}
      , {VertexType::SLog, std::bind(&Stow::updateSLog, this, std::placeholders::_1)}
      , {VertexType::SExp, std::bind(&Stow::updateSExp, this, std::placeholders::_1)}
      , {VertexType::SSqrt, std::bind(&Stow::updateSSqrt, this, std::placeholders::_1)}
      , {VertexType::SAbs, std::bind(&Stow::updateSAbs, this, std::placeholders::_1)}
      , {VertexType::SPow, std::bind(&Stow::updateSPow, this, std::placeholders::_1)}
      , {VertexType::SAtan2, std::bind(&Stow::updateSAtan2, this, std::placeholders::_1)}
      , {VertexType::SHypot, std::bind(&Stow::updateSHypot, this, std::placeholders::_1)}
      , {VertexType::SMin, std::bind(&Stow::updateSMin, this, std::placeholders::_1)}
      , {VertexType::SMax, std::bind(&Stow::updateSMax, this, std::placeholders::_1)}
      , {VertexType::SFloor, std::bind(&Stow::updateSFloor, this, std::placeholders::_1)}
      , {VertexType::SCeil, std::bind(&Stow::updateSCeil, this, std::placeholders::_1)}
      , {VertexType::SRound, std::bind(&Stow::updateSRound, this, std::placeholders::_1)}
      , {VertexType::SCondition, std::bind(&Stow::updateSCondition, this, std::placeholders::_1)}
      , {VertexType::VScalars, std::bind(&Stow::updateVScalars, this, std::placeholders::_1)}
      , {VertexType::VDecay, std::bind(&Stow::updateVDecay, this, std::placeholders::_1)}
      , {VertexType::VLength, std::bind(&Stow::updateVLength, this, std::placeholders::_1)}
      , {VertexType::VDot, std::bind(&Stow::updateVDot, this, std::placeholders::_1)}
      , {VertexType::VNormalize, std::bind(&Stow::updateVNormalize, this, std::placeholders::_1)}
      , {VertexType::VCross, std::bind(&Stow::updateVCross, this, std::placeholders::_1)}
      , {VertexType::VMultiplication, std::bind(&Stow::updateVMultiplication, this, std::placeholders::_1)}
      , {VertexType::VDivision, std::bind(&Stow::updateVDivision, this, std::placeholders::_1)}
      , {VertexType::VAddition, std::bind(&Stow::updateVAddition, this, std::placeholders::_1)}
      , {VertexType::VSubtraction, std::bind(&Stow::updateVSubtraction, this, std::placeholders::_1)}
      , {VertexType::VParenth, std::bind(&Stow::updateVParenth, this, std::placeholders::_1)}
      , {VertexType::RScalars, std::bind(&Stow::updateRScalars, this, std::placeholders::_1)}
      , {VertexType::RAxisAngle, std::bind(&Stow::updateRAxisAngle, this, std::placeholders::_1)}
      , {VertexType::RFromTo, std::bind(&Stow::updateRFromTo, this, std::placeholders::_1)}
      , {VertexType::RBasePairs, std::bind(&Stow::updateRBasePairs, this, std::placeholders::_1)}
      , {VertexType::RDecayAxis, std::bind(&Stow::updateRDecayAxis, this, std::placeholders::_1)}
      , {VertexType::RDecayAngle, std::bind(&Stow::updateRDecayAngle, this, std::placeholders::_1)}
      , {VertexType::RDecayAxisVector, std::bind(&Stow::updateRDecayAxisVector, this, std::placeholders::_1)}
      , {VertexType::RInverse, std::bind(&Stow::updateRInverse, this, std::placeholders::_1)}
      , {VertexType::RRotateVector, std::bind(&Stow::updateRRotateVector, this, std::placeholders::_1)}
      , {VertexType::RMultiplication, std::bind(&Stow::updateRMultiplication, this, std::placeholders::_1)}
      , {VertexType::RDivision, std::bind(&Stow::updateRDivision, this, std::placeholders::_1)}
      , {VertexType::RParenth, std::bind(&Stow::updateRParenth, this, std::placeholders::_1)}
      , {VertexType::CVectors, std::bind(&Stow::updateCVectors, this, std::placeholders::_1)}
      , {VertexType::CRotationOrigin, std::bind(&Stow::updateCRotationOrigin, this, std::placeholders::_1)}
      , {VertexType::CConstantZero, std::bind(&Stow::updateCConstantZero, this, std::placeholders::_1)}
      , {VertexType::CInverse, std::bind(&Stow::updateCInverse, this, std::placeholders::_1)}
      , {VertexType::CMap, std::bind(&Stow::updateCMap, this, std::placeholders::_1)}
      , {VertexType::CMultiplication, std::bind(&Stow::updateCMultiplication, this, std::placeholders::_1)}
      , {VertexType::CDivision, std::bind(&Stow::updateCDivision, this, std::placeholders::_1)}
      , {VertexType::CParenth, std::bind(&Stow::updateCParenth, this, std::placeholders::_1)}
      , {VertexType::CDecayRotation, std::bind(&Stow::updateCDecayRotation, this, std::placeholders::_1)}
      , {VertexType::CDecayOrigin, std::bind(&Stow::updateCDecayOrigin, this, std::placeholders::_1)}
    };
    
    std::string toStringExpression(Vertex) const;
    std::string toStringSConstant(Vertex) const;
    std::string toStringSConstantPI(Vertex) const;
    std::string toStringSConstantE(Vertex) const;
    std::string toStringSMultiplication(Vertex) const;
    std::string toStringSDivision(Vertex) const;
    std::string toStringSAddition(Vertex) const;
    std::string toStringSSubtraction(Vertex) const;
    std::string toStringSParenth(Vertex) const;
    std::string toStringSSin(Vertex) const;
    std::string toStringSCos(Vertex) const;
    std::string toStringSTan(Vertex) const;
    std::string toStringSAsin(Vertex) const;
    std::string toStringSAcos(Vertex) const;
    std::string toStringSAtan(Vertex) const;
    std::string toStringSRadToDeg(Vertex) const;
    std::string toStringSDegToRad(Vertex) const;
    std::string toStringSLog(Vertex) const;
    std::string toStringSExp(Vertex) const;
    std::string toStringSSqrt(Vertex) const;
    std::string toStringSAbs(Vertex) const;
    std::string toStringSPow(Vertex) const;
    std::string toStringSAtan2(Vertex) const;
    std::string toStringSHypot(Vertex) const;
    std::string toStringSMin(Vertex) const;
    std::string toStringSMax(Vertex) const;
    std::string toStringSFloor(Vertex) const;
    std::string toStringSCeil(Vertex) const;
    std::string toStringSRound(Vertex) const;
    std::string toStringSCondition(Vertex) const;
    std::string toStringVScalars(Vertex) const;
    std::string toStringVConstantX(Vertex) const;
    std::string toStringVConstantY(Vertex) const;
    std::string toStringVConstantZ(Vertex) const;
    std::string toStringVConstantZero(Vertex) const;
    std::string toStringVDecay(Vertex) const;
    std::string toStringVLength(Vertex) const;
    std::string toStringVDot(Vertex) const;
    std::string toStringVNormalize(Vertex) const;
    std::string toStringVCross(Vertex) const;
    std::string toStringVMultiplication(Vertex) const;
    std::string toStringVDivision(Vertex) const;
    std::string toStringVAddition(Vertex) const;
    std::string toStringVSubtraction(Vertex) const;
    std::string toStringVParenth(Vertex) const;
    std::string toStringRConstantZero(Vertex) const;
    std::string toStringRScalars(Vertex) const;
    std::string toStringRAxisAngle(Vertex) const;
    std::string toStringRFromTo(Vertex) const;
    std::string toStringRDecayAxis(Vertex) const;
    std::string toStringRDecayAngle(Vertex) const;
    std::string toStringRDecayAxisVector(Vertex) const;
    std::string toStringRBasePairs(Vertex) const;
    std::string toStringRInverse(Vertex) const;
    std::string toStringRMultiplication(Vertex) const;
    std::string toStringRDivision(Vertex) const;
    std::string toStringRParenth(Vertex) const;
    std::string toStringRRotateVector(Vertex) const;
    std::string toStringCVectors(Vertex) const;
    std::string toStringCRotationOrigin(Vertex) const;
    std::string toStringCConstantZero(Vertex) const;
    std::string toStringCInverse(Vertex) const;
    std::string toStringCMap(Vertex) const;
    std::string toStringCMultiplication(Vertex) const;
    std::string toStringCDivision(Vertex) const;
    std::string toStringCParenth(Vertex) const;
    std::string toStringCDecayRotation(Vertex) const;
    std::string toStringCDecayOrigin(Vertex) const;
    
    using StringFunction = std::function<std::string (Vertex)>;
    std::map<VertexType, StringFunction> stringFunctionMap =
    {
      {VertexType::Expression, std::bind(&Stow::toStringExpression, this, std::placeholders::_1)}
      , {VertexType::SConstant, std::bind(&Stow::toStringSConstant, this, std::placeholders::_1)}
      , {VertexType::SConstantPI, std::bind(&Stow::toStringSConstantPI, this, std::placeholders::_1)}
      , {VertexType::SConstantE, std::bind(&Stow::toStringSConstantE, this, std::placeholders::_1)}
      , {VertexType::SMultiplication, std::bind(&Stow::toStringSMultiplication, this, std::placeholders::_1)}
      , {VertexType::SDivision, std::bind(&Stow::toStringSDivision, this, std::placeholders::_1)}
      , {VertexType::SAddition, std::bind(&Stow::toStringSAddition, this, std::placeholders::_1)}
      , {VertexType::SSubtraction, std::bind(&Stow::toStringSSubtraction, this, std::placeholders::_1)}
      , {VertexType::SParenth, std::bind(&Stow::toStringSParenth, this, std::placeholders::_1)}
      , {VertexType::SSin, std::bind(&Stow::toStringSSin, this, std::placeholders::_1)}
      , {VertexType::SCos, std::bind(&Stow::toStringSCos, this, std::placeholders::_1)}
      , {VertexType::STan, std::bind(&Stow::toStringSTan, this, std::placeholders::_1)}
      , {VertexType::SAsin, std::bind(&Stow::toStringSAsin, this, std::placeholders::_1)}
      , {VertexType::SAcos, std::bind(&Stow::toStringSAcos, this, std::placeholders::_1)}
      , {VertexType::SAtan, std::bind(&Stow::toStringSAtan, this, std::placeholders::_1)}
      , {VertexType::SRadToDeg, std::bind(&Stow::toStringSRadToDeg, this, std::placeholders::_1)}
      , {VertexType::SDegToRad, std::bind(&Stow::toStringSDegToRad, this, std::placeholders::_1)}
      , {VertexType::SLog, std::bind(&Stow::toStringSLog, this, std::placeholders::_1)}
      , {VertexType::SExp, std::bind(&Stow::toStringSExp, this, std::placeholders::_1)}
      , {VertexType::SSqrt, std::bind(&Stow::toStringSSqrt, this, std::placeholders::_1)}
      , {VertexType::SAbs, std::bind(&Stow::toStringSAbs, this, std::placeholders::_1)}
      , {VertexType::SPow, std::bind(&Stow::toStringSPow, this, std::placeholders::_1)}
      , {VertexType::SAtan2, std::bind(&Stow::toStringSAtan2, this, std::placeholders::_1)}
      , {VertexType::SHypot, std::bind(&Stow::toStringSHypot, this, std::placeholders::_1)}
      , {VertexType::SMin, std::bind(&Stow::toStringSMin, this, std::placeholders::_1)}
      , {VertexType::SMax, std::bind(&Stow::toStringSMax, this, std::placeholders::_1)}
      , {VertexType::SFloor, std::bind(&Stow::toStringSFloor, this, std::placeholders::_1)}
      , {VertexType::SCeil, std::bind(&Stow::toStringSCeil, this, std::placeholders::_1)}
      , {VertexType::SRound, std::bind(&Stow::toStringSRound, this, std::placeholders::_1)}
      , {VertexType::SCondition, std::bind(&Stow::toStringSCondition, this, std::placeholders::_1)}
      , {VertexType::VScalars, std::bind(&Stow::toStringVScalars, this, std::placeholders::_1)}
      , {VertexType::VConstantX, std::bind(&Stow::toStringVConstantX, this, std::placeholders::_1)}
      , {VertexType::VConstantY, std::bind(&Stow::toStringVConstantY, this, std::placeholders::_1)}
      , {VertexType::VConstantZ, std::bind(&Stow::toStringVConstantZ, this, std::placeholders::_1)}
      , {VertexType::VConstantZero, std::bind(&Stow::toStringVConstantZero, this, std::placeholders::_1)}
      , {VertexType::VDecay, std::bind(&Stow::toStringVDecay, this, std::placeholders::_1)}
      , {VertexType::VLength, std::bind(&Stow::toStringVLength, this, std::placeholders::_1)}
      , {VertexType::VDot, std::bind(&Stow::toStringVDot, this, std::placeholders::_1)}
      , {VertexType::VNormalize, std::bind(&Stow::toStringVNormalize, this, std::placeholders::_1)}
      , {VertexType::VCross, std::bind(&Stow::toStringVCross, this, std::placeholders::_1)}
      , {VertexType::VMultiplication, std::bind(&Stow::toStringVMultiplication, this, std::placeholders::_1)}
      , {VertexType::VDivision, std::bind(&Stow::toStringVDivision, this, std::placeholders::_1)}
      , {VertexType::VAddition, std::bind(&Stow::toStringVAddition, this, std::placeholders::_1)}
      , {VertexType::VSubtraction, std::bind(&Stow::toStringVSubtraction, this, std::placeholders::_1)}
      , {VertexType::VParenth, std::bind(&Stow::toStringVParenth, this, std::placeholders::_1)}
      , {VertexType::RConstantZero, std::bind(&Stow::toStringRConstantZero, this, std::placeholders::_1)}
      , {VertexType::RScalars, std::bind(&Stow::toStringRScalars, this, std::placeholders::_1)}
      , {VertexType::RAxisAngle, std::bind(&Stow::toStringRAxisAngle, this, std::placeholders::_1)}
      , {VertexType::RFromTo, std::bind(&Stow::toStringRFromTo, this, std::placeholders::_1)}
      , {VertexType::RDecayAxis, std::bind(&Stow::toStringRDecayAxis, this, std::placeholders::_1)}
      , {VertexType::RDecayAngle, std::bind(&Stow::toStringRDecayAngle, this, std::placeholders::_1)}
      , {VertexType::RDecayAxisVector, std::bind(&Stow::toStringRDecayAxisVector, this, std::placeholders::_1)}
      , {VertexType::RBasePairs, std::bind(&Stow::toStringRBasePairs, this, std::placeholders::_1)}
      , {VertexType::RInverse, std::bind(&Stow::toStringRInverse, this, std::placeholders::_1)}
      , {VertexType::RMultiplication, std::bind(&Stow::toStringRMultiplication, this, std::placeholders::_1)}
      , {VertexType::RDivision, std::bind(&Stow::toStringRDivision, this, std::placeholders::_1)}
      , {VertexType::RParenth, std::bind(&Stow::toStringRParenth, this, std::placeholders::_1)}
      , {VertexType::RRotateVector, std::bind(&Stow::toStringRRotateVector, this, std::placeholders::_1)}
      , {VertexType::CVectors, std::bind(&Stow::toStringCVectors, this, std::placeholders::_1)}
      , {VertexType::CRotationOrigin, std::bind(&Stow::toStringCRotationOrigin, this, std::placeholders::_1)}
      , {VertexType::CConstantZero, std::bind(&Stow::toStringCConstantZero, this, std::placeholders::_1)}
      , {VertexType::CInverse, std::bind(&Stow::toStringCInverse, this, std::placeholders::_1)}
      , {VertexType::CMap, std::bind(&Stow::toStringCMap, this, std::placeholders::_1)}
      , {VertexType::CMultiplication, std::bind(&Stow::toStringCMultiplication, this, std::placeholders::_1)}
      , {VertexType::CDivision, std::bind(&Stow::toStringCDivision, this, std::placeholders::_1)}
      , {VertexType::CParenth, std::bind(&Stow::toStringCParenth, this, std::placeholders::_1)}
      , {VertexType::CDecayRotation, std::bind(&Stow::toStringCDecayRotation, this, std::placeholders::_1)}
      , {VertexType::CDecayOrigin, std::bind(&Stow::toStringCDecayOrigin, this, std::placeholders::_1)}
    };
  };
}

#endif
