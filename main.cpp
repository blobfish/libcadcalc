/*
 * libcadcalc. Math Expression Parsing And Evaluation For CAD
 * Copyright (C) 2020  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <sstream>

#include <boost/math/constants/constants.hpp>

#include "lccmanager.h"

namespace
{
  std::string prettyDouble(const double &dIn, int precision = std::numeric_limits<double>::digits10 - 1)
  {
    //std::numeric_limits<double>::digits10: gives '25.300000000000001' for '25.3'
    //std::numeric_limits<double>::digits10 - 1: gives '25.3' for '25.3'
    std::ostringstream stream;
    stream << std::setprecision(precision) << std::fixed << dIn;
    std::string out = stream.str();
    out.erase(out.find_last_not_of('0') + 1, std::string::npos);
    if (out.back() == '.')
      out += '0';
    return out;
  }
  
  std::string generateValueString(const std::vector<double> &vIn)
  {
    std::stringstream out;
    if (vIn.size() == 1) //scalar
    {
      out << prettyDouble(vIn[0]);
    }
    else if (vIn.size() == 3) //vector
    {
      out << "[" << prettyDouble(vIn[0]) << ", " << prettyDouble(vIn[1]) << ", " << prettyDouble(vIn[2]) << "]";
    }
    else if (vIn.size() == 4) //rotation/quat
    {
      out << "[" << prettyDouble(vIn[0]) << ", " << prettyDouble(vIn[1]) << ", " << prettyDouble(vIn[2]) <<  ", " << prettyDouble(vIn[3]) << "]";
    }
    else if (vIn.size() == 7) //csys
    {
      out << "[[" << prettyDouble(vIn[0]) << ", " << prettyDouble(vIn[1]) << ", " << prettyDouble(vIn[2]) <<  ", " << prettyDouble(vIn[3]) << "]";
      out << ", " << "[" << prettyDouble(vIn[4]) << ", " << prettyDouble(vIn[5]) << ", " << prettyDouble(vIn[6]) << "]]";
    }
    else
      assert(0); //unsupported quantity.
      
    return out.str();
  }
  
  struct Test
  {
    std::string inputExpression; //!< expression to parse.
    std::string outputExpression; //!< the expression extracted back out of the graph.
    bool runTest; //!< whether this test should be run or not.
    bool parseShouldPass; //!< whether this test is suppose to pass or fail.
    bool updateShouldPass; //!< whether this test is suppose to update the graph correctly or not.
    bool writeOutParseGraph; //!< whether the parse graph should be written to graphviz file.
    bool writeOutExpressionGraph; //!< whether the parse graph should be written to graphviz file.
    std::vector<double> value; //!< array of doubles that should be the result value of expresssion.
    
    bool isAcceptedValue(const std::vector<double> &vIn) const
    {
      assert(vIn.size() == value.size());
      for (std::size_t index = 0; index < value.size(); ++index)
        if (!fuzzyCompare(vIn[index], value[index]))
          return false;
      return true;
    }
    
    static bool fuzzyCompare(const double &v1, const double &v2)
    {
      if (std::fabs(v1 - v2) < std::numeric_limits<float>::epsilon())
        return true;
      return false;
    }
  };
}

//shut the compiler up about unused variable.
template<class T> void ignore(const T&) {}
int main(int, char **)
{
  bool goTest = true; ignore(goTest); //run the test
  bool stopTest = false; ignore(stopTest); //don't run the test
  bool parsePass = true; ignore(parsePass); //parse should pass
  bool parseFail = false; ignore(parseFail); //parse should fail
  bool updatePass = true; ignore(updatePass); //update should pass
  bool updateFail = false; ignore(updateFail); //update should fail
  bool goParseDot = true; ignore(goParseDot); //export a graphviz file of parsing result
  bool stopParseDot = false; ignore(stopParseDot); //don't export graphviz file of parsing result
  bool goExpressDot = true; ignore(goExpressDot); //export a graphviz file of current expression graph. note before update
  bool stopExpressDot = false; ignore(stopExpressDot); //don't export a graphviz file of current expression graph.
  
  std::vector<Test> tests
  {
    //passing scalar
    {"sDouble = 0.25" , "sDouble = 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25}}
    , {"sDouble02 = .25" , "sDouble02 = 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25}}
    , {"sDouble03 = 25." , "sDouble03 = 25.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {25.0}}
    , {"sDouble04 = 25.3" , "sDouble04 = 25.3", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {25.3}}
    , {" sDouble05=-0.25 " , "sDouble05 = -0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-0.25}}
    , {"sDouble06 = -.25" , "sDouble06 = -0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-0.25}}
    , {"sDouble07 = -25." , "sDouble07 = -25.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-25.0}}
    , {"sDouble08 = -25.3" , "sDouble08 = -25.3", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-25.3}}
    , {"sName = sDouble" , "sName = sDouble", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25}}
    , {"PIE = PI" , "PIE = PI", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {boost::math::constants::pi<double>()}}
    , {"EULER = E" , "EULER = E", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {boost::math::constants::e<double>()}}
    , {"sMultiply2Doubles01 = 0.25 * 0.25" , "sMultiply2Doubles01 = 0.25 * 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {.0625}}
    , {"sMultiply2Doubles02=0.25*-0.25" , "sMultiply2Doubles02 = 0.25 * -0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-.0625}}
    , {"sMultiply2Names = sMultiply2Doubles01 * sMultiply2Doubles02" , "sMultiply2Names = sMultiply2Doubles01 * sMultiply2Doubles02", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-0.00390625}}
    , {"sMultiplyNameDouble = sMultiply2Doubles01 * 0.25" , "sMultiplyNameDouble = sMultiply2Doubles01 * 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.015625}}
    , {"sMultiplyDoubleName = 0.25 * sMultiply2Doubles01" , "sMultiplyDoubleName = 0.25 * sMultiply2Doubles01", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.015625}}
    , {"sDivide2Doubles = 0.25 / 0.25" , "sDivide2Doubles = 0.25 / 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sDivide2Names = sDivide2Doubles / sMultiply2Doubles01" , "sDivide2Names = sDivide2Doubles / sMultiply2Doubles01", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {16.0}}
    , {"sDivideNameDouble = sDivide2Doubles / 0.25" , "sDivideNameDouble = sDivide2Doubles / 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {4.0}}
    , {"sDivideDoubleName = 0.25 / sDivide2Doubles" , "sDivideDoubleName = 0.25 / sDivide2Doubles", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25}}
    , {"sFactorCombination01 = 0.25 * sMultiply2Doubles01 / 6.0 * sDivide2Doubles / 2.0", "sFactorCombination01 = 0.25 * sMultiply2Doubles01 / 6.0 * sDivide2Doubles / 2.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.00130208333333}}
    , {"sAdd2Doubles = 0.25 + 0.25", "sAdd2Doubles = 0.25 + 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5}}
    , {"sAdd2Names = sDouble + sDouble", "sAdd2Names = sDouble + sDouble", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5}}
    , {"sAddNameDouble = sDouble + 0.25", "sAddNameDouble = sDouble + 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5}}
    , {"sAddDoubleName = 0.25 + sDouble", "sAddDoubleName = 0.25 + sDouble", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5}}
    , {"sSubtract2Names = sAdd2Doubles - sDouble", "sSubtract2Names = sAdd2Doubles - sDouble", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25}}
    , {"sSubtract2Doubles = 0.5 - 0.25", "sSubtract2Doubles = 0.5 - 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25}}
    , {"sSubtractNameDouble = sSubtract2Doubles - 0.25", "sSubtractNameDouble = sSubtract2Doubles - 0.25", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sSubtractDoubleName = 0.25 - sSubtract2Doubles", "sSubtractDoubleName = 0.25 - sSubtract2Doubles", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sTermComination01 = 0.25 + 3.0 - 0.5 + 1.0 - 4.0 + 2.0", "sTermComination01 = 0.25 + 3.0 - 0.5 + 1.0 - 4.0 + 2.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.75}}
    , {"sTermComination02 = 0.25 + sAdd2Doubles - 0.5 + sSubtract2Doubles - 4.0 + sDouble", "sTermComination02 = 0.25 + sAdd2Doubles - 0.5 + sSubtract2Doubles - 4.0 + sDouble", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-3.25}}
    , {"sFactorTermComination01 = 6 + 3 * 2.0 + 6 / 2", "sFactorTermComination01 = 6.0 + 3.0 * 2.0 + 6.0 / 2.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {15.0}}
    , {"sFactorTermComination02 = 0.25 - sSubtract2Doubles * 10.0 + sAdd2Doubles / sDouble", "sFactorTermComination02 = 0.25 - sSubtract2Doubles * 10.0 + sAdd2Doubles / sDouble", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-.25}}
    , {"sParenth01 = (0.25)", "sParenth01 = (0.25)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25}}
    , {"sParenth02 = (sDouble)", "sParenth02 = (sDouble)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25}}
    , {"sParenth03 = (0.25 * 0.25)", "sParenth03 = (0.25 * 0.25)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0625}}
    , {"sParenth04 = (sDouble * sParenth01)", "sParenth04 = (sDouble * sParenth01)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0625}}
    , {"sParenth05 = (0.25 / 0.25)", "sParenth05 = (0.25 / 0.25)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sParenth06 = (sDouble / sParenth01)", "sParenth06 = (sDouble / sParenth01)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sParenth07 = (0.25 + 0.25)", "sParenth07 = (0.25 + 0.25)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5}}
    , {"sParenth08 = (sDouble + sParenth01)", "sParenth08 = (sDouble + sParenth01)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5}}
    , {"sParenth09 = (0.25 - 0.25)", "sParenth09 = (0.25 - 0.25)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sParenth10 = (sDouble - sParenth01)", "sParenth10 = (sDouble - sParenth01)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sParenth11 = (sDouble - sParenth01) + (0.25 * 0.25)", "sParenth11 = (sDouble - sParenth01) + (0.25 * 0.25)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0625}}
    , {"sParenth12 = ((sDouble - sParenth01) * 0.3 * (0.25 + sParenth02))", "sParenth12 = ((sDouble - sParenth01) * 0.3 * (0.25 + sParenth02))", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sParenth13 = (6 + 3) * (2.0 + 6) / 2", "sParenth13 = (6.0 + 3.0) * (2.0 + 6.0) / 2.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {36.0}}
    , {"sSin = sin(PI / 4)", "sSin = sin(PI / 4.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.707106781187}}
    , {"sCos=cos(PI/4)", "sCos = cos(PI / 4.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.707106781187}}
    , {"sTan = tan(PI / 4)", "sTan = tan(PI / 4.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sASin = asin(0.707106781187)", "sASin = asin(0.707106781187)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.785398163398}}
    , {"sACos = acos(0.707106781187)", "sACos = acos(0.707106781187)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.785398163398}}
    , {"sATan = atan(1.0)", "sATan = atan(1.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.785398163398}}
    , {"sRadToDeg = radtodeg(0.785398163398)", "sRadToDeg = radtodeg(0.785398163398)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {45.0}}
    , {"sDegToRad = degtorad(45.0)", "sDegToRad = degtorad(45.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.785398163398}}
    , {"sLog = log(2.0)", "sLog = log(2.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.301029995664}}
    , {"sExp = exp(2.0)", "sExp = exp(2.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {7.38905609893}}
    , {"sSqrt = sqrt(25)", "sSqrt = sqrt(25.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {5.0}}
    , {"sAbs = abs(-2.4)", "sAbs = abs(-2.4)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {2.4}}
    , {"sPow = pow(2, 3)", "sPow = pow(2.0, 3.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {8.0}}
    , {"sATan2 = atan2(1, 1)", "sATan2 = atan2(1.0, 1.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.785398163398}}
    , {"sHypot = hypot(3,4)", "sHypot = hypot(3.0, 4.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {5.0}}
    , {"sMin = min(1.4, 4.8)", "sMin = min(1.4, 4.8)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.4}}
    , {"sMax = max(1.4, 4.8)", "sMax = max(1.4, 4.8)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {4.8}}
    , {"sFloor1 = floor(1.374, 0.125)", "sFloor1 = floor(1.374, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.25}}
    , {"sFloor2 = floor(1.375, 0.125)", "sFloor2 = floor(1.375, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.375}}
    , {"sFloor3 = floor(1.376, 0.125)", "sFloor3 = floor(1.376, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.375}}
    , {"sCeil1 = ceil(1.374, 0.125)", "sCeil1 = ceil(1.374, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.375}}
    , {"sCeil2 = ceil(1.375, 0.125)", "sCeil2 = ceil(1.375, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.375}}
    , {"sCeil3 = ceil(1.376, 0.125)", "sCeil3 = ceil(1.376, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.5}}
    , {"sRound1 = round(1.0624, 0.125)", "sRound1 = round(1.0624, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sRound2 = round(1.0625, 0.125)", "sRound2 = round(1.0625, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.125}}
    , {"sRound3 = round(1.0626, 0.125)", "sRound3 = round(1.0626, 0.125)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.125}}
    , {"sIfThen3 = if(3 >= 3) then(1.0) else(0.0)", "sIfThen3 = if(3.0 >= 3.0) then (1.0) else (0.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sIfThen4 = if(3 <= 3) then(1.0) else(0.0)", "sIfThen4 = if(3.0 <= 3.0) then (1.0) else (0.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sIfThen1 = if(3 > 3) then(1.0) else(0.0)", "sIfThen1 = if(3.0 > 3.0) then (1.0) else (0.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sIfThen2 = if(3 < 3) then(1.0) else(0.0)", "sIfThen2 = if(3.0 < 3.0) then (1.0) else (0.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sIfThen5 = if(3 == 3) then(1.0) else(0.0)", "sIfThen5 = if(3.0 == 3.0) then (1.0) else (0.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sIfThen6 = if(3 != 3) then(1.0) else(0.0)", "sIfThen6 = if(3.0 != 3.0) then (1.0) else (0.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sIfThen7 = if(sin(degtorad(44)) < sin(degtorad(46))) then(sIfThen1) else(sIfThen2)", "sIfThen7 = if(sin(degtorad(44.0)) < sin(degtorad(46.0))) then (sIfThen1) else (sIfThen2)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    , {"sIfThen8 = if(3 != 3) then 1.0 else 0.0", "sIfThen8 = if(3.0 != 3.0) then 1.0 else 0.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0}}
    
    //passing vector
    , {"vDoubles = [0.25, 0.5, 0.75]", "vDoubles = [0.25, 0.5, 0.75]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25, 0.5, 0.75}}
    , {"vDoubles2=[ 0.25 , 0.5 , 0.75 ] ", "vDoubles2 = [0.25, 0.5, 0.75]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25, 0.5, 0.75}}
    , {"vScalarNames = [sDouble, sDouble, sDouble]", "vScalarNames = [sDouble, sDouble, sDouble]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25, 0.25, 0.25}}
    , {"vName = vDoubles", "vName = vDoubles", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25, 0.5, 0.75}}
    , {"vConstantX = X", "vConstantX = X", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0, 0.0, 0.0}}
    , {"vConstantY = Y", "vConstantY = Y", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 1.0, 0.0}}
    , {"vConstantZ = Z", "vConstantZ = Z", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 1.0}}
    , {"vConstantZero = VZERO", "vConstantZero = VZERO", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0}}
    , {"vScalarMath = [0.25+.125, 3/2, 6*.75]", "vScalarMath = [0.25 + 0.125, 3.0 / 2.0, 6.0 * 0.75]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.375, 1.5, 4.5}}
    , {"sDecayX = vdecay(X, [1.0, 2.0, 3.0])", "sDecayX = vdecay(X, [1.0, 2.0, 3.0])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.0}}
    , {"sDecayY = vdecay(Y, [1.0, 2.0, 3.0])", "sDecayY = vdecay(Y, [1.0, 2.0, 3.0])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {2.0}}
    , {"sDecayZ = vdecay(Z, [1.0, 2.0, 3.0])", "sDecayZ = vdecay(Z, [1.0, 2.0, 3.0])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {3.0}}
    , {"sLength = vlength([0.25, 0.25, 0.25])", "sLength = vlength([0.25, 0.25, 0.25])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.433012701892}}
    , {"sDot = vdot([0.25, 0.25, 0.25], [0.5, 0.5, 0.5])", "sDot = vdot([0.25, 0.25, 0.25], [0.5, 0.5, 0.5])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.375}}
    , {"vNormalize = vnormalize([0.25, 0.25, 0.25])", "vNormalize = vnormalize([0.25, 0.25, 0.25])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.57735026918963, 0.57735026918963, 0.57735026918963}}
    , {"vCross = vcross(vnormalize([1.0, 1.0, 0.0]), vnormalize([-1.0, 1.0, 0.0]))", "vCross = vcross(vnormalize([1.0, 1.0, 0.0]), vnormalize([-1.0, 1.0, 0.0]))", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 1.0}}
    , {"sLengthSin = sin(vlength([0.25, 0.25, 0.25]))", "sLengthSin = sin(vlength([0.25, 0.25, 0.25]))", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.419607349285}}
    , {"sDotSin = sin(vdot(vnormalize([0.25, 0.25, 0.25]), vnormalize([0.25, -0.25, 0.25])))", "sDotSin = sin(vdot(vnormalize([0.25, 0.25, 0.25]), vnormalize([0.25, -0.25, 0.25])))", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.32719469679615}}
    , {"vAddition = [0.25, 0.25, 0.25] + [0.5, 0.5, 0.5]", "vAddition = [0.25, 0.25, 0.25] + [0.5, 0.5, 0.5]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.75, 0.75, 0.75}}
    , {"vMultiplication = [0.25, 0.25, 0.25] * 0.5", "vMultiplication = [0.25, 0.25, 0.25] * 0.5", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.125, 0.125, 0.125}}
    , {"vDivision = [0.25, 0.25, 0.25] / 0.5", "vDivision = [0.25, 0.25, 0.25] / 0.5", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5, 0.5, 0.5}}
    , {"vSubtraction = [0.25, 0.25, 0.25] - [0.5, 0.5, 0.5]", "vSubtraction = [0.25, 0.25, 0.25] - [0.5, 0.5, 0.5]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-0.25, -0.25, -0.25}}
    , {"vParenth1 = ([0.25, 0.25, 0.25])", "vParenth1 = ([0.25, 0.25, 0.25])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.25, 0.25, 0.25}}
    , {"vOp1 = [0.25, 0.25, 0.25] + [0.5, 0.5, 0.5] * 2.0", "vOp1 = [0.25, 0.25, 0.25] + [0.5, 0.5, 0.5] * 2.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.25, 1.25, 1.25}}
    , {"vOp2 = ([0.25, 0.25, 0.25] + [0.5, 0.5, 0.5]) * 5.0", "vOp2 = ([0.25, 0.25, 0.25] + [0.5, 0.5, 0.5]) * 5.0", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {3.75, 3.75, 3.75}}
    , {"vOp3 = [0.25, 0.25, 0.25] + ([0.5, 2.0 * 0.25, 0.5] * 5.0)", "vOp3 = [0.25, 0.25, 0.25] + ([0.5, 2.0 * 0.25, 0.5] * 5.0)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {2.75, 2.75, 2.75}}
    
    //passing rotation
    , {"rConstant = RZERO", "rConstant = RZERO", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0}}
    , {"rScalars4 = [0.25, 0.25, 0.25, .35]", "rScalars4 = [0.25, 0.25, 0.25, 0.35]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.44901325506693, 0.44901325506693, 0.44901325506693, 0.62861855709371}}
    , {"rNames4 = [sDouble, sDouble, sDouble, sDouble]", "rNames4 = [sDouble, sDouble, sDouble, sDouble]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5, 0.5, 0.5, 0.5}}
    , {"rAxisAngle = [[0.25, 0.25, 0.25], .35]", "rAxisAngle = [[0.25, 0.25, 0.25], 0.35]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.10052138010776, 0.10052138010776, 0.10052138010776, 0.98472653890493}}
    , {"rVectorFromTo = [[1.0, 0.0, 0.0], [0.0, 1.0, 1.0]]", "rVectorFromTo = [[1.0, 0.0, 0.0], [0.0, 1.0, 1.0]]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, -0.5, 0.5, 0.70710678118655}}
    , {"vRDecayAxis = rdecayaxis([0.25, 0.25, 0.25, .35])", "vRDecayAxis = rdecayaxis([0.25, 0.25, 0.25, 0.35])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.57735026918962, 0.57735026918962, 0.57735026918962}}
    , {"sRDecayAngle = rdecayangle([0.25, 0.25, 0.25, .35])", "sRDecayAngle = rdecayangle([0.25, 0.25, 0.25, 0.35])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {1.78204136043620}}
    , {"vRDecayAxisVectorX = rdecayaxis(X, [0.25, 0.25, 0.25, .35])", "vRDecayAxisVectorX = rdecayaxis(X, [0.25, 0.25, 0.25, 0.35])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.19354838709677, 0.96774193548387, -0.16129032258064}}
    , {"vRDecayAxisVectorY = rdecayaxis(Y, [0.25, 0.25, 0.25, .35])", "vRDecayAxisVectorY = rdecayaxis(Y, [0.25, 0.25, 0.25, 0.35])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-0.16129032258064, 0.19354838709677, 0.96774193548387}}
    , {"vRDecayAxisVectorZ = rdecayaxis(Z, [0.25, 0.25, 0.25, .35])", "vRDecayAxisVectorZ = rdecayaxis(Z, [0.25, 0.25, 0.25, 0.35])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.96774193548387, -0.16129032258064, 0.19354838709677}}
    , {"rBasePairsXY = [XY, [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]", "rBasePairsXY = [XY, [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5, 0.5, 0.5, 0.5}}
    , {"rBasePairsXZ = [XZ, [0.0, 1.0, 0.0], [1.0, 0.0, 0.0]]", "rBasePairsXZ = [XZ, [0.0, 1.0, 0.0], [1.0, 0.0, 0.0]]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5, 0.5, 0.5, 0.5}}
    , {"rBasePairsYZ = [YZ, [0.0, 0.0, 1.0], [1.0, 0.0, 0.0]]", "rBasePairsYZ = [YZ, [0.0, 0.0, 1.0], [1.0, 0.0, 0.0]]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.5, 0.5, 0.5, 0.5}}
    , {"rInverse = rinverse([0.25, 0.25, 0.25, .35])", "rInverse = rinverse([0.25, 0.25, 0.25, 0.35])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-0.44901325506693, -0.44901325506693, -0.44901325506693, 0.62861855709371}}
    , {"rMultiply = [0.25, 0.25, 0.25, .35] * [0.25, 0.25, 0.25, .35]", "rMultiply = [0.25, 0.25, 0.25, 0.35] * [0.25, 0.25, 0.25, 0.35]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.56451612903226, 0.56451612903226, 0.56451612903226, -0.20967741935484}}
    , {"rDivide = [0.25, 0.25, 0.25, .35] / [0.5, 0.5, 0.5, 2.0]", "rDivide = [0.25, 0.25, 0.25, 0.35] / [0.5, 0.5, 0.5, 2.0]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.26782783411486, 0.26782783411486, 0.26782783411486, 0.8858920666876}}
    , {"rParenth = [0.25, 0.25, 0.25, .35] * ([0.5, 0.5, 0.5, .5] / [0.1, 0.1, 0.1, .5])", "rParenth = [0.25, 0.25, 0.25, 0.35] * ([0.5, 0.5, 0.5, 0.5] / [0.1, 0.1, 0.1, 0.5])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.57701759830681, 0.57701759830681, 0.57701759830681, -0.03394221166511}}
    , {"vRotatedVector = rrotatevector([[0.0, 0.0, 1.0], PI / 2.0], vnormalize([1.0, 1.0, 1.0]))", "vRotatedVector = rrotatevector([[0.0, 0.0, 1.0], PI / 2.0], vnormalize([1.0, 1.0, 1.0]))", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-0.57735026918963, 0.57735026918963, 0.57735026918963}}
    , {"vRotatedVector2 = rrotatevector(rScalars4, vDoubles)", "vRotatedVector2 = rrotatevector(rScalars4, vDoubles)", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.69354838709677, 0.21774193548387, 0.58870967741935}}
    
    //passing csys
    , {"cVectors = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0], [10.0, 10.0, 10.0]]", "cVectors = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0], [10.0, 10.0, 10.0]]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, 10.0, 10.0, 10.0}}
    , {"cNames4 = [vConstantX, vConstantY, vConstantZ, vDoubles]", "cNames4 = [vConstantX, vConstantY, vConstantZ, vDoubles]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, 0.25, 0.5, 0.75}}
    , {"cRotationOrigin = [RZERO, [1.0, 2.0, 3.0]]", "cRotationOrigin = [RZERO, [1.0, 2.0, 3.0]]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, 1.0, 2.0, 3.0}}
    , {"cAbsolute2 = [RZERO, VZERO]", "cAbsolute2 = [RZERO, VZERO]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0}}
    , {"cAbsolute1 = CZERO", "cAbsolute1 = CZERO", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0}}
    , {"cInverse = cinverse([RZERO, [5.0, 5.0, 5.0]])", "cInverse = cinverse([RZERO, [5.0, 5.0, 5.0]])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, -5.0, -5.0, -5.0}}
    , {"vMapVector01 = cmap([RZERO, [10.0, 10.0, 10.0]], [RZERO, [5.0, 5.0, 5.0]], [0.25, 0.25, 0.25])", "vMapVector01 = cmap([RZERO, [10.0, 10.0, 10.0]], [RZERO, [5.0, 5.0, 5.0]], [0.25, 0.25, 0.25])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-4.75, -4.75, -4.75}}
    , {"vMapVector02 = cmap([RZERO, [10.0, 10.0, 10.0]], [RZERO, [5.0, 5.0, 5.0]], [0.25, 0.25, 0.25] + [0.125, 0.125, 0.125])", "vMapVector02 = cmap([RZERO, [10.0, 10.0, 10.0]], [RZERO, [5.0, 5.0, 5.0]], [0.25, 0.25, 0.25] + [0.125, 0.125, 0.125])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {-4.625, -4.625, -4.625}}
    , {"cMultiplication = [RZERO, [5.0, 5.0, 5.0]] * [RZERO, [10.0, 10.0, 10.0]]", "cMultiplication = [RZERO, [5.0, 5.0, 5.0]] * [RZERO, [10.0, 10.0, 10.0]]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, 15.0, 15.0, 15.0}}
    , {"cDivision = [RZERO, [5.0, 5.0, 5.0]] / [RZERO, [10.0, 10.0, 10.0]]", "cDivision = [RZERO, [5.0, 5.0, 5.0]] / [RZERO, [10.0, 10.0, 10.0]]", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, -5.0, -5.0, -5.0}}
    , {"cParenth = [RZERO, [5.0, 5.0, 5.0]] / ([RZERO, [10.0, 10.0, 10.0]] * [RZERO, [15.0, 15.0, 15.0]])", "cParenth = [RZERO, [5.0, 5.0, 5.0]] / ([RZERO, [10.0, 10.0, 10.0]] * [RZERO, [15.0, 15.0, 15.0]])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, -20.0, -20.0, -20.0}}
    , {"rCDecayRotation = cdecayrotation([RZERO, [5.0, 5.0, 5.0]])", "rCDecayRotation = cdecayrotation([RZERO, [5.0, 5.0, 5.0]])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0}}
    , {"vCDecayOrigin = cdecayorigin([RZERO, [5.0, 5.0, 5.0]])", "vCDecayOrigin = cdecayorigin([RZERO, [5.0, 5.0, 5.0]])", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {5.0, 5.0, 5.0}}
    , {"cNames4Copy = cNames4", "cNames4Copy = cNames4", goTest, parsePass, updatePass, stopParseDot, stopExpressDot, {0.0, 0.0, 0.0, 1.0, 0.25, 0.5, 0.75}}
    
    //failing doubles
    , {"failDouble01 = ..25", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"failDouble02 = --25", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"failDouble03 = .-25", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"failDouble04 = -25.2.3", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"failDouble05 = noexist", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"failVNames = [vDoubles, vDoubles, vDoubles]", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}} //wrong type
    
    //test reserved words
    , {"PI = PI", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"E = E", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"sin = 2.0", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"cos = 2.0", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"tan = 2.0", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"asin = 2.0", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"acos = 2.0", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"atan = 2.0", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"radtodeg = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"degtorad = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"log = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"exp = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"sqrt = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"abs = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"atan2 = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"hypot = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"min = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"max = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"floor = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"ceil = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"round = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"if = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"then = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"else = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"X = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"Y = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"Z = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"XY = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"XZ = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"YZ = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"YZ = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"VZERO = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"vdecay = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"vlength = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"vdot = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"vnormalize = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"vcross = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"RZERO = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"rdecayaxis = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"rdecayangle = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"rinverse = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"rrotatevector = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"cdecayrotation = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"cdecayorigin = 32.5", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
    
    //update failures
    , {"divideByZero = 5.0 / 0.0", "", goTest, parsePass, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"normalizeZero = vnormalize([0.0, 0.0, 0.0])", "", goTest, parsePass, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"negativeSqrt = sqrt(-1.0)", "", goTest, parsePass, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"zeroATan2 = atan2(0.0, 0.0)", "", goTest, parsePass, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"zeroLog = log(0.0)", "", goTest, parsePass, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"negativeLog = log(-1.0)", "", goTest, parsePass, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"sDouble = [1.0, 0.0, 0.0]", "", goTest, parsePass, updateFail, stopParseDot, stopExpressDot, {0.0}}
    , {"sDouble = sTermComination02", "", goTest, parseFail, updateFail, stopParseDot, stopExpressDot, {0.0}}
  };
  
  std::string_view green("\033[0;32m");
  std::string_view yellow("\033[0;33m");
  std::string_view red("\033[0;31m");
  std::string_view reset("\033[0m");
  
  lcc::Manager man;
  for (const auto &t : tests)
  {
    if (!t.runTest)
    {
      std::cout << std::endl << yellow << "skipping test: " << t.inputExpression << reset << std::endl;
      continue;
    }
    
    assert(!t.inputExpression.empty());
    
    man.writeParseTree = t.writeOutParseGraph;
    man.writeExpressionGraph = t.writeOutExpressionGraph;
    
    std::cout << std::endl << "test: " << t.inputExpression << std::endl;
    
    lcc::Result result = man.parseString(t.inputExpression);
    if (t.parseShouldPass == result.parsed)
      std::cout << green << "OK: expected parsing result. Message: " << result.parseErrorMessage << reset << std::endl;
    else
    {
      std::cout << red << "FAILED: unexpected parsing result. Message: " << result.parseErrorMessage << reset << std::endl;
      continue;
    }
    if (!result.parsed)
      continue;
    
    if (t.updateShouldPass == result.updated)
      std::cout << green << "OK: expected update result. Message: " << result.updateErrorMessage << reset << std::endl;
    else
    {
      std::cout << red << "FAILED: unexpected update result. Message: " << result.updateErrorMessage << reset << std::endl;
      continue;
    }
    if (!result.updated)
      continue;
    
    if (t.outputExpression == man.buildExpressionString(result.expressionName))
      std::cout << green << "OK: expected output expression" << reset << std::endl;
    else
    {
      std::cout << red << "FAILED: unexpected output expression" << std::endl;
      std::cout << "    Expected: " << t.outputExpression << std::endl;
      std::cout << "    Generate: " << man.buildExpressionString(result.expressionName) << reset << std::endl;
      continue;
    }
    
    if (t.isAcceptedValue(man.getExpressionValue(result.expressionName)))
      std::cout << green << "OK: expected expression value" << reset << std::endl;
    else
    {
      std::cout << red << "FAILED: unexpected expression value" << std::endl;
      std::cout << "    Expected: " << generateValueString(t.value) << std::endl;
      std::cout << "    Generate: " << generateValueString(man.getExpressionValue(result.expressionName)) << reset << std::endl;
      continue;
    }
  }
  
  //update dependent values.
  std::cout << std::endl << "update dependent test: " << std::endl;
  //assert prior state
  assert(Test::fuzzyCompare(man.getExpressionValue("sDouble").front(), 0.25));
  assert(Test::fuzzyCompare(man.getExpressionValue("sAdd2Names").front(), 0.5));
  assert(Test::fuzzyCompare(man.getExpressionValue("sTermComination02").front(), -3.25));
  lcc::Result result = man.parseString("sDouble = 0.5");
  assert(result.parsed && result.updated);
  if
  (
    !Test::fuzzyCompare(man.getExpressionValue("sDouble").front(), 0.5)
    || !Test::fuzzyCompare(man.getExpressionValue("sAdd2Names").front(), 1.0)
    || !Test::fuzzyCompare(man.getExpressionValue("sTermComination02").front(), -3.00)
  )
    std::cout << red << "FAILED: unexpected expression value" << reset << std::endl;
  else
    std::cout << green << "OK: expected expression values" << reset << std::endl;
  if (result.updatedIds.size() != 17)
    std::cout << red << "FAILED: unexpected number of updated. Expected 17 got: " << result.updatedIds.size() << reset << std::endl;
  else
    std::cout << green << "OK: expected number of updated" << reset << std::endl;
  
  //test export, reset and import
  std::cout << std::endl << "export import test: " << std::endl;
  std::ofstream fileOut("exportTest.expr");
  man.exportExpressions(fileOut);
  fileOut.close();
  auto sizeBefore = man.getExpressionNames().size();
  man.reset();
  if (man.getExpressionNames().empty())
    std::cout << green << "OK: manager reset as expected" << reset << std::endl;
  else
    std::cout << red << "FAILED: manager didn't reset" << reset << std::endl;
  std::ifstream fileIn("exportTest.expr");
  auto importResults = man.importExpressions(fileIn);
  fileIn.close();
  auto sizeAfter = man.getExpressionNames().size();
  if (sizeBefore == sizeAfter)
    std::cout << green << "OK: sizes match" << reset << std::endl;
  else
    std::cout << red << "FAILED: sizes don't match.    before: " << sizeBefore << "    after: " << sizeAfter << reset << std::endl;
  //only report failures
  for (const auto &r : importResults)
  {
    if (!r.parsed || !r.updated)
      std::cout << red << "FAILED didn't import: " << r.input << "    " << r.parseErrorMessage << "    " << r.updateErrorMessage << reset << std::endl;
  }
  
  //testing groups
  std::cout << std::endl << "group tests: " << std::endl;
  int group0 = man.addGroup("group0");
  int group1 = man.addGroup("group1");
  man.addExpressionToGroup(group1, *man.getExpressionId("sDouble")); //real case should test optional.
  int group2 = man.addGroup("group2");
  man.addExpressionToGroup(group2, *man.getExpressionId("sDouble"));
  man.addExpressionToGroup(group2, *man.getExpressionId("sAdd2Names"));
  int group3 = man.addGroup("group3");
  man.addExpressionToGroup(group3, *man.getExpressionId("sDouble"));
  man.addExpressionToGroup(group3, *man.getExpressionId("sAdd2Names"));
  man.addExpressionToGroup(group3, *man.getExpressionId("sTermComination02"));
  if (man.getGroupIds().size() ==  4)
    std::cout << green << "OK: group count" << reset << std::endl;
  else
    std::cout << red << "FAILED: wrong group count.    expected: " << 4 << "    got: " << man.getGroupIds().size() << reset << std::endl;
  if
  (
    man.getExpressionsOfGroup(group0).size() == 0
    && man.getExpressionsOfGroup(group1).size() == 1
    && man.getExpressionsOfGroup(group2).size() == 2
    && man.getExpressionsOfGroup(group3).size() == 3
  )
    std::cout << green << "OK: expected expression count of groups" << reset << std::endl;
  else
    std::cout << red << "FAILED: unexpected expression count of groups."
    << "    group0: " << man.getExpressionsOfGroup(group0).size()
    << "    group1: " << man.getExpressionsOfGroup(group1).size()
    << "    group2: " << man.getExpressionsOfGroup(group2).size()
    << "    group3: " << man.getExpressionsOfGroup(group3).size()
    << reset << std::endl;
  man.removeExpression("sTermComination02");
  if (man.getExpressionsOfGroup(group3).size() == 2)
    std::cout << green << "OK: expected expression count of group after expression removal" << reset << std::endl;
  else
    std::cout << red << "FAILED: expected expression count of group after expression removal. expected: 2    got: " << man.getExpressionsOfGroup(group3).size() << reset << std::endl;
  
  //testing serial
  std::cout << std::endl << "serial test: " << std::endl;
  lcc::Serial serial = man.serialOut();
  man.reset();
  auto sResults = man.serialIn(serial);
  if (sResults.empty())
    std::cout << green << "OK: expected 0 results from serial in" << reset << std::endl;
  else
    std::cout << red << "FAILED: unexpected result count from serial in. expected: 0    got: " << sResults.size() << reset << std::endl;
  if (man.getGroupIds().size() ==  4)
    std::cout << green << "OK: group count after serial in" << reset << std::endl;
  else
    std::cout << red << "FAILED: wrong group count after serial in.    expected: " << 4 << "    got: " << man.getGroupIds().size() << reset << std::endl;
  if
  (
    man.getExpressionsOfGroup(group0).size() == 0
    && man.getExpressionsOfGroup(group1).size() == 1
    && man.getExpressionsOfGroup(group2).size() == 2
    && man.getExpressionsOfGroup(group3).size() == 2
  )
    std::cout << green << "OK: expected expression count of groups after serial in" << reset << std::endl;
  else
    std::cout << red << "FAILED: unexpected expression count of groups after serial in."
    << "    group0: " << man.getExpressionsOfGroup(group0).size()
    << "    group1: " << man.getExpressionsOfGroup(group1).size()
    << "    group2: " << man.getExpressionsOfGroup(group2).size()
    << "    group3: " << man.getExpressionsOfGroup(group3).size()
    << reset << std::endl;
  if
  (
    !Test::fuzzyCompare(man.getExpressionValue("sDouble").front(), 0.5)
    || !Test::fuzzyCompare(man.getExpressionValue("sAdd2Names").front(), 1.0)
  )
    std::cout << red << "FAILED: unexpected expression value after serial in." << reset << std::endl;
  else
    std::cout << green << "OK: expected expression values after serial in." << reset << std::endl;
  
  return 0;
}

